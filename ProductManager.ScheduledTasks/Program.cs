﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagerScheduledTasks
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            string URI = "http://localhost:50238/SpecSheet/GenerateOutdated";
#else
            string URI = "http://productmanager.westchestergear.com/SpecSheet/GenerateOutdated";
#endif

            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string HtmlResult = wc.UploadString(URI, "");
            }
        }
    }
}
