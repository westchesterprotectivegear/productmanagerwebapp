﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Measurement
    {
        public virtual Dimension Dimension { get; set; }
        public virtual Guid DimensionID { get; set; }
        [Id]
        public virtual Guid ID { get; set; }
        public virtual Item Item { get; set; }
        public virtual string SKU { get; set; }
        public virtual Guid ItemID { get; set; }
        public virtual string Value { get; set; }
    }

    public sealed class MeasurementMapping : ClassMap<Measurement>
    {
        public MeasurementMapping()
        {
            Table("ProductMeasurements");
            Id(p => p.ID, "MeasurementID");
            Map(p => p.Value, "Measurement");
            Map(p => p.DimensionID);
            Map(p => p.ItemID);
            Map(p => p.SKU, "Item");
            References(x => x.Item)
                .Not.Insert().Not.Update().Column("ItemID");
            References(x => x.Dimension)
                .Not.Insert().Not.Update().Column("DimensionID");
        }
    }
}
