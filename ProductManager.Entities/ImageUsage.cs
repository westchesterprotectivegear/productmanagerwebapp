﻿using System;
using System.ComponentModel;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class ImageUsage
    {
        [Id]
        public virtual Guid ID { get; set; }
        public virtual Guid ImageID { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual ImageUsageType Type { get; set; }
        public virtual int Delta { get; set; }
    }

    public sealed class ImageUsageMapping : ClassMap<ImageUsage>
    {
        public ImageUsageMapping()
        {
            Id(p => p.ID);
            Map(p => p.ImageID);
            Map(p => p.BasePartNumber);
            Map(p => p.Type).CustomType<ImageUsageType>();
            Map(p => p.Delta);
        }
    }

    public enum ImageUsageType
    {
        [Description("Primary")]
        Primary = 0,
        [Description("Additional")]
        Additional = 1,
        [Description("Carton Marking")]
        CartonMarking = 2,
        [Description("Case Bumper")]
        CaseBumper = 3,
        [Description("Hang Tag")]
        HangTag = 4,
        [Description("Neck Label")]
        NeckLabel = 5,
        [Description("Vendible Instructions")]
        VendibleInstructions = 6
    }
}
