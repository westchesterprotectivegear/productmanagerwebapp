namespace ProductManager.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("PortalItem")]
    public partial class PortalItem
    {
        [StringLength(18)]
        public string SKU { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string CustomerNumber { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "numeric")]
        public decimal PriceListPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? CustomerSpecificPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Discount { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(2)]
        public string PriceList { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(100)]
        public string BasePartNumber { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string Photo { get; set; }

        [StringLength(150)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(40)]
        public string Description { get; set; }

        [StringLength(750)]
        public string MarketingCopy { get; set; }

        [StringLength(50)]
        public string Size { get; set; }

        [StringLength(8000)]
        public string UnitOfMeasure { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(35)]
        public string CustomerSKU { get; set; }

        [StringLength(1000)]
        public string UPC { get; set; }
    }
}
