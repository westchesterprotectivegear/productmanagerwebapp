﻿
using System;
using System.Collections.Generic;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class ProductManagerUser
    {
        public virtual string DisplayName { get; set; }
        public virtual string Email { get; set; }
        public virtual string ID { get; set; }
        public virtual IList<UserRole> Roles { get; set; }
        public virtual string UserName { get; set; }
    }

    public sealed class ProductManagerUserMapping : ClassMap<ProductManagerUser>
    {
        public ProductManagerUserMapping()
        {
            LazyLoad();
            Table("PMUser");
            Id(x => x.ID).GeneratedBy.Assigned();
            Map(x => x.DisplayName);
            Join("AspNetUsers", m =>
            {
                m.Inverse();
                m.Fetch.Join();
                m.KeyColumn("Id");
                m.Map(x => x.Email);
                m.Map(x => x.UserName);
                m.HasMany(x => x.Roles)
                    .KeyColumn("UserId")
                    .Inverse()
                    .Cascade.All()
                    .Not.LazyLoad();
            });
        }
    }

    public class Role
    {
        public virtual string ID { get; set; }
        public virtual string Name { get; set; }
    }

    public sealed class RoleMapping : ClassMap<Role>
    {
        public RoleMapping()
        {
            LazyLoad();
            Table("AspNetRoles");
            Id(x => x.ID).GeneratedBy.Assigned();
            Map(x => x.Name);
        }
    }

    public class UserRole
    {
        public virtual string RoleID { get; set; }
        public virtual string UserID { get; set; }
        public virtual Role Role { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public sealed class UserRoleMapping : ClassMap<UserRole>
    {
        public UserRoleMapping()
        {
            LazyLoad();
            Table("AspNetUserRoles");
            CompositeId()
                .KeyProperty(x => x.RoleID)
                .KeyProperty(x => x.UserID);
            References(x => x.Role)
                .Not.Insert().Not.Update().Column("RoleID");
        }
    }
}
