﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Collection
    {
        public virtual Guid ID { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<CollectionValue> Values { get; set; }
    }

    public sealed class CollectionMapping : ClassMap<Collection>
    {
        public CollectionMapping()
        {
            Table("ProductCollections");
            Id(p => p.ID);
            Map(p => p.Name);
            HasMany(x => x.Values)
                .KeyColumn("CollectionID").Cascade.All();
        }
    }

    public class CollectionValue
    {
        public virtual Guid CollectionID { get; set; }
        public virtual int CollectionIndex { get; set; }
        public virtual Guid ID { get; set; }
        public virtual string Value { get; set; }
    }

    public sealed class CollectionValueMapping : ClassMap<CollectionValue>
    {
        public CollectionValueMapping()
        {
            Table("ProductCollectionValues");
            Id(p => p.ID);
            Map(p => p.CollectionID);
            Map(p => p.CollectionIndex);
            Map(p => p.Value);
        }
    }
}
