﻿using System;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Photo
    {
        public virtual string AssociatedText { get; set; }
        public virtual string BasePartNumber { get; set; }
        [Id]
        public virtual Guid ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Type { get; set; }
        public virtual DateTime Created { get; set; }
    }

    public sealed class PhotoMapping : ClassMap<Photo>
    {
        public PhotoMapping()
        {
            Table("ProductPhoto");
            Id(p => p.ID, "PhotoID");
            Map(p => p.AssociatedText);
            Map(p => p.BasePartNumber);
            Map(p => p.Name, "Photo");
            Map(p => p.Type, "PhotoType");
            Map(p => p.Created, "CreatedDate");
        }
    }
}
