﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Feature
    {
        [Id]
        public virtual Guid ID { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual int Rank { get; set; }
        public virtual string Value { get; set; }
    }

    public sealed class FeatureMapping : ClassMap<Feature>
    {
        public FeatureMapping()
        {
            Table("ProductFeaturesBenefits");
            Id(p => p.ID, "FeatureID");
            Map(p => p.BasePartNumber);
            Map(p => p.Rank);
            Map(p => p.Value, "Feature");
        }
    }
}
