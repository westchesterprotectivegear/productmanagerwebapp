﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Mapping;
using NHibernate.Proxy;

namespace ProductManager.Entities
{
    [Serializable]
    public class PackSize : ICloneable
    {
        public virtual bool Active { get; set; }
        public virtual Apparel Apparel { get; set; }
        [ReadOnly]
        public virtual string BasePartNumber { get; set; }
        public virtual int CT1Cases { get; set; }
        public virtual double CT1GrossCaseWeight
        {
            get
            {
                return (PieceWeight * UnitsPerCase + 1) * CT1Cases + 1.5;
            }
        }
        public virtual string CT1GTIN { get; set; }
        public virtual double CT1Height { get; set; }
        public virtual double CT1Length { get; set; }
        public virtual double CT1Volume
        {
            get
            {
                return (CT1Width * CT1Length * CT1Height) / 1728;
            }
        }
        public virtual double CT1Width { get; set; }
        public virtual double CaseHeight { get; set; }
        public virtual double CaseLength { get; set; }
        public virtual double CaseVolume
        {
            get
            {
                return (CaseWidth * CaseLength * CaseHeight) / 1728;
            }
        }
        public virtual double CaseWidth { get; set; }
        public virtual string DipType { get; set; }
        public virtual string Display { get; set; }
        public virtual string GoldenSampleLocation { get; set; }
        public virtual double GrossCaseWeight
        {
            get
            {
                return PieceWeight * UnitsPerCase + 1;
            }
        }
        public virtual bool InnerDimensionsOnSpec { get; set; }
        public virtual double InnerHeight
        {
            get
            {
                return ItemHeight * UnitsPerPack;
            }
        }
        public virtual double InnerLength
        {
            get
            {
                return ItemLength;
            }
        }
        public virtual string InnerPackGTIN { get; set; }
        public virtual double InnerVolume
        {
            get
            {
                return (InnerWidth * InnerLength * InnerHeight) / 1728;
            }
        }
        public virtual double InnerWeight
        {
            get
            {
                return PieceWeight * UnitsPerPack;
            }
        }
        public virtual double InnerWidth
        {
            get
            {
                return ItemWidth;
            }
        }
        public virtual Item Item { get; set; }
        public virtual double ItemHeight { get; set; }
        public virtual Guid ItemID { get; set; }
        public virtual double ItemLength { get; set; }
        public virtual double ItemWidth { get; set; }
        public virtual string MasterCaseGTIN { get; set; }
        public virtual string MaterialWeight { get; set; }
        public virtual double NetCaseWeight { get; set; }
        public virtual int PackingColumns { get; set; }
        public virtual int PackingLayers { get; set; }
        public virtual int PackingRows { get; set; }
        public virtual string PalletCaseLayer { get; set; }
        public virtual string PalletGTIN { get; set; }
        public virtual string PalletLayerNumber { get; set; }
        public virtual double PieceWeight { get; set; }
        public virtual PriceListData PriceListData { get; set; }
        public virtual bool Sensormatic { get; set; }
        public virtual bool ShowPackingMethod { get; set; }
        public virtual string Size { get; set; }
        [ReadOnly]
        public virtual string SKU { get; set; }
        public virtual string TagAssembly { get; set; }
        public virtual string UPC { get; set; }
        public virtual string UnitOfMeasure { get; set; }
        public virtual string UnitsOnPallet { get; set; }
        public virtual int UnitsPerCase { get; set; }
        public virtual int UnitsPerPack { get; set; }
        public virtual string Violator { get; set; }
        public virtual string WarningLabel { get; set; }
        public virtual object Clone()
        {
            var clone = (PackSize)MemberwiseClone();
            return clone;
        }
    }

    public sealed class PackSizeMapping : ClassMap<PackSize>
    {
        public PackSizeMapping()
        {
            LazyLoad();
            Table("PackSize");
            Id(p => p.SKU).GeneratedBy.Assigned();
            Map(x => x.Active);
            Map(x => x.BasePartNumber);
            Map(x => x.CT1Cases);
            Map(x => x.CT1GTIN);
            Map(x => x.CT1Height);
            Map(x => x.CT1Length);
            Map(x => x.CT1Width);
            Map(x => x.CaseHeight);
            Map(x => x.CaseLength);
            Map(x => x.CaseWidth);
            Map(x => x.DipType);
            Map(x => x.Display);
            Map(x => x.GoldenSampleLocation);
            Map(x => x.InnerDimensionsOnSpec);
            Map(x => x.InnerPackGTIN);
            Map(x => x.ItemHeight);
            Map(x => x.ItemID);
            Map(x => x.ItemLength);
            Map(x => x.ItemWidth);
            Map(x => x.MasterCaseGTIN);
            Map(x => x.MaterialWeight);
            Map(x => x.NetCaseWeight);
            Map(x => x.PackingColumns);
            Map(x => x.PackingLayers);
            Map(x => x.PackingRows);
            Map(x => x.PalletCaseLayer);
            Map(x => x.PalletGTIN);
            Map(x => x.PalletLayerNumber);
            Map(x => x.PieceWeight);
            Map(x => x.Sensormatic);
            Map(x => x.ShowPackingMethod);
            Map(x => x.Size);
            Map(x => x.TagAssembly);
            Map(x => x.UPC);
            Map(x => x.UnitOfMeasure);
            Map(x => x.UnitsOnPallet);
            Map(x => x.UnitsPerCase);
            Map(x => x.UnitsPerPack);
            Map(x => x.Violator);
            Map(x => x.WarningLabel);

            References(x => x.Item)
                .NotFound.Ignore()
                .Not.Insert()
                .Not.Update()
                .Column("ItemID");

            HasOne(x => x.PriceListData);
        }
    }
}
