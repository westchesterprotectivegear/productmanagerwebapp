﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class FilePM
    {
        [Id]
        public virtual Guid ID { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string Path { get; set; }
    }

    public sealed class FilePMMapping : ClassMap<FilePM>
    {
        public FilePMMapping()
        {
            Table("[File]");
            Id(p => p.ID);
            Map(p => p.BasePartNumber);
            Map(p => p.Path);
        }
    }
}
