﻿using System;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class ImageTransform
    {
        [Id]
        public virtual Guid ID { get; set; }
        public virtual Guid ImageID { get; set; }
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }
        public virtual string FileName { get; set; }
        public virtual ImageTransformType Type { get; set; }
    }

    public sealed class ImageTransformMapping : ClassMap<ImageTransform>
    {
        public ImageTransformMapping()
        {
            Id(p => p.ID);
            Map(p => p.ImageID);
            Map(p => p.Width);
            Map(p => p.Height);
            Map(p => p.FileName);
            Map(p => p.Type).CustomType<ImageTransformType>();
        }
    }

    public enum ImageTransformType
    {
        Generic = 0,
        Original = 1,
        Thumbnail = 2,
        Square = 3
    }
}
