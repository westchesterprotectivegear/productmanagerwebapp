﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Product : ICloneable
    {
        public virtual bool Active { get; set; }
        [CanCopy]
        public virtual bool ActiveIndustrial { get; set; }
        public virtual bool ActiveIroncat { get; set; }
        public virtual bool ActiveRetail { get; set; }
        public virtual string Applications { get; set; }
        public virtual string AssemblyInstructions { get; set; }
        [Id]
        [ReadOnly]
        public virtual string BasePartNumber { get; set; }
        public virtual string Brand { get; set; }
        public virtual string ColorPattern { get; set; }
        public virtual string Components { get; set; }
        public virtual string CountryOfOrigin { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime dtModified { get; set; }
        [CanCopy]
        public virtual string Description { get; set; }
        public virtual string DimensionProfileName { get; set; }
        public virtual string Disclaimer { get; set; }
        public virtual bool Discontinued { get; set; }
        public virtual string Division { get; set; }
        [CanCopy]
        public virtual string DutyRate { get; set; }
        public virtual bool Excess { get; set; }
        public virtual IList<Facet> Facets { get; set; }
        public virtual string GramsMils { get; set; }
        [CanCopy]
        public virtual string HTSNumber { get; set; }
        [CanCopy]
        public virtual string HTSNumberCanada { get; set; }
        public virtual string ImageURL
        {
            get
            {
                return "https://www.westchestergear.com/wp-content/uploads/ProductImages/" + BasePartNumber.Replace("/", "~") + ".png";
            }
        }
        public virtual string InnerPackMarkings { get; set; }
        public virtual string Keywords { get; set; }
        public virtual string LogoArt { get; set; }
        public virtual string MainProductLine { get; set; }
        public virtual string MajorDefects { get; set; }
        [CanCopy]
        public virtual string MarketingCopy { get; set; }
        public virtual string MasterCaseMarkings { get; set; }
        public virtual string MaterialContent { get; set; }
        public virtual string MinimumOrderQuantity { get; set; }
        public virtual string MinorDefects { get; set; }
        [CanCopy]
        public virtual string Name { get; set; }
        public virtual bool NewProductDevelopment { get; set; }
        public virtual bool OutdatedIndustrial { get; set; }
        public virtual bool OutdatedIroncat { get; set; }
        public virtual bool OutdatedRetail { get; set; }
        public virtual bool Prop65LabelRequired { get; set; }
        public virtual string PackagingArtwork { get; set; }
        public virtual string PackageRequired { get; set; }
        public virtual string PackageType { get; set; }
        public virtual bool PackComplete { get; set; }
        public virtual bool PrivateLabel { get; set; }
        public virtual string ProductLine { get; set; }
        public virtual string ProductType { get; set; }
        public virtual string ProductCategory { get; set; }
        public virtual string Prop65Chemicals { get; set; }
        [CanCopy]
        public virtual string QualityStatement { get; set; }
        [CanCopy]
        public virtual string RNNumber { get; set; }
        [CanCopy]
        public virtual string SAPGroupNumber { get; set; }
        public virtual string SAPHierarchy { get; set; }
        public virtual bool Sensormatic { get; set; }
        public virtual string SewingInstructions { get; set; }
        [CanCopy]
        public virtual string SewnInLabel { get; set; }
        public virtual bool SpecComplete { get; set; }
        public virtual string SpecialInstructions { get; set; }
        public virtual string SpecSheetCategory { get; set; }
        public virtual string StitchesPerInch { get; set; }
        public virtual string Testing { get; set; }
        public virtual bool TestProduct { get; set; }
        public virtual string ThreadType { get; set; }
        public virtual string UnitOfMeasure { get; set; }
        public virtual bool UpdatePending { get; set; }
        public virtual string UNSPSCNumber { get; set; }
        [CanCopy]
        public virtual string WashingInstructions { get; set; }
        public virtual string AmazonLink { get; set; }

        public virtual IList<CrossReference> CrossReferences { get; set; } = new List<CrossReference>();
        public virtual IList<Feature> Features { get; set; } = new List<Feature>();
        public virtual IList<FilePM> Files { get; set; } = new List<FilePM>();
        public virtual IList<ProductHistoryItem> History { get; set; } = new List<ProductHistoryItem>();
        public virtual IList<Item> Items { get; set; } = new List<Item>();
        public virtual IList<Photo> Photos { get; set; } = new List<Photo>();
        public virtual DimensionProfile DimensionProfile { get; set; }

        public virtual Photo PrimaryPhoto
        {
            get
            {
                if (Photos.Count > 0 && Photos.Any(x => x.Type == "Primary"))
                    return Photos.First(x => x.Type == "Primary");
                else
                {
                    return new Photo()
                    {
                        Name = "ImageUnavailable.png"
                    };
                }
            }
        }

        public virtual object Clone()
        {
            var clone = (Product)MemberwiseClone();
            return clone;
        }
    }

    public class ProductMapping<T> : ClassMap<T> where T : Product
    {
        public ProductMapping()
        {
            LazyLoad();
            Table("ProductBase");
            Id(p => p.BasePartNumber).GeneratedBy.Assigned();
            Map(p => p.Active);
            Map(p => p.ActiveIndustrial);
            Map(p => p.ActiveIroncat);
            Map(p => p.ActiveRetail);
            Map(p => p.Applications);
            Map(p => p.AssemblyInstructions);
            Map(p => p.Brand);
            Map(p => p.CountryOfOrigin);
            Map(p => p.DateCreated).Generated.Always();
            Map(p => p.Description);
            Map(p => p.DimensionProfileName, "DimensionProfile");
            Map(p => p.Disclaimer);
            Map(p => p.Discontinued);
            Map(p => p.Division);
            Map(p => p.DutyRate);
            Map(p => p.Excess);
            Map(p => p.GramsMils);
            Map(p => p.HTSNumber);
            Map(p => p.HTSNumberCanada);
            Map(p => p.InnerPackMarkings);
            Map(p => p.Keywords);
            Map(p => p.LogoArt);
            Map(p => p.MainProductLine);
            Map(p => p.MajorDefects);
            Map(p => p.MarketingCopy);
            Map(p => p.MasterCaseMarkings);
            Map(p => p.MinimumOrderQuantity);
            Map(p => p.MinorDefects);
            Map(p => p.Name);
            Map(p => p.NewProductDevelopment);
            Map(p => p.PackComplete);
            Map(p => p.PackageRequired);
            Map(p => p.PackageType);
            Map(p => p.PackagingArtwork);
            Map(p => p.PrivateLabel);
            Map(p => p.ProductLine);
            Map(p => p.ProductType);
            Map(p => p.ProductCategory);
            Map(p => p.Prop65Chemicals);
            Map(p => p.Prop65LabelRequired);
            Map(p => p.QualityStatement);
            Map(p => p.RNNumber);
            Map(p => p.SAPGroupNumber);
            Map(p => p.SAPHierarchy);
            Map(p => p.Sensormatic);
            Map(p => p.SpecComplete);
            Map(p => p.SpecialInstructions);
            Map(p => p.TestProduct);
            Map(p => p.Testing);
            Map(p => p.UnitOfMeasure);
            Map(p => p.UpdatePending);
            Map(p => p.WashingInstructions);
            Map(x => x.ColorPattern);
            Map(x => x.Components);
            Map(x => x.MaterialContent);
            Map(x => x.OutdatedIndustrial);
            Map(x => x.OutdatedIroncat);
            Map(x => x.OutdatedRetail);
            Map(x => x.SewingInstructions);
            Map(x => x.SewnInLabel);
            Map(x => x.SpecSheetCategory);
            Map(x => x.StitchesPerInch);
            Map(x => x.ThreadType);
            Map(x => x.UNSPSCNumber);
            Map(x => x.AmazonLink);
            Map(x => x.dtModified);
            HasMany(x => x.CrossReferences)
                .KeyColumn("BasePartNumber").Cascade.AllDeleteOrphan();
            HasMany(x => x.Facets)
                .KeyColumn("BasePartNumber").Cascade.AllDeleteOrphan();
            HasMany(x => x.Features)
                .KeyColumn("BasePartNumber").Cascade.AllDeleteOrphan();
            HasMany(x => x.Files)
                .KeyColumn("BasePartNumber").Cascade.All();
            HasMany(x => x.History)
                .KeyColumn("BasePartNumber").Cascade.All();
            HasMany(x => x.Items)
                .KeyColumn("BasePartNumber").Cascade.All();
            HasMany(x => x.Photos)
                .KeyColumn("BasePartNumber").Cascade.All();
            References(x => x.DimensionProfile)
                .Column("DimensionProfile")
                .Not.Insert()
                .Not.Update();
        }
    }

    public class Glove : Product
    {
        public virtual string BackOfHand { get; set; }
        public virtual string BackOfHandConstruction { get; set; }
        public virtual string Coating { get; set; }
        public virtual string ColorDetail { get; set; }
        public virtual string CuffConstruction { get; set; }
        public virtual string CuffType { get; set; }
        public virtual string DipType { get; set; }
        public virtual string Grade { get; set; }
        public virtual string HemConstruction { get; set; }
        public virtual string KnuckleConstruction { get; set; }
        public virtual string LeatherLocation { get; set; }
        public virtual string LeatherType { get; set; }
        public virtual string Lining { get; set; }
        public virtual string MilGauge { get; set; }
        public virtual string PalmConstruction { get; set; }
        public virtual string PalmStyle { get; set; }
        public virtual string Pattern { get; set; }
        public virtual string QualityLevel { get; set; }
        public virtual string ReinforcedFeatures { get; set; }
        public virtual string Shell { get; set; }
        public virtual string ThumbType { get; set; }
    }

    public class GloveMapping : ProductMapping<Glove>
    {
        public GloveMapping()
        {
            Join("ProductBaseGlove2", m =>
            {
                m.KeyColumn("BasePartNumber");
                m.Map(x => x.BackOfHand);
                m.Map(x => x.BackOfHandConstruction);
                m.Map(x => x.Coating);
                m.Map(x => x.ColorDetail);
                m.Map(x => x.CuffConstruction);
                m.Map(x => x.CuffType);
                m.Map(x => x.DipType);
                m.Map(x => x.Grade);
                m.Map(x => x.HemConstruction);
                m.Map(x => x.KnuckleConstruction);
                m.Map(x => x.LeatherLocation);
                m.Map(x => x.LeatherType);
                m.Map(x => x.Lining);
                m.Map(x => x.MilGauge);
                m.Map(x => x.PalmConstruction);
                m.Map(x => x.PalmStyle);
                m.Map(x => x.Pattern);
                m.Map(x => x.QualityLevel);
                m.Map(x => x.ReinforcedFeatures);
                m.Map(x => x.Shell);
                m.Map(x => x.ThumbType);
            });
        }
    }

    public class Apparel : Product
    {
        public virtual string ANSIGarmentType { get; set; }
        public virtual string ANSIPerformanceClass { get; set; }
        public virtual string BaseMaterial { get; set; }
        public virtual string ButtonType { get; set; }
        public virtual string Closure { get; set; }
        public virtual string Configuration { get; set; }
        public virtual string ElasticType { get; set; }
        public virtual string GarmentLogo { get; set; }
        public virtual string NumberOfPockets { get; set; }
        public virtual string ReflectiveTape { get; set; }
        public virtual string SeamConstruction { get; set; }
        public virtual string StormFlaps { get; set; }
        public virtual string ZipperGrade { get; set; }
    }

    public class ApparelMapping : ProductMapping<Apparel>
    {
        public ApparelMapping()
        {
            Join("ProductBaseClothing2", m =>
            {
                m.KeyColumn("BasePartNumber");
                m.Map(x => x.ANSIGarmentType);
                m.Map(x => x.ANSIPerformanceClass);
                m.Map(x => x.BaseMaterial);
                m.Map(x => x.ButtonType);
                m.Map(x => x.Closure);
                m.Map(x => x.Configuration);
                m.Map(x => x.ElasticType);
                m.Map(x => x.GarmentLogo);
                m.Map(x => x.NumberOfPockets);
                m.Map(x => x.ReflectiveTape);
                m.Map(x => x.SeamConstruction);
                m.Map(x => x.StormFlaps);
                m.Map(x => x.ZipperGrade);
            });
        }
    }

    public class ProductShell
    {
        public virtual string BasePartNumber { get; set; }
        public virtual string DimensionProfileName { get; set; }
    }

    public class ProductShellMapping : ClassMap<ProductShell>
    {
        public ProductShellMapping()
        {
            LazyLoad();
            Table("ProductBase");
            Id(p => p.BasePartNumber).GeneratedBy.Assigned();
            Map(x => x.DimensionProfileName, "DimensionProfile");
        }
    }
}
