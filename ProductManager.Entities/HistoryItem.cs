﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class HistoryItem
    {
        [Id]
        public virtual Guid ID { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string Message { get; set; }
    }

    public class HistoryItemMapping<T> : ClassMap<T> where T : HistoryItem
    {
        public HistoryItemMapping()
        {
            Id(p => p.ID);
            Map(p => p.UserName);
            Map(p => p.Date).Generated.Always();
            Map(p => p.BasePartNumber);
        }
    }

    public class SKUHistoryItem : HistoryItem
    {
        public virtual string SKU { get; set; }
        public virtual string OriginalValue { get; set; }
        public virtual string NewValue { get; set; }
        public virtual string Property { get; set; }
        public override string Message
        {
            get
            {
                return Property + " Changed For " + SKU;
            }
        }
    }

    public sealed class SKUHistoryItemMapping : HistoryItemMapping<SKUHistoryItem>
    {
        public SKUHistoryItemMapping()
        {
            Table("ItemHistory");
            Map(p => p.SKU, "Item");
            Map(p => p.OriginalValue);
            Map(p => p.NewValue);
            Map(p => p.Property);
        }
    }

    public class ProductHistoryItem : HistoryItem
    {
        public virtual string OriginalValue { get; set; }
        public virtual string NewValue { get; set; }
        public virtual string Property { get; set; }
        public override string Message
        {
            get
            {
                return Property + " Changed";
            }
        }
    }

    public sealed class ProductHistoryItemMapping : HistoryItemMapping<ProductHistoryItem>
    {
        public ProductHistoryItemMapping()
        {
            Table("ProductHistory");
            Map(p => p.OriginalValue);
            Map(p => p.NewValue);
            Map(p => p.Property);
        }
    }
}
