﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Note
    {
        public virtual string BasePartNumber { get; set; }
        public virtual string Date { get; set; }
        [Id]
        public virtual Guid ID { get; set; }
        public virtual string Text { get; set; }
        public virtual string Title { get; set; }
        public virtual string User { get; set; }
    }

    public sealed class NoteMapping : ClassMap<Note>
    {
        public NoteMapping()
        {
            Table("ProductNotes");
            Id(p => p.ID, "NoteID");
            Map(p => p.BasePartNumber);
            Map(p => p.Date, "DateAdded").Generated.Always();
            Map(p => p.Text);
            Map(p => p.Title);
            Map(p => p.User, "AddedByUser");
        }
    }
}
