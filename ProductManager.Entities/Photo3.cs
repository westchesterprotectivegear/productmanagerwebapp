﻿using System;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Photo3
    {
        public virtual string AssociatedText { get; set; }
        public virtual string BasePartNumber { get; set; }
        [Id]
        public virtual Guid ID { get; set; }
        public virtual string OriginalName { get; set; }
        public virtual string Type { get; set; }
    }

    public sealed class Photo3Mapping : ClassMap<Photo3>
    {
        public Photo3Mapping()
        {
            Table("ProductPhoto3");
            Id(p => p.ID, "PhotoID");
            Map(p => p.AssociatedText);
            Map(p => p.BasePartNumber);
            Map(p => p.OriginalName);
        }
    }
}
