﻿using System;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class CrossReference
    {
        [Id]
        public virtual Guid ID { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string Company { get; set; }
        public virtual string CrossReferenceNumber { get; set; }
    }

    public sealed class CrossReferenceMapping : ClassMap<CrossReference>
    {
        public CrossReferenceMapping()
        {
            Table("ProductCrossReference");
            Id(p => p.ID, "CrossReferenceID");
            Map(p => p.BasePartNumber);
            Map(p => p.Company);
            Map(p => p.CrossReferenceNumber);
        }
    }
}
