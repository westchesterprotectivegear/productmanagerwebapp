﻿
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Test
    {
        public virtual string one { get; set; }
        public virtual string two { get; set; }
        public virtual string three { get; set; }
        public virtual string four { get; set; }
    }

    public sealed class TestMapping : ClassMap<Test>
    {
        public TestMapping()
        {
            LazyLoad();
            Id(p => p.one).GeneratedBy.Assigned();
            Map(p => p.two);
            Map(p => p.three);
            Map(p => p.four);
        }
    }
}
