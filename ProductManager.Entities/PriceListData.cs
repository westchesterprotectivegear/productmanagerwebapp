﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class PriceListData
    {
        public virtual string SKU { get; set; }
        public virtual string UnitOfMeasure { get; set; }
        public virtual double APrice { get; set; }
        public virtual double CPrice { get; set; }
        public virtual double DPrice { get; set; }
        public virtual double EPrice { get; set; }

        public virtual PackSize PackSize { get; set; }
    }

    public sealed class PriceListDataMapping : ClassMap<PriceListData>
    {
        public PriceListDataMapping()
        {
            Table("IndustrialPriceList");
            Id(p => p.SKU, "Item");
            Map(p => p.UnitOfMeasure, "UM");
            Map(p => p.APrice, "A_Price");
            Map(p => p.CPrice, "C_Price");
            Map(p => p.DPrice, "D_Price");
            Map(p => p.EPrice, "E_Price");
        }
    }
}
