﻿
using System;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Facet
    {
        public virtual string BasePartNumber { get; set; }
        [Id]
        public virtual Guid ID { get; set; }
        public virtual Guid ValueID { get; set; }
        public virtual FacetValue FacetValue { get; set; }
    }

    public sealed class FacetMapping : ClassMap<Facet>
    {
        public FacetMapping()
        {
            LazyLoad();
            Table("ProductBaseFacetedSearch");
            Id(p => p.ID).GeneratedBy.GuidComb();
            Map(p => p.ValueID);
            Map(p => p.BasePartNumber);
            References(x => x.FacetValue, "ValueID")
                .Not.Insert().Not.Update();
        }
    }

    public class FacetValue
    {
        public virtual Guid ID { get; set; }
        public virtual Guid ParentID { get; set; }
        public virtual string Site { get; set; }
        public virtual string Value { get; set; }
        public virtual FacetValue Parent { get; set; }

        public virtual string Category
        {
            get
            {
                if (Parent == null)
                    return "Top";
                else
                    return Parent.Value;
            }
        }
    }

    public sealed class FacetValueMapping : ClassMap<FacetValue>
    {
        public FacetValueMapping()
        {
            LazyLoad();
            Table("ProductFacetedSearchValues");
            Id(p => p.ID).GeneratedBy.GuidComb();
            Map(p => p.ParentID);
            Map(p => p.Site);
            Map(p => p.Value);
            References(x => x.Parent, "ParentID").NotFound.Ignore();
        }
    }
}
