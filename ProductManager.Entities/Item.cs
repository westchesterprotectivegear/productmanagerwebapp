﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Mapping;
using NHibernate.Proxy;

namespace ProductManager.Entities
{
    [Serializable]
    public class Item : ICloneable
    {
        public virtual bool Active { get; set; }
        public virtual Apparel Apparel { get; set; }
        [ReadOnly]
        public virtual string BasePartNumber { get; set; }
        public virtual Glove Glove { get; set; }
        public virtual string HemColor { get; set; }
        public virtual IList<SKUHistoryItem> History { get; set; }
        [Id]
        public virtual Guid ID { get; set; }
        public virtual IList<Measurement> Measurements { get; set; } = new List<Measurement>();
        public virtual IList<PackSize> PackSizes { get; set; }
        public virtual string Size { get; set; }

        public virtual Product Product
        {
            get
            {
                if (Glove != null)
                    return Glove;
                else
                    return Apparel;
            }
        }
        public virtual object Clone()
        {
            var clone = (Item)MemberwiseClone();
            return clone;
        }
    }

    public sealed class ItemMapping : ClassMap<Item>
    {
        public ItemMapping()
        {
            LazyLoad();
            Table("Item2");
            Id(p => p.ID);
            Map(x => x.BasePartNumber);
            Map(x => x.Size);
            Map(x => x.Active);
            Map(x => x.HemColor);

            HasMany(x => x.Measurements)
                .KeyColumn("ItemID").Cascade.AllDeleteOrphan();
            //HasMany(x => x.History)
            //    .KeyColumn("Item");
            HasMany(x => x.PackSizes)
                .KeyColumn("ItemID").Cascade.AllDeleteOrphan();
            References(x => x.Apparel)
                .NotFound.Ignore()
                .Not.Insert()
                .Not.Update()
                .Column("BasePartNumber");
            References(x => x.Glove)
                .NotFound.Ignore()
                .Not.Insert()
                .Not.Update()
                .Column("BasePartNumber");
        }
    }
}
