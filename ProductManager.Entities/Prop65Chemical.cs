﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class Prop65Chemical
    {
        public virtual string Name { get; set; }
        public virtual bool BirthRisk { get; set; }
        public virtual bool CancerRisk { get; set; }
        public virtual string Warning { get; set; }
    }

    public sealed class Prop65ChemicalMapping : ClassMap<Prop65Chemical>
    {
        public Prop65ChemicalMapping()
        {
            Id(p => p.Name);
            Map(p => p.BirthRisk);
            Map(p => p.CancerRisk);
            Map(p => p.Warning);
        }
    }
}
