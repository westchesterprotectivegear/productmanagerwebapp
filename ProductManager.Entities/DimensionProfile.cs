﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace ProductManager.Entities
{
    public class DimensionProfile
    {
        public virtual string Image { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<Dimension> Dimensions { get; set; }
    }

    public sealed class DimensionProfileMapping : ClassMap<DimensionProfile>
    {
        public DimensionProfileMapping()
        {
            Table("ProductDimensionProfilePictures");
            Id(p => p.Name);
            Map(p => p.Image);
            HasMany(x => x.Dimensions)
                .KeyColumn("DimensionProfile")
                .Inverse()
                .Cascade.AllDeleteOrphan();
        }
    }

    public class Dimension
    {
        public virtual Guid ID { get; set; }
        public virtual string ProfileName { get; set; }
        public virtual string Code { get; set; }
        public virtual string Value { get; set; }
        public virtual string Tolerance { get; set; }
        public virtual bool ActiveOnCustomerSpec { get; set; }
        public virtual DimensionProfile Profile { get; set; }
        public virtual IList<Measurement> Measurements { get; set; }
    }

    public sealed class DimensionMapping : ClassMap<Dimension>
    {
        public DimensionMapping()
        {
            Table("ProductDimensionProfiles");
            Id(p => p.ID, "DimensionID");
            Map(p => p.ProfileName, "DimensionProfile");
            Map(p => p.Code, "DimensionCode");
            Map(p => p.Value, "Dimension");
            Map(p => p.Tolerance);
            Map(p => p.ActiveOnCustomerSpec);
            HasMany(x => x.Measurements).KeyColumn("DimensionID");
        }
    }
}
