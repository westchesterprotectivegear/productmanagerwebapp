﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Models;
using ProductManager.Services;

namespace ProductManager.Controllers
{
    [Authorize]
    public class HistoryController : _ApplicationController
    {
        [Inject]
        public IHistoryService historyService { get; set; }

        // GET: History
        public ActionResult Index(string partNumber)
        {
            var history = historyService.GetHistoryForProduct(partNumber);
            var model = Mapper.Map<List<HistoryItemViewModel>>(history);
            return PartialView(model);
        }
        public ActionResult Detail(string id)
        {
            var historyItem = historyService.GetHistoryItem(id);
            var model = Mapper.Map<HistoryDetailViewModel>(historyItem);
            return PartialView("_DetailPartial", model);
        }
    }
}