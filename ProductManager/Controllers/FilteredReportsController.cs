﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.Mvc;
using System.IO;
using AutoMapper;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;
using ProductManager.Services;
using OfficeOpenXml;
using SAP.Entities;

namespace ProductManager.Controllers
{
    [Authorize]
    public class FilteredReportsController : _ApplicationController
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }
        [Inject]
        public IOptionsService optionsService { get; set; }
        [Inject]
        public IImageService imageService { get; set; }
        // GET: FilteredReports
        public ActionResult Index()
        {
            FilteredReportsViewModel filter = new FilteredReportsViewModel();
            filter.BrandOptions = optionsService.GetBrands();
            filter.CountryOfOriginOptions = optionsService.GetCountriesOfOrigin();
            return View(filter);
        }
        [HttpPost]
        public FileResult Index(FilteredReportsViewModel filter)
        {
            var outprod = productRepo.GetAll();
            if (filter.Divisions != null && filter.Divisions.Count > 0 && filter.Divisions[0] != "")
            {
                outprod = outprod.Where(x => filter.Divisions.Contains(x.Division));
            }
            if (filter.ProductTypes != null && filter.ProductTypes.Count > 0 && filter.ProductTypes[0] != "")
            {
                outprod = outprod.Where(x => filter.ProductTypes.Contains(x.ProductType));
            }
            if (filter.ProductCategory != null && filter.ProductCategory.Count > 0 && filter.ProductCategory[0] != "")
            {
                outprod = outprod.Where(x => filter.ProductCategory.Contains(x.ProductCategory));
            }
            if (filter.Brand != null && filter.Brand.Count > 0 && filter.Brand[0] != "")
            {
                outprod = outprod.Where(x => filter.Brand.Contains(x.Brand));
            }
            if (filter.CountryOfOrigin != null && filter.CountryOfOrigin.Count > 0 && filter.CountryOfOrigin[0] != "")
            {
                outprod = outprod.Where(x => filter.CountryOfOrigin.Contains(x.CountryOfOrigin));
            }
            var prodlist = outprod.ToList();
            string filepath = createExcelReport(prodlist, filter);


            return File(filepath, "application/excel", "FilteredReport.xls");
        }

        public string createExcelReport(List<Product> outprod, FilteredReportsViewModel filter)
        {
            string filepath = @"\\fred\PMFiles\filteredReports\" + DateTime.Now.Ticks.ToString() + ".xls";
            string[] filterlist = new string[] { };
            ExcelPackage ex = new ExcelPackage();
            ExcelWorksheet sheet = ex.Workbook.Worksheets.Add("Template");
            int x = 1;
            int y = 1;
            //var length = outprod.Count();
            //var prodlist = outprod.ToList();
            sheet.Cells[x, y].Value = "SKU";
            y++;
            sheet.Cells[x, y].Value = "Base Part Number";
            y++;
            if (filter.SKUList != null)
            {
                filter.SKUList = filter.SKUList.Replace(" ", String.Empty);
                if (filter.SKUList.Contains(","))
                {
                    filterlist = filter.SKUList.Split(',');
                }
                else
                {
                    filterlist = filter.SKUList.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                }
            }
            foreach (var col in filter.SelectedColumns)
            {
                if (col == "Features")
                {
                    sheet.Cells[x, y].Value = "Feature 1";
                    y++;
                    sheet.Cells[x, y].Value = "Feature 2";
                    y++;
                    sheet.Cells[x, y].Value = "Feature 3";
                    y++;
                    sheet.Cells[x, y].Value = "Feature 4";
                    y++;
                    sheet.Cells[x, y].Value = "Feature 5";
                    y++;
                }
                else
                {
                    sheet.Cells[x, y].Value = col;
                    y++;
                }
            }
            y = 1;
            x++;
            foreach (var prod in outprod)
            {
                foreach (var i in prod.Items)
                {
                    foreach (var ps in i.PackSizes)
                    {
                        if (filter.SKUList == null || filterlist.Contains(ps.SKU))
                        {
                            if (filter.SelectedColumns.Contains("Thumbnail"))
                            {
                                if (filter.SKUList != null)
                                {
                                    Image image = null;
                                    string lastImagePath = "";

                                    Photo photo = prod.PrimaryPhoto;

                                    string path = @"\\fred\images\ProductImages\Thumbnails\" + photo.Name;
                                    if (System.IO.File.Exists(path))
                                    {
                                        // just use the last image if it's the same
                                        if (lastImagePath != path || image == null)
                                        {
                                            image = Image.FromFile(path);
                                            image = imageService.ScaleImage(image, 50, 50);
                                        }
                                        var picture = sheet.Drawings.AddPicture(ps.SKU, image);
                                        //sheet.Cells[x, 1].Value = image;
                                        //picture.SetPosition(50 * (x-2) + ((x-1) * 50)-25, 3);
                                        picture.SetPosition(50 * (x - 2) + 30 + ((x - 2) * 12), 3);
                                        lastImagePath = path;
                                    }

                                    sheet.Row(x).Height = 50;
                                    y++;
                                }
                            }
                            sheet.Cells[x, y].Value = ps.SKU;
                            y++;
                            sheet.Cells[x, y].Value = prod.BasePartNumber;
                            y++;
                            foreach (var col in filter.SelectedColumns)
                            {

                                if (col == "Name") { sheet.Cells[x, y].Value = prod.Name; }
                                else if (col == "Size") { sheet.Cells[x, y].Value = ps.Size; }
                                else if (col == "Description") { sheet.Cells[x, y].Value = prod.Description; }
                                else if (col == "Marketing Copy") { sheet.Cells[x, y].Value = prod.MarketingCopy; }
                                else if (col == "Division") { sheet.Cells[x, y].Value = prod.Division; }
                                else if (col == "Product Type") { sheet.Cells[x, y].Value = prod.ProductType; }
                                else if (col == "Product Category") { sheet.Cells[x, y].Value = prod.ProductCategory; }
                                else if (col == "SAP Group Number") { sheet.Cells[x, y].Value = prod.SAPGroupNumber; }
                                else if (col == "Brand") { sheet.Cells[x, y].Value = prod.Brand; }
                                else if (col == "New Product Flag") { sheet.Cells[x, y].Value = prod.NewProductDevelopment; }
                                else if (col == "Private Label Flag") { sheet.Cells[x, y].Value = prod.PrivateLabel; }
                                else if (col == "Discontinued Flag") { sheet.Cells[x, y].Value = prod.Discontinued; }
                                else if (col == "Prop 65") { sheet.Cells[x, y].Value = prod.Prop65Chemicals; }
                                else if (col == "Country of Origin") { sheet.Cells[x, y].Value = prod.CountryOfOrigin; }
                                else if (col == "HTS Number") { sheet.Cells[x, y].Value = prod.HTSNumber; }
                                else if (col == "HTS Number Canada") { sheet.Cells[x, y].Value = prod.HTSNumberCanada; }
                                else if (col == "Duty Rate") { sheet.Cells[x, y].Value = prod.DutyRate; }
                                else if (col == "UNSPSC Number") { sheet.Cells[x, y].Value = prod.UNSPSCNumber; }
                                else if (col == "RN Number") { sheet.Cells[x, y].Value = prod.RNNumber; }
                                else if (col == "SAP Hierarchy") { sheet.Cells[x, y].Value = prod.SAPHierarchy; }
                                else if (col == "Testing") { sheet.Cells[x, y].Value = prod.Testing; }
                                else if (col == "Features")
                                {
                                    for (int z = 0; z < 5; z++)
                                    {
                                        if ((z + 1) < prod.Features.Count)
                                        {
                                            sheet.Cells[x, y].Value = prod.Features[z].Value;
                                        }
                                        y++;
                                    }
                                }
                                else if (col == "Pattern") { sheet.Cells[x, y].Value = prod.ColorPattern; }
                                else if (col == "Coating") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.Coating; } }
                                else if (col == "Thickness/Weight") { sheet.Cells[x, y].Value = prod.GramsMils; }
                                else if (col == "Lining") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.Lining; } }
                                else if (col == "Shell") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.Shell; } }
                                else if (col == "Palm Style Material/Construction") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.PalmStyle; } }
                                else if (col == "Back of Hand Material/Construction") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.BackOfHand; } }
                                else if (col == "Cuff Type") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.CuffType; } }
                                else if (col == "Leather Type") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.LeatherType; } }
                                else if (col == "Leather Location") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.LeatherLocation; } }
                                else if (col == "Grade") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.Grade; } }
                                else if (col == "Quality Level") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.QualityLevel; } }
                                else if (col == "Thumb Type") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.ThumbType; } }
                                else if (col == "Components") { sheet.Cells[x, y].Value = prod.Components; }
                                else if (col == "Material Content") { sheet.Cells[x, y].Value = prod.MaterialContent; }
                                else if (col == "Cuff Construction") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.CuffConstruction; } }
                                else if (col == "Hem Construction") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.HemConstruction; } }
                                else if (col == "Knuckle Construction") { if (i.Glove != null) { sheet.Cells[x, y].Value = i.Glove.KnuckleConstruction; } }
                                else if (col == "Thread Type") { sheet.Cells[x, y].Value = prod.ThreadType; }
                                else if (col == "Stitches Per Inch") { sheet.Cells[x, y].Value = prod.StitchesPerInch; }
                                else if (col == "Color Pattern") { sheet.Cells[x, y].Value = prod.ColorPattern; }
                                else if (col == "Base Material") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.BaseMaterial; } }
                                else if (col == "Number Of Pockets"){ if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.NumberOfPockets; } }
                                else if (col == "Elastic Type") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.ElasticType; } }
                                else if (col == "Seam Construction") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.SeamConstruction; } }
                                else if (col == "Button Type") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.ButtonType; } }
                                else if (col == "Zipper Grade") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.ZipperGrade; } }
                                else if (col == "Closure") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.Closure; } }
                                else if (col == "Storm Flaps") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.StormFlaps; } }
                                else if (col == "Garment Logo") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.GarmentLogo; } }
                                else if (col == "Sewn In Label") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.SewnInLabel; } }
                                else if (col == "Material Content") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.MaterialContent; } }
                                else if (col == "Washing Instructions") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.WashingInstructions; } }
                                else if (col == "Components") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.Components; } }
                                else if (col == "Sewing Instructions") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.SewingInstructions; } }
                                else if (col == "Major Defects") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.MajorDefects; } }
                                else if (col == "Minor Defects") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.MinorDefects; } }
                                else if (col == "Special Instructions") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.SpecialInstructions; } }
                                else if (col == "Quality Statement") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.QualityStatement; } }
                                else if (col == "ANSI Garment Type") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.ANSIGarmentType; } }
                                else if (col == "ANSI Performance Class") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.ANSIPerformanceClass; } }
                                else if (col == "Reflective Tape") { if (i.Apparel != null) { sheet.Cells[x, y].Value = i.Apparel.ReflectiveTape; } }
                                else if (col == "Unit of Measure") { sheet.Cells[x, y].Value = ps.UnitOfMeasure; }
                                else if (col == "Item Length") { sheet.Cells[x, y].Value = ps.ItemLength; }
                                else if (col == "Item Width") { sheet.Cells[x, y].Value = ps.ItemWidth; }
                                else if (col == "Item Height") { sheet.Cells[x, y].Value = ps.ItemHeight; }
                                else if (col == "Item Weight") { sheet.Cells[x, y].Value = ps.PieceWeight; }
                                else if (col == "Golden Sample Location") { sheet.Cells[x, y].Value = ps.GoldenSampleLocation; }
                                else if (col == "Units Per Pack") { sheet.Cells[x, y].Value = ps.UnitsPerPack; }
                                else if (col == "Units Per Case") { sheet.Cells[x, y].Value = ps.UnitsPerCase; }
                                else if (col == "Units Per CT1") { sheet.Cells[x, y].Value = ps.UnitsOnPallet; }
                                else if (col == "Inner Length") { sheet.Cells[x, y].Value = ps.InnerLength; }
                                else if (col == "Inner Width") { sheet.Cells[x, y].Value = ps.InnerWidth; }
                                else if (col == "Inner Height") { sheet.Cells[x, y].Value = ps.InnerHeight; }
                                else if (col == "Inner Volume") { sheet.Cells[x, y].Value = ps.InnerVolume; }
                                else if (col == "Inner Weight") { sheet.Cells[x, y].Value = ps.InnerWeight; }
                                else if (col == "UPC") { sheet.Cells[x, y].Value = ps.UPC; }
                                else if (col == "Inner Pack GTIN") { sheet.Cells[x, y].Value = ps.InnerPackGTIN; }
                                else if (col == "Master Case GTIN") { sheet.Cells[x, y].Value = ps.MasterCaseGTIN; }
                                else if (col == "CT1GTIN") { sheet.Cells[x, y].Value = ps.CT1GTIN; }
                                else if (col == "Pallet GTIN") { sheet.Cells[x, y].Value = ps.PalletGTIN; }
                                else if (col == "Case Length") { sheet.Cells[x, y].Value = ps.CaseLength; }
                                else if (col == "Case Width") { sheet.Cells[x, y].Value = ps.CaseWidth; }
                                else if (col == "Case Height") { sheet.Cells[x, y].Value = ps.CaseHeight; }
                                else if (col == "Case Volume") { sheet.Cells[x, y].Value = ps.CaseVolume; }
                                else if (col == "Net Case Weight") { sheet.Cells[x, y].Value = ps.NetCaseWeight; }
                                else if (col == "Gross Case Weight") { sheet.Cells[x, y].Value = ps.GrossCaseWeight; }
                                else if (col == "CT1 Length") { sheet.Cells[x, y].Value = ps.CT1Length; }
                                else if (col == "CT1 Width") { sheet.Cells[x, y].Value = ps.CT1Width; }
                                else if (col == "CT1 Height") { sheet.Cells[x, y].Value = ps.CT1Height; }
                                else if (col == "CT1 Volume") { sheet.Cells[x, y].Value = ps.CT1Volume; }
                                else if (col == "CT1 Gross Case Weight") { sheet.Cells[x, y].Value = ps.CT1GrossCaseWeight; }
                                else { y--; }
                                y++;
                            }
                            y = 1;
                            x++;
                        }
                    }
                }
            }
            ex.SaveAs(new System.IO.FileInfo(filepath));
            return filepath;
        }
    }
}