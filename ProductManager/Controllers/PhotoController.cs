﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;
using ProductManager.Helpers;
using ProductManager.Models;
using ProductManager.Services;

namespace ProductManager.Controllers
{
    [Authorize]
    public class PhotoController : _ApplicationController
    {
        [Inject]
        public IRepository<Photo, ProductManagerDatabaseContext> photoRepo { get; set; }
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<Photo3, ProductManagerDatabaseContext> photo3Repo { get; set; }
        [Inject]
        public IRepository<ImageUsage, ProductManagerDatabaseContext> usageRepo { get; set; }
        [Inject]
        public IRepository<ImageTransform, ProductManagerDatabaseContext> transRepo { get; set; }
        [Inject]
        public IPhotoService photoService { get; set; }
        [Inject]
        public IImageService imageService { get; set; }

        // GET: Photo
        public ActionResult Index(string partNumber)
        {
            //var allPhotos = photoRepo.GetAll().Take(1000).ToList();

            //foreach (var photo in allPhotos)
            //{
            //    if (!System.IO.File.Exists(@"\\fred\e$\WEBTEST\ProductManager\Content\images\ProductImages\" + photo.Name))
            //        continue;
                
            //    var newPhoto = new Photo3()
            //    {
            //        AssociatedText = photo.AssociatedText,
            //        OriginalName = photo.Name
            //    };

            //    Guid photoID = Guid.Parse(photo3Repo.Create(newPhoto).ToString());

            //    ImageUsageType type = ImageUsageType.Additional;
            //    foreach (ImageUsageType val in Enum.GetValues(typeof(ImageUsageType)))
            //    {
            //        if (val.ToDescriptionString() == photo.Type)
            //            type = val;
            //    }

            //    var usage = new ImageUsage()
            //    {
            //        BasePartNumber = photo.BasePartNumber,
            //        Delta = 0,
            //        ImageID = photoID,
            //        Type = type
            //    };

            //    usageRepo.Create(usage);

            //    var orig = Image.FromFile(@"\\fred\e$\WEBTEST\ProductManager\Content\images\ProductImages\" + photo.Name);
            //    orig.Save(@"\\fleabottom\productmanager\images\original\" + photoID + Path.GetExtension(photo.Name));
            //    var transName = photoID + "_" + orig.Width + "x" + orig.Height + ".jpg";
            //    orig.Save(@"\\fleabottom\productmanager\images\transform\" + transName);

            //    var trans = new ImageTransform()
            //    {
            //        Width = orig.Width,
            //        Height = orig.Height,
            //        FileName = transName,
            //        ImageID = photoID,
            //        Type = ImageTransformType.Original
            //    };

            //    transRepo.Create(trans);

            //    if (type == ImageUsageType.Primary)
            //    {
            //        var thumb = imageService.ScaleImage(orig, 100, 100);
            //        transName = photoID + "_" + thumb.Width + "x" + thumb.Height + ".jpg";
            //        thumb.Save(@"\\fleabottom\productmanager\images\transform\" + transName);

            //        trans = new ImageTransform()
            //        {
            //            Width = thumb.Width,
            //            Height = thumb.Height,
            //            FileName = transName,
            //            ImageID = photoID,
            //            Type = ImageTransformType.Thumbnail
            //        };
            //    }
            //}


            var photos = photoRepo.GetGroup(x => x.BasePartNumber == partNumber).ToList();
            var model = new PhotosViewModel()
            {
                BasePartNumber = partNumber,
                Photos = Mapper.Map<List<PhotoViewModel>>(photos)
            };
            return PartialView(model);
        }

        // POST: Photo/Create
        [HttpPost]
        public ActionResult Create(CreatePhotoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var photo = Mapper.Map<Photo>(model);
                photoService.AddPhoto(model.File.FileName, model.File.InputStream, photo);
                /* var prod = productRepo.Get(model.BasePartNumber);
                prod.dtModified = DateTime.Now;
                productRepo.Update(prod); */
                return RedirectToAction("Index", new { partNumber = model.BasePartNumber });
            }

            return PartialView("_CreatePartial", model);
        }

        [HttpPost]
        public ActionResult CreateImages(HttpPostedFileBase file, string basePart)
        {

            var photo = new Photo();
            photo.BasePartNumber = basePart;
            photo.Created = DateTime.Now;
            photo.Name = file.FileName;
            photo.Type = "Website";
            photoService.AddPhoto(file.FileName, file.InputStream, photo);
         /*   var prod = productRepo.Get(photo.BasePartNumber);
            prod.dtModified = DateTime.Now;
            productRepo.Update(prod); */
            return RedirectToAction("Index", new { partNumber = basePart });
        }

        public ActionResult Detail(string id)
        {
            var photo = photoRepo.Get(Guid.Parse(id));
            var model = Mapper.Map<PhotoDetailViewModel>(photo);
            return PartialView("_DetailPartial", model);
        }

        //[HttpPost]
        //public ActionResult Detail(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // POST: Photo/Delete/5
        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                var photo = photoRepo.Get(Guid.Parse(id));
                photoRepo.Delete(photo);
                return RedirectToAction("Index", new { partNumber = photo.BasePartNumber });
            }
            catch
            {
                return View();
            }
        }
    }
}
