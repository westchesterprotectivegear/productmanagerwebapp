﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;
using ProductManager.Services;

namespace ProductManager.Controllers
{
    [Authorize]
    public class ReportingController : _ApplicationController
    {
        [Inject]
        public IReportingService reportingService { get; set; }
        [Inject]
        public IOptionsService optionsService { get; set; }
        [Inject]
        public IRepository<PackSize, ProductManagerDatabaseContext> packRepo { get; set; }

        public ActionResult DownloadReport()
        {
            var model = new ReportingIndexViewModel();

            // load skus
            foreach (var sku in reportingService.GetSKUs())
            {
                if (!model.SKUs.Keys.Contains(sku))
                    model.SKUs.Add(sku, false);
            }

            // load fields
            foreach (var field in reportingService.GetFields())
            {
                if (!model.Fields.Keys.Contains(field))
                    model.Fields.Add(field, false);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult DownloadReport(ReportingIndexViewModel model)
        {
            // get the checked values
            List<string> skus = (from x in model.SKUs where x.Value select x.Key).ToList();
            List<string> fields = (from x in model.Fields where x.Value select x.Key).ToList();

            // add errors if any and return page
            string error = "";
            if (skus.Count == 0)
                error += "You must select at least one sku.";
            if (fields.Count == 0 && !model.CrossReferencs && !model.Features && !model.Specs)
                error += "You must select at least one field.";

            if (error != "")
            {
                ViewData["error"] = error;
                return View(model);
            }

            var report = reportingService.CreateReport(skus, fields, model.Features, model.CrossReferencs, model.Thumbnails, model.Specs, model.Pricing);

            // write excel bytes to http response
            Response.Clear();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + "PMReport.xlsx");

            Response.BinaryWrite(report);
            Response.End();
            return null;
        }

        [HttpGet]
        public ActionResult DownloadZip()
        {
            return View();
        }

        [HttpPost]
        public void DownloadZip(ZipDownloadViewModel model)
        {
            string imageDir = @"\\fred\Images\ProductImages\";
            string vendorSpecDir = @"\\fred\specs";
            string customerSpecDir = @"\\fred\PMFiles\CustomerSpecs";
            string tempDir = @"\\fred\PMFiles\temp";

            string[] skus = model.SKUs.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            string zipName = Path.Combine(tempDir, Guid.NewGuid().ToString() + ".zip");
            using (var fileStream = new FileStream(zipName, FileMode.CreateNew))
            {
                using (var archive = new ZipArchive(fileStream, ZipArchiveMode.Create, true))
                {
                    foreach (var sku in skus)
                    {
                        var pack = packRepo.Get(sku.ToUpper());

                        if (pack != null)
                        {
                            if (model.VendorSpecs)
                            {
                                string path = Path.Combine(vendorSpecDir, pack.BasePartNumber.Replace("/", "~") + ".pdf");
                                if (System.IO.File.Exists(path))
                                {
                                    var bytes = System.IO.File.ReadAllBytes(path);
                                    var zipArchiveEntry = archive.CreateEntry("VendorSpecs/" + pack.SKU.Replace("/", "~") + Path.GetExtension(path), System.IO.Compression.CompressionLevel.Fastest);
                                    using (var zipStream = zipArchiveEntry.Open())
                                        zipStream.Write(bytes, 0, bytes.Length);
                                }
                            }

                            if (model.CustomerSpecs)
                            {
                                string path = Path.Combine(customerSpecDir, pack.BasePartNumber.Replace("/", "~") + ".pdf");
                                if (System.IO.File.Exists(path))
                                {
                                    var bytes = System.IO.File.ReadAllBytes(path);
                                    var zipArchiveEntry = archive.CreateEntry("CustomerSpecs/" + pack.SKU.Replace("/", "~") + Path.GetExtension(path), System.IO.Compression.CompressionLevel.Fastest);
                                    using (var zipStream = zipArchiveEntry.Open())
                                        zipStream.Write(bytes, 0, bytes.Length);
                                }
                            }

                            if (model.Images)
                            {
                                var image = pack.Item.Product.PrimaryPhoto.Name;

                                string path = Path.Combine(imageDir, image);
                                if (System.IO.File.Exists(path))
                                {
                                    var bytes = System.IO.File.ReadAllBytes(path);
                                    var zipArchiveEntry = archive.CreateEntry("Images/" + pack.SKU.Replace("/", "~") + Path.GetExtension(path), System.IO.Compression.CompressionLevel.Fastest);
                                    using (var zipStream = zipArchiveEntry.Open())
                                        zipStream.Write(bytes, 0, bytes.Length);
                                }
                            }
                        }
                    }
                }
            }

            System.IO.Stream iStream = null;

            // Buffer to read 10K bytes in chunk:
            byte[] buffer = new Byte[10000];

            // Length of the file:
            int length;

            // Total bytes to read:
            long dataToRead;

            // Identify the file to download including its path.
            string filepath = zipName;

            // Identify the file name.
            string filename = System.IO.Path.GetFileName(filepath);

            try
            {
                // Open the file.
                iStream = new System.IO.FileStream(filepath, System.IO.FileMode.Open,
                            System.IO.FileAccess.Read, System.IO.FileShare.Read);


                // Total bytes to read:
                dataToRead = iStream.Length;

                Response.ContentType = MimeMapping.GetMimeMapping(filepath);
                Response.AddHeader("Content-Disposition", "attachment; filename=\"ProductMangerDownload.zip\"");
                Response.AddHeader("Content-Length", iStream.Length.ToString());

                // Read the bytes.
                while (dataToRead > 0)
                {
                    // Verify that the client is connected.
                    if (Response.IsClientConnected)
                    {
                        // Read the data in buffer.
                        length = iStream.Read(buffer, 0, 10000);

                        // Write the data to the current output stream.
                        Response.OutputStream.Write(buffer, 0, length);

                        // Flush the data to the HTML output.
                        Response.Flush();

                        buffer = new Byte[10000];
                        dataToRead = dataToRead - length;
                    }
                    else
                    {
                        //prevent infinite loop if user disconnects
                        dataToRead = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                // Trap the error, if any.
                Response.Write("Error : " + ex.Message);
            }
            finally
            {
                if (iStream != null)
                {
                    //Close the file.
                    iStream.Close();
                }
                Response.Close();

                System.IO.File.Delete(zipName);
            }
        }

        public JsonResult GetPriceList()
        {
            return Json(new { skus = optionsService.GetPriceListSKUs() });
        }

        public ActionResult GetMeasurementReport(string basePart)
        {
            var report = reportingService.MeasurementReport(basePart);

            // write excel bytes to http response
            Response.Clear();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            string fileName = basePart.Replace("/", "~") + "(" + DateTime.Now.ToShortDateString().Replace("/", "-") + ")" + ".xlsx";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

            Response.BinaryWrite(report);
            Response.End();
            return null;
        }
    }
}

static class EPPlusExtensions
{
    public static string ToNonNullString(this object value)
    {
        return value == null ? "" : value.ToString().ToUpper().Trim();
    }
}