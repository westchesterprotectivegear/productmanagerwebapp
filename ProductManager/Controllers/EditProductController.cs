﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;
using ProductManager.Services;
using SAP.Entities;

namespace ProductManager.Controllers
{
    [Authorize]
    public class EditProductController : _ApplicationController
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<FacetValue, ProductManagerDatabaseContext> facetValueRepo { get; set; }
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }
        [Inject]
        public IRepository<Measurement, ProductManagerDatabaseContext> measurementRepo { get; set; }
        [Inject]
        public IRepository<PackSize, ProductManagerDatabaseContext> packRepo { get; set; }
        [Inject]
        public IRepository<DimensionProfile, ProductManagerDatabaseContext> profileRepo { get; set; }
        [Inject]
        public ISAPHierarchyService hierarchyService { get; set; }
        [Inject]
        public IProductService productService { get; set; }
        [Inject]
        public IHistoryService historyService { get; set; }
        [Inject]
        public IOptionsService optionsService { get; set; }
        //[Inject]
        //public IWebsiteUpdateService<IroncatWebsiteDatabaseContext> ironcatWebUpdateService { get; set; }
        //[Inject]
        //public IWebsiteUpdateService<IndustrialWebsiteDatabaseContext> industrialWebUpdateService { get; set; }
        //[Inject]
        //public IWebsiteUpdateService<RetailWebsiteDatabaseContext> retailWebUpdateService { get; set; }

        public ActionResult Index(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);
            var primaryImage = product.Photos.FirstOrDefault(x => x.Type == "Primary");
            var model = new EditProductViewModel()
            {
                BasePartNumber = product.BasePartNumber,
                Created = product.DateCreated,
                Modified = historyService.GetModifiedDate(product.BasePartNumber),
                Sizes = productService.OrderBySize(product.Items.Where(x => x.Active))
                    .Select(x => x.Size).ToList(),
                ImagePath = primaryImage != null ? primaryImage.Name : ""
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult EditProductDetails(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);
            var model = Mapper.Map<EditProductDetailsViewModel>(product);
            model.BrandOptions = optionsService.GetBrands();
            model.ProductLineOptions = optionsService.GetProductLines();
            model.SAPGroupNumberOptions = optionsService.GetSAPGroups();
            model.RNNumberOptions = optionsService.GetRNNumbers();
            model.CountryOfOriginOptions = optionsService.GetCountriesOfOrigin();
            model.Prop65ChemicalOptions = optionsService.GetProp65Chemicals();

            model.CountriesOfOriginSelected = model.CountryOfOrigin.Split(',');
            model.Prop65ChemicalsSelected = model.Prop65Chemicals.Split(',');
            return PartialView("_EditProductDetailsPartial", model);
        }

        [HttpPost]
        public ActionResult EditProductDetails([Bind] EditProductDetailsViewModel model)
        {
            model.CountryOfOrigin = string.Join(",", model.CountriesOfOriginSelected ?? new string[0]);
            model.Prop65Chemicals = string.Join(",", model.Prop65ChemicalsSelected ?? new string[0]);

            var product = productRepo.Get(x => x.BasePartNumber == model.BasePartNumber);
            var origProduct = (Product)product.Clone();
            product.Features.Clear();
            product.CrossReferences.Clear();
            product.dtModified = DateTime.UtcNow;
            Mapper.Map<EditProductDetailsViewModel, Product>(model, product);
            historyService.CreateProductHistory(product, origProduct, (ProductManagerUnitOfWork as UnitOfWork<ProductManagerDatabaseContext>).Session, User.Identity.Name);
            return RedirectToAction("EditProductDetails", new { partNumber = product.BasePartNumber });
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddFeature()
        {
            var model = new EditProductDetailsViewModel();
            model.Features.Add(new FeatureViewModel());

            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddCrossReference()
        {
            var model = new EditProductDetailsViewModel();
            model.CrossReferences.Add(new CrossReferenceViewModel());

            return View(model);
        }

        public ActionResult PackSizePager(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);
            var orderedItems = productService.OrderBySize(product.Items);

            var model = new PackSizePagerViewModel();
            model.BasePartNumber = product.BasePartNumber;
            model.SizeOptions = orderedItems.Select(x => x.Size).ToList();

            foreach (var item in product.Items)
            {
                foreach (var packSize in item.PackSizes)
                {
                    var editor = new PackSizeEditorViewModel();
                    editor.Packaging = Mapper.Map<EditPackSizePackagingViewModel>(packSize);
                    editor.Details = Mapper.Map<EditPackSizeDetailsViewModel>(packSize);
                    editor.Details.UnitOfMeasureOptions = optionsService.GetUnitsOfMeasure();
                    editor.Details.SizeOptions = model.SizeOptions;

                    editor.Packaging.ViolatorOptions = optionsService.GetViolators();
                    editor.Packaging.WarningLabelOptions = optionsService.GetWarningLabels();
                    editor.Packaging.DisplayTypeOptions = optionsService.GetDisplayTypes();
                    editor.Packaging.TagAssemblyOptions = optionsService.GetTagAssemblies();

                    editor.Packaging.ViolatorsSelected = editor.Packaging.Violator.Split(',');
                    editor.Packaging.WarningLabelsSelected = editor.Packaging.WarningLabel.Split(',');
                    editor.Packaging.DisplayTypesSelected = editor.Packaging.Display.Split(',');
                    model.Editors.Add(editor);
                }
            }

            return PartialView("_PackSizePagerPartial", model);
        }

        public ActionResult EditPackSizeDetails(string sku)
        {
            var pack = packRepo.Get(sku);
            var model = Mapper.Map<EditPackSizeDetailsViewModel>(pack);
            model.UnitOfMeasureOptions = optionsService.GetUnitsOfMeasure();
            model.SizeOptions = optionsService.GetSizes();
            return PartialView("_EditPackSizeDetailsPartial", model);
        }

        [HttpPost]
        public ActionResult EditPackSizeDetails(EditPackSizeDetailsViewModel model)
        {
            var pack = packRepo.Get(model.SKU);
            var origPack = (PackSize)pack.Clone();
            Mapper.Map<EditPackSizeDetailsViewModel, PackSize>(model, pack);
            historyService.CreateSKUHistory(pack, origPack, (ProductManagerUnitOfWork as UnitOfWork<ProductManagerDatabaseContext>).Session, User.Identity.Name);
            return RedirectToAction("EditPackSizeDetails", new { sku = pack.SKU });
        }

        public ActionResult EditPackaging(string sku)
        {
            var pack = packRepo.Get(sku);
            var model = Mapper.Map<EditPackSizePackagingViewModel>(pack);
            model.ViolatorOptions = optionsService.GetViolators();
            model.WarningLabelOptions = optionsService.GetWarningLabels();
            model.DisplayTypeOptions = optionsService.GetDisplayTypes();
            model.TagAssemblyOptions = optionsService.GetTagAssemblies();

            model.ViolatorsSelected = model.Violator.Split(',');
            model.WarningLabelsSelected = model.WarningLabel.Split(',');
            model.DisplayTypesSelected = model.Display.Split(',');
            return PartialView("_EditPackSizePackagingPartial", model);
        }

        [HttpPost]
        public ActionResult EditPackaging(EditPackSizePackagingViewModel model)
        {
            model.Violator = string.Join(",", model.ViolatorsSelected ?? new string[0]);
            model.WarningLabel = string.Join(",", model.WarningLabelsSelected ?? new string[0]);
            model.Display = string.Join(",", model.DisplayTypesSelected ?? new string[0]);

            var pack = packRepo.Get(model.SKU);
            //var origItem = (Item)item.Clone();
            Mapper.Map<EditPackSizePackagingViewModel, PackSize>(model, pack);
            //historyService.CreateSKUHistory(item, origItem, (UnitOfWork as UnitOfWork<ProductManagerDatabaseContext>).Session);
            return RedirectToAction("EditPackaging", new { sku = pack.SKU });
        }

        public ActionResult EditWebsitePresence(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);
            
            var model = Mapper.Map<WebsitePresenceViewModel>(product);

            var facetGroups = facetValueRepo.GetGroup(x => x.ParentID != Guid.Empty).GroupBy(x => x.ParentID).Select(x => x.ToList()).ToList();

            foreach (var group in facetGroups)
            {
                var facetGroupModel = new FacetGroupViewModel();
                facetGroupModel.Name = group[0].Category;

                foreach (var facet in group)
                {
                    var facetModel = new WebFacetViewModel()
                    {
                        ID = facet.ID,
                        Value = facet.Value,
                        Selected = product.Facets.Any(x => x.ValueID == facet.ID)
                    };
                    facetGroupModel.Facets.Add(facetModel);
                }

                switch (group[0].Site)
                {
                    case "Industrial":
                        model.IndustrialFacets.Add(facetGroupModel);
                        break;
                    case "Retail":
                        model.RetailFacets.Add(facetGroupModel);
                        break;
                    case "Ironcat":
                        model.IroncatFacets.Add(facetGroupModel);
                        break;
                }
            }
            return PartialView("_WebsitePresencePartial", model);
        }

        [HttpPost]
        public void AddToSite(string partNumber)
        {
            //ironcatWebUpdateService.UpdateProducts(false);
        }

        [HttpPost]
        public ActionResult EditWebsitePresence(WebsitePresenceViewModel model)
        {
            var product = productRepo.Get(x => x.BasePartNumber == model.BasePartNumber);
            product.ActiveIndustrial = model.ActiveIndustrial;

            // remove all old facets
            product.Facets.Clear();

            // combine lists, add new from model
            var allFacets = model.IndustrialFacets.Select(x => x.Facets).ToList();
            allFacets.AddRange(model.RetailFacets.Select(x => x.Facets));
            allFacets.AddRange(model.IroncatFacets.Select(x => x.Facets));
            foreach (var group in allFacets)
            {
                foreach (var item in group.Where(x => x.Selected))
                {
                    product.Facets.Add(new Facet()
                    {
                        BasePartNumber = product.BasePartNumber,
                        ValueID = item.ID
                    });
                }
            }
            product.dtModified = DateTime.UtcNow;
            return RedirectToAction("EditWebsitePresence", new { partNumber = product.BasePartNumber });
        }

        public ActionResult EditConstruction(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);
            if (product is Glove)
            {
                var model = Mapper.Map<EditGloveConstructionViewModel>((Glove)product);
                model.GlovePatternOptions = optionsService.GetGlovePatterns();
                model.CuffTypeOptions = optionsService.GetCuffTypess();
                model.GradeOptions = optionsService.GetGrades();
                model.QualityLevelOptions = optionsService.GetQualityLevels();
                model.ThumbTypeOptions = optionsService.GetThumbTypes();
                return PartialView("_EditGloveConstructionPartial", model);
            }
            else
            {
                var model = Mapper.Map<EditApparelConstructionViewModel>((Apparel)product);
                model.ANSIGarmentTypeOptions = new List<string>() { "", "Type O", "Type P", "Type R" };
                model.ANSIPerformanceClassOptions = new List<string>() { "", "Class I", "Class II", "Class III", "Class E" };
                model.ReflectiveTapeOptions = new List<string>() { "", "1\" silver tape", "2\" silver tape", "2\" two-toned contrast tape, silver with colored border", "2\" silver segmented tape" };
                return PartialView("_EditApparelConstructionPartial", model);
            }
        }

        [HttpPost]
        public ActionResult EditGloveConstruction(EditGloveConstructionViewModel model)
        {
            var product = productRepo.Get(x => x.BasePartNumber == model.BasePartNumber);
            var origProduct = (Product)product.Clone();
            product.dtModified = DateTime.UtcNow;
            Mapper.Map<EditGloveConstructionViewModel, Product>(model, (Glove)product);
            historyService.CreateProductHistory(product, origProduct, (ProductManagerUnitOfWork as UnitOfWork<ProductManagerDatabaseContext>).Session, User.Identity.Name);
            return RedirectToAction("EditConstruction", new { partNumber = model.BasePartNumber });
        }

        [HttpPost]
        public ActionResult EditApparelConstruction(EditApparelConstructionViewModel model)
        {
            var product = productRepo.Get(x => x.BasePartNumber == model.BasePartNumber);
            var origProduct = (Product)product.Clone();
            product.dtModified = DateTime.UtcNow;
            Mapper.Map<EditApparelConstructionViewModel, Product>(model, (Apparel)product);
            historyService.CreateProductHistory(product, origProduct, (ProductManagerUnitOfWork as UnitOfWork<ProductManagerDatabaseContext>).Session, User.Identity.Name);
            return RedirectToAction("EditConstruction", new { partNumber = model.BasePartNumber });
        }

        public ActionResult SAPHierarchyOptions(string root)
        {
            var options = optionsService.GetSAPHierarchyOptions(root);
            var currentHier = hierarchyService.GetHierarchy(root);

            // are there any more options after this selection
            var anyMore = hierarchyService.HasChildren(root);

            // get list of english words representing hierarchy
            var crumbs = new List<string>();
            while (currentHier != null)
            {
                crumbs.Insert(0, currentHier.Text);
                currentHier = hierarchyService.GetHierarchy(currentHier.ParentID);
            }

            var model = new SAPHierarchyViewModel()
            {
                Root = root,
                BreadCrumb = crumbs,
                Options = options
            };
            return Json(new { anyMore = anyMore, view = RenderViewToString("_SAPHierarchyOptionsPartial", model) });
        }

        public ActionResult CreatePackSize(string sku, string size, string basePartNumber)
        {
            var item = itemRepo.Get(x => x.BasePartNumber == basePartNumber && x.Size == size);

            var packSize = new PackSize()
            {
                Size = size,
                SKU = sku.ToUpper(),
                BasePartNumber = basePartNumber,
                ItemID = item.ID
            };

            packRepo.Create(packSize);
            var model = new PackSizeEditorViewModel();
            model.Packaging = Mapper.Map<EditPackSizePackagingViewModel>(packSize);
            model.Details = Mapper.Map<EditPackSizeDetailsViewModel>(packSize);
            model.Details.UnitOfMeasureOptions = optionsService.GetUnitsOfMeasure();
            model.Details.SizeOptions = optionsService.GetSizes();

            model.Packaging.ViolatorOptions = optionsService.GetViolators();
            model.Packaging.WarningLabelOptions = optionsService.GetWarningLabels();
            model.Packaging.DisplayTypeOptions = optionsService.GetDisplayTypes();
            model.Packaging.TagAssemblyOptions = optionsService.GetTagAssemblies();

            model.Packaging.ViolatorsSelected = model.Packaging.Violator.Split(',');
            model.Packaging.WarningLabelsSelected = model.Packaging.WarningLabel.Split(',');
            model.Packaging.DisplayTypesSelected = model.Packaging.Display.Split(',');

            ViewData["index"] = item.PackSizes.Count;

            return RedirectToAction("PackSizePager", new { partNumber = basePartNumber });
        }

        public ActionResult AddSize(string size, string basePartNumber)
        {
            var item = new Item()
            {
                Size = size,
                BasePartNumber = basePartNumber
            };

            itemRepo.Create(item);
            return RedirectToAction("EditProductMeasurements", "EditMeasurement", new { partNumber = basePartNumber });
        }

        [HttpPost]
        public void SetProductChanged(string basePartNumber)
        {
            Product product = productService.Get(basePartNumber);
            product.dtModified = DateTime.UtcNow;
            product.OutdatedIndustrial = true;
            product.OutdatedRetail = true;
            product.OutdatedIroncat = true;
            productRepo.Update(product);
        }
        
        public ActionResult setRetailActive()
        {
            return View();
        }

        [HttpPost]
        public void setRetailActive(string filepath)
        {
            string[] lines = System.IO.File.ReadAllLines(filepath);
            foreach (string line in lines)
            {
                var vals = line.Split(',');
                var prod = productService.Get(vals[0]);
                if (prod is Product) {
                    prod.ActiveRetail = true;
                    prod.Active = true;
                    prod.dtModified = DateTime.UtcNow;
                    productRepo.Update(prod);
                }
            }
        }
    }
}