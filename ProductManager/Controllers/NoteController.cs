﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;

namespace ProductManager.Controllers
{
    [Authorize]
    public class NoteController : _ApplicationController
    {
        [Inject]
        public IRepository<Note, ProductManagerDatabaseContext> noteRepo { get; set; }

        // GET: Note
        public ActionResult Index(string partNumber)
        {
            var notes = noteRepo.GetGroup(x => x.BasePartNumber == partNumber).ToList();
            var model = new NotesViewModel()
            {
                BasePartNumber = partNumber,
                Notes = Mapper.Map<List<NoteViewModel>>(notes)
            };
            return PartialView(model);
        }

        // GET: Note/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Note/Create
        [HttpPost]
        public ActionResult Create(CreateNoteViewModel model)
        {
            if (ModelState.IsValid)
            {
                var note = Mapper.Map<Note>(model);
                note.User = User.Identity.Name;
                noteRepo.Create(note);
                return RedirectToAction("Index", new { partNumber = model.BasePartNumber });
            }

            return Json(new { fail = true, view = RenderViewToString("_CreatePartial", model) });
        }
        
        public ActionResult Detail(string id)
        {
            var note = noteRepo.Get(Guid.Parse(id));
            var model = Mapper.Map<NoteDetailViewModel>(note);
            return PartialView("_DetailPartial", model);
        }
        
        [HttpPost]
        public ActionResult Detail(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Note/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Note/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
