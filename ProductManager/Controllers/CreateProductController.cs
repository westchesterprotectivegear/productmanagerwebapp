﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;
using ProductManager.Services;

namespace ProductManager.Controllers
{
    [Authorize]
    public class CreateProductController : _ApplicationController
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }
        [Inject]
        public IRepository<PackSize, ProductManagerDatabaseContext> packRepo { get; set; }
        [Inject]
        public IRepository<DimensionProfile, ProductManagerDatabaseContext> dimensionProfileRepo { get; set; }
        [Inject]
        public IProductService productService { get; set; }
        [Inject]
        public IOptionsService optionsService { get; set; }
        [Inject]
        public IReportingService reportingService { get; set; }

        private CopyProductModel CopyModel
        {
            get
            {
                if (Session["copyModel"] == null)
                    Session["copyModel"] = new CopyProductModel();
                return Session["copyModel"] as CopyProductModel;
            }
            set { Session["copyModel"] = value; }
        }

        public ActionResult CreateProduct()
        {
            var model = new CreateProductViewModel();
            model.DimensionProfiles = (from x in dimensionProfileRepo.GetAll() select x.Name).OrderBy(x => x).ToList();
            model.Sizes = optionsService.GetSizes().Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => new
                {
                    Key = x,
                    Value = false
                }).ToDictionary(x => x.Key, x => x.Value);

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateProduct(CreateProductViewModel model)
        {
            Product product;
            if (model.ProductType == "Glove")
                product = new Glove();
            else
                product = new Apparel();

            product.BasePartNumber = model.BasePartNumber.ToUpper();
            product.DimensionProfileName = model.DimensionProfile;
            product.UpdatePending = false;
            product.Discontinued = false;
            product.SpecComplete = false;
            product.PackComplete = false;
            product.Sensormatic = false;
            product.Active = false;
            product.dtModified = DateTime.UtcNow;

            productRepo.Create(product);

            foreach (var size in model.Sizes.Where(x => x.Value))
            {
                itemRepo.Create(new Item()
                {
                    Size = size.Key,
                    BasePartNumber = model.BasePartNumber.ToUpper()
                });
            }

            return RedirectToAction("Index", "EditProduct", new { partNumber = model.BasePartNumber });
        }

        public ActionResult CreateProductFromCopy()
        {
            return View();
        }

        public ActionResult CopyStep1()
        {
            var model = GetCopyStep1Model(new CopyStep1ViewModel());
            return PartialView("_CopyStep1", model);
        }

        [HttpPost]
        public ActionResult CopyStep1(CopyStep1ViewModel model)
        {
            var result = new WizardResult();

            if (ModelState.IsValid)
            {
                model.NewBasePartNumber = model.NewBasePartNumber.ToUpper();

                if (!productService.ProductExists(model.NewBasePartNumber))
                {
                    var product = productRepo.Get(x => x.BasePartNumber == model.CopyBasePartNumber);
                    var step2Model = new CopyStep2ViewModel();

                    step2Model.CopySizes = productService.OrderBySize(product.Items)
                        .Select(x => new
                        {
                            Key = x.Size,
                            Value = false
                        }).ToDictionary(x => x.Key, x => x.Value);

                    step2Model.NewSizes = optionsService.GetSizes().Where(x => !string.IsNullOrWhiteSpace(x))
                        .Select(x => new
                        {
                            Key = x,
                            Value = false
                        }).ToDictionary(x => x.Key, x => x.Value);

                    CopyModel.Step1 = model;
                    result.Success = true;
                    result.Page = RenderViewToString("_CopyStep2", step2Model);
                }
                else
                {
                    model = GetCopyStep1Model(model);
                    result.Success = false;
                    result.Message = "The product " + model.NewBasePartNumber + " already exists!";
                    result.Page = RenderViewToString("_CopyStep1", model);
                }
            }
            else
            {
                model = GetCopyStep1Model(model);
                result.Success = false;
                result.Page = RenderViewToString("_CopyStep1", model);
            }

            return Json(result);
        }

        private CopyStep1ViewModel GetCopyStep1Model(CopyStep1ViewModel model)
        {
            model.BasePartNumbers = (from x in productRepo.GetAll() select x.BasePartNumber).OrderBy(x => x).ToList();
            return model;
        }

        public ActionResult CopyStep2()
        {
            var model = new CopyStep2ViewModel();
            model.NewSizes = optionsService.GetSizes().Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => new
                {
                    Key = x,
                    Value = false
                }).ToDictionary(x => x.Key, x => x.Value);

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CopyStep2(CopyStep2ViewModel model)
        {
            var result = new WizardResult();

            if (model.NewSizes.Any(x => x.Value) || model.CopySizes.Any(x => x.Value))
            {
                var step3Model = new CopyStep3ViewModel();
                var product = productService.Get(CopyModel.Step1.CopyBasePartNumber);

                // load fields
                foreach (var field in reportingService.GetCopyableFields(product.GetType()))
                {
                    if (!step3Model.Fields.Keys.Contains(field))
                        step3Model.Fields.Add(field, true);
                }

                CopyModel.Step2 = model;
                result.Success = true;
                result.Page = RenderViewToString("_CopyStep3", step3Model);
                return Json(result);
            }
            else
            {
                result.Success = false;
                result.Message = "Please select at least one size";
                result.Page = RenderViewToString("_CopyStep2", model);
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult CopyStep3(CopyStep3ViewModel model)
        {
            var result = new WizardResult();

            // add confirmation values
            var step4Model = new CopyStep4ViewModel();
            step4Model.BasePartNumber = CopyModel.Step1.NewBasePartNumber;
            step4Model.CopyBasePartNumber = CopyModel.Step1.CopyBasePartNumber;
            step4Model.FieldCount = model.Fields.Where(x => x.Value).Count();

            var copySizes = CopyModel.Step2.CopySizes
                .Where(x => x.Value)
                .Select(x => x.Key).ToList();
            step4Model.Sizes = copySizes;
            step4Model.Sizes.AddRange(CopyModel.Step2.NewSizes
                .Where(x => x.Value)
                .Select(x => x.Key));
            step4Model.Sizes = productService.OrderBySize(step4Model.Sizes);

            CopyModel.Step3 = model;
            result.Success = true;
            result.Page = RenderViewToString("_CopyStep4", step4Model);
            return Json(result);
        }

        [HttpPost]
        public ActionResult CopyStep4(CopyStep4ViewModel model)
        {
            var ignoreProps = CopyModel.Step3.Fields
                .Where(x => !x.Value)
                .Select(x => x.Key).ToList();

            var clone = productService.Copy(model.CopyBasePartNumber, model.BasePartNumber, ignoreProps);

            clone.BasePartNumber = model.BasePartNumber;

            if (!CopyModel.Step3.CrossReferencs)
                clone.CrossReferences.Clear();
            if (!CopyModel.Step3.Features)
                clone.Features.Clear();

            // remove 
            List<Item> removeItems = new List<Item>();
            for (int i = 0; i < clone.Items.Count; i++)
            {
                var item = clone.Items[i];

                if (!CopyModel.Step3.Measurements)
                    item.Measurements.Clear();

                if (!CopyModel.Step2.CopySizes[item.Size])
                    removeItems.Add(item);
            }

            foreach (var item in removeItems)
            {
                clone.Items.Remove(item);
            }

            foreach (var size in CopyModel.Step2.NewSizes.Where(x => x.Value))
            {
                clone.Items.Add(new Item()
                {
                    BasePartNumber = clone.BasePartNumber,
                    Size = size.Key
                });
            }

            productRepo.Create(clone);

            return RedirectToAction("Index", "EditProduct", new { partNumber = clone.BasePartNumber });
        }

        [HttpPost]
        public ActionResult ProductExists(string partNumber)
        {
            var result = productRepo.Get(x => x.BasePartNumber == partNumber) != null;
            return Json(new { exists = result });
        }
    }
}