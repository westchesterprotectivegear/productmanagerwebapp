﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;

namespace ProductManager.Controllers
{
    [Authorize]
    public class FileController : _ApplicationController
    {
        private const string TempPath = @"\\fred\pmfiles";

        [Inject]
        public IRepository<FilePM, ProductManagerDatabaseContext> fileRepo { get; set; }

        // GET: File
        public ActionResult Index(string partNumber)
        {
            var files = fileRepo.GetGroup(x => x.BasePartNumber == partNumber).AsEnumerable();
            return PartialView(files);
        }

        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase file, string basePart)
        {
            System.IO.FileInfo fileInf = new System.IO.FileInfo(Path.Combine(TempPath, basePart.Replace("/", "~"), Uri.UnescapeDataString(file.FileName)));
            fileInf.Directory.Create();
            System.IO.File.WriteAllBytes(fileInf.FullName, ReadData(file.InputStream));

            try
            {
                fileRepo.Create(new FilePM()
                {
                    BasePartNumber = basePart,
                    Path = fileInf.FullName
                });

                return RedirectToAction("Index", new { partNumber = basePart });
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult Download(string id)
        {
            var file = fileRepo.Get(Guid.Parse(id));

            try
            {
                string fileName = Path.Combine(TempPath, file.Path);
                return File(fileName, MimeMapping.GetMimeMapping(fileName), file.Path);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            var file = fileRepo.Get(Guid.Parse(id));
            System.IO.FileInfo fileInf = new System.IO.FileInfo(Path.Combine(TempPath, file.Path));

            try
            {
                fileRepo.Delete(file);
                fileInf.Delete();

                return RedirectToAction("Index", new { partNumber = file.BasePartNumber });
            }
            catch (Exception)
            {

                throw;
            }
        }

        private byte[] ReadData(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];

            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }
    }
}