﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;

namespace ProductManager.Controllers
{
    [Authorize]
    public class DimensionProfileController : _ApplicationController
    {
        [Inject]
        public IRepository<DimensionProfile, ProductManagerDatabaseContext> dimensionProfileRepo { get; set; }
        [Inject]
        public IRepository<Dimension, ProductManagerDatabaseContext> dimensionRepo { get; set; }

        // GET: CreateProduct
        public ActionResult Index()
        {
            var model = dimensionProfileRepo.GetAll().OrderBy(x => x.Name).AsEnumerable().ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new DimensionProfileViewModel();
            model.Dimensions.Add(new EditDimensionViewModel());
            return View(model);
        }

        [HttpPost]
        public ActionResult Create([Bind] DimensionProfileViewModel model)
        {
            var profile = Mapper.Map<DimensionProfile>(model);
            profile.Image = "";
            foreach (var dim in profile.Dimensions)
            {
                dim.ProfileName = profile.Name;
            }
            dimensionProfileRepo.Create(profile);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(string name)
        {
            var profile = dimensionProfileRepo.Get(name);
            var model = Mapper.Map<DimensionProfileViewModel>(profile);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit([Bind] DimensionProfileViewModel model)
        {
            var profile = dimensionProfileRepo.Get(model.Name);

            // remove or update current dimensions
            for (int i = 0; i < profile.Dimensions.Count; i++)
            {
                var dim = profile.Dimensions[i];
                var match = model.Dimensions.FirstOrDefault(x => x.ID == dim.ID);
                if (match != null)
                {
                    Mapper.Map<EditDimensionViewModel, Dimension>(match, dim);
            //        model.Dimensions.Remove(match);
                }
                else
                    profile.Dimensions.Remove(dim);
            }
           
            var newdimensions = model.Dimensions.Where(x => !profile.Dimensions.Any(y => x.ID == y.ID)).ToList();
            // add others
            foreach (var modelDim in newdimensions)
            {
                var newDim = Mapper.Map<Dimension>(modelDim);
                profile.Dimensions.Add(newDim);
            }
            
            model = Mapper.Map<DimensionProfileViewModel>(profile);

            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddDimension(string profileName)
        {
            var profile = new DimensionProfileViewModel();
            profile.Dimensions.Add(new EditDimensionViewModel()
            {
                ProfileName = profileName
            });

            return View(profile);
        }

        [HttpGet]
        public int CheckDimensionOrphans(string id)
        {
            var dimension = dimensionRepo.Get(Guid.Parse(id));
            var count = (from x in dimension.Measurements select x.SKU).Distinct().Count();
            return count;
        }
        
        public ActionResult ProfileImage(string name)
        {
            var profile = dimensionProfileRepo.Get(name);
            var model = Mapper.Map<ProfileImageViewModel>(profile);
            return PartialView("_ProfileImagePartial", model);
        }

        [HttpPost]
        public ActionResult UpdateProfilePhoto(HttpPostedFileBase file, string Name)
        {
            System.IO.FileInfo fileInf = new System.IO.FileInfo(Path.Combine(@"\\fred\images\DimensionProfileImages\", Uri.UnescapeDataString(file.FileName)));
            System.IO.File.WriteAllBytes(fileInf.FullName, ReadData(file.InputStream));

            var profile = dimensionProfileRepo.Get(Name);
            profile.Image = file.FileName;

            return RedirectToAction("ProfileImage", new { name = profile.Name });
        }

        public JsonResult DimensionList(string profileName)
        {
            var profile = dimensionProfileRepo.Get(profileName);
            var dimensions = profile.Dimensions
                .OrderBy(x => x.Code)
                .Select(x => new
                    {
                        ID = x.ID,
                        Name = x.Value
                    });
            return Json(dimensions, JsonRequestBehavior.AllowGet);
        }
    }
}