﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;
using ProductManager.Services;

namespace ProductManager.Controllers
{
    public class SpecSheetController : Controller
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IVendorSpecService vendorSpecService { get; set; }
        [Inject]
        public ICustomerSpecService customerSpecService { get; set; }

        private string vendorSpecDir = @"\\fred\specs";
        private string customerSpecDir = @"\\fred\pmfiles\CustomerSpecs";

        // GET: SpecSheet
        public ActionResult Index(string partNumber)
        {
            string vFileName = Path.Combine(vendorSpecDir, partNumber.Replace("/", "~") + ".pdf");
            string cFileName = Path.Combine(customerSpecDir, partNumber.Replace("/", "~") + ".pdf");

            var model = new SpecSheetViewModel();
            model.BasePartNumber = partNumber;
            model.VendorSpecExists = System.IO.File.Exists(vFileName);
            model.CustomerSpecExists = System.IO.File.Exists(cFileName);
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Generate(SpecSheetViewModel model)
        {
            try
            {
                if (model.Type == "Vendor")
                    vendorSpecService.Generate(model.BasePartNumber);
                else
                    customerSpecService.GenerateSpec(model.BasePartNumber);
            }
            catch (Exception ex)
            {
                EmailService emailService = new EmailService();
                MailMessage message = new MailMessage();
                message.Subject = "Spec Sheet Error - " + model.BasePartNumber;
                message.Body = ex.Message + "\n\n" + ex.StackTrace;
                message.To.Add("mmoore@westchestergear.com");
                emailService.Send(message);
            }

            return RedirectToAction("Index", new { partNumber = model.BasePartNumber });
        }

        [HttpPost]
        public void GenerateOutdated()
        {
            vendorSpecService.GenerateOutdated();
            customerSpecService.GenerateOutdated();
        }

        [HttpPost]
        public FileResult Download(SpecSheetViewModel model)
        {
            try
            {
                string fileName = "";
                string friendlyPartNumber = model.BasePartNumber.Replace("/", "~") + ".pdf";
                if (model.Type == "Vendor")
                    fileName = Path.Combine(vendorSpecDir, friendlyPartNumber);
                else
                    fileName = Path.Combine(customerSpecDir, friendlyPartNumber);

                string mime = MimeMapping.GetMimeMapping(fileName);
                return File(fileName, "application/pdf", friendlyPartNumber);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}