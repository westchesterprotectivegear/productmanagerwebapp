﻿using System;
using System.IO;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using ProductManager.Entities;

namespace ProductManager.Controllers
{
    public class _ApplicationController : Controller
    {
        [Inject]
        public IUnitOfWork<ProductManagerDatabaseContext> ProductManagerUnitOfWork { get; set; }
        [Inject]
        public IRepository<ProductManagerUser, ProductManagerDatabaseContext> userRepo { get; set; }
        [Inject]
        public IMapper Mapper { get; set; }

        public ProductManagerUser GetUser()
        {
            return (ProductManagerUser)Session["user"];
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (!filterContext.IsChildAction)
                {
                    ProductManagerUnitOfWork.BeginTransaction();
                }

                if (User.Identity.IsAuthenticated)
                {
                    if (Session["user"] == null)
                        Session["user"] = userRepo.Get(x => x.UserName == User.Identity.Name);
                }
            }
            catch (Exception e)
            {
                Session.Clear();
                Redirect("/Account/LogOff");
            }
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            try
            {
                if (!filterContext.IsChildAction)
                {
                    ProductManagerUnitOfWork.Commit();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public string RenderViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public byte[] ReadData(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];

            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }
    }
}