﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;

namespace ProductManager.Controllers
{
    [Authorize]
    public class TechPackController : _ApplicationController
    {
        [Inject]
        public IRepository<CollectionValue, ProductManagerDatabaseContext> collectionRepo { get; set; }
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }

        // GET: TechPack
        public ActionResult Index(string sku)
        {
            var item = itemRepo.Get(sku);
            var model = new TechPackDesignViewModel();
            model.IncludeItemOnSpec = item.Active;
            model.Violators = collectionRepo.GetGroup(x => x.CollectionID == Guid.Parse("2C5E12B3-A1C7-4C27-8E97-838A6FDC7DF4"))
                .ToDictionary(x => x.Value, x => false);
            model.WarningLabels = collectionRepo.GetGroup(x => x.CollectionID == Guid.Parse("651362BA-46FD-40F2-84CC-9A9A1AD8A871"))
                .ToDictionary(x => x.Value, x => false);
            return PartialView(model);
        }
    }
}