﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;

namespace ProductManager.Controllers
{
    [Authorize]
    public class CollectionController : _ApplicationController
    {
        [Inject]
        public IRepository<Collection, ProductManagerDatabaseContext> collectionRepo { get; set; }

        // GET: Collection
        public ActionResult Index()
        {
            var collections = collectionRepo.GetAll();
            var model = Mapper.Map<IEnumerable<EditCollectionViewModel>>(collections);
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var collection = collectionRepo.Get(Guid.Parse(id));
            var model = Mapper.Map<EditCollectionViewModel>(collection);
            model.Values = model.Values.OrderBy(x => x.CollectionIndex).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit([Bind] EditCollectionViewModel model)
        {
            var collection = collectionRepo.Get(model.ID);
            collection.Values.Clear();
            foreach (var value in model.Values)
            {
                var newValue = Mapper.Map<CollectionValue>(value);
                newValue.CollectionID = collection.ID;
                collection.Values.Add(newValue);
            }
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddValue()
        {
            var collection = new EditCollectionViewModel();
            collection.Values.Add(new EditCollectionValueViewModel());

            return View(collection);
        }
    }
}