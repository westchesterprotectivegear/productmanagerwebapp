﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;
using ProductManager.Services;

namespace ProductManager.Controllers
{
    [Authorize]
    public class MassEditController : _ApplicationController
    {
        [Inject]
        public IReportingService reportingService { get; set; }
        [Inject]
        public IMassEditService massEditService { get; set; }
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }

        // GET: Reporting
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TemplateBuilder()
        {
            var model = new TemplateBuilderViewModel();

            // load skus
            foreach (var sku in reportingService.GetSKUs())
            {
                if (!model.SKUs.Keys.Contains(sku))
                    model.SKUs.Add(sku, false);
            }

            // load fields
            foreach (var field in reportingService.GetFields())
            {
                if (!model.Fields.Keys.Contains(field))
                    model.Fields.Add(field, false);
            }

            return View(model);
        }

        [HttpPost]
        public void TemplateBuilder(TemplateBuilderViewModel model)
        {
            // get the checked values
            List<string> skus = (from x in model.SKUs where x.Value select x.Key).ToList();
            List<string> fields = (from x in model.Fields where x.Value select x.Key).ToList();

            var template = massEditService.CreateUploadTemplate(skus, fields);

            // write excel bytes to http response
            Response.Clear();

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + "template.xlsx");

            Response.BinaryWrite(template);
            Response.End();
        }

        [HttpGet]
        public ActionResult UploadData()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadData(HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                MemoryStream target = new MemoryStream();
                file.InputStream.CopyTo(target);
                massEditService.UploadData(target);
            }

            return View();
        }
    }
}