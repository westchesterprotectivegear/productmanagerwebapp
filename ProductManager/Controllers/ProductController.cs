﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.Mvc;
using System.IO;
using System.Web.Services;
using AutoMapper;
using Ninject;
using ProductManager.Entities;
using ProductManager.Services;
using ProductManager.Models;
using OfficeOpenXml;
using System.Data.SqlClient;
using System.Configuration;

namespace ProductManager.Controllers
{
    [Authorize]
    public class ProductController : _ApplicationController
    {
        [Inject]
        public IRepository<ProductShell, ProductManagerDatabaseContext> productShellRepo { get; set; }
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IImageService imageService { get; set; }
        public ActionResult Index()
        {
            var products = productShellRepo.GetAll().OrderBy(x => x.BasePartNumber).ToList();
            return View(products);
        }

        public ActionResult AutocompleteProduct(string term)
        {
            var products = productShellRepo.GetAll().Select(x => x.BasePartNumber).OrderBy(x => x).ToArray();

            var filteredItems = products.Where(x => x.IndexOf(term, StringComparison.InvariantCultureIgnoreCase) >= 0);
            return Json(filteredItems, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BulkEdit()
        {
            return View();
        }
        public ActionResult MassCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BulkEdit(ProductMassCreateViewModel file)
        {
            try
            {
                if (file.outfile.ContentLength > 0)
                {
                     List<string> failures = EditProducts(file);
                     ViewBag.Message = "File Processed Successfully<br />";
                     ViewBag.Message += "The Following Part Numbers Failed:<br />";
                     foreach (string failure in failures)
                     {
                         ViewBag.Message += failure + "<br />";
                     }
                }
            }
            catch (Exception ex)
            {

                ViewBag.Message = ex.Message + " - " + ex.StackTrace;
            }
            return View();
        }

        [HttpPost]
        public string GetBulkTemplate()
        {
            string filepath = @"\\fred\PMFiles\BulkEditTemplate.xls";
            ExcelPackage ex = new ExcelPackage();
            ExcelWorksheet sheet = ex.Workbook.Worksheets.Add("Template");
            var prod = productRepo.GetAll().ToList();
            int x = 1;
            int y = 6;
            sheet.Cells[x, 1].Value = "Base Part Number";
            sheet.Cells[x, 2].Value = "Name";
            sheet.Cells[x, 3].Value = "Brand";
            sheet.Cells[x, 4].Value = "Material Group";
            sheet.Cells[x, 5].Value = "Description";
            sheet.Cells[x, 6].Value = "Marketing Copy";
            sheet.Cells[x, 7].Value = "Division";
            sheet.Cells[x, 8].Value = "Product Type";
            sheet.Cells[x, 9].Value = "Product Category";
            sheet.Cells[x, 10].Value = "Features 1";
            sheet.Cells[x, 11].Value = "Features 2";
            sheet.Cells[x, 12].Value = "Features 3";
            sheet.Cells[x, 13].Value = "Features 4";
            sheet.Cells[x, 14].Value = "Features 5";
            sheet.Cells[x, 15].Value = "Country of Origin";

            x++;
            foreach (Product p in prod)
            {
                sheet.Cells[x, 1].Value = p.BasePartNumber;
                sheet.Cells[x, 2].Value = p.Name;
                sheet.Cells[x, 3].Value = p.Brand;
                sheet.Cells[x, 4].Value = p.SAPGroupNumber;
                sheet.Cells[x, 5].Value = p.Description;
                sheet.Cells[x, 6].Value = p.MarketingCopy;
                sheet.Cells[x, 7].Value = p.Division;
                sheet.Cells[x, 8].Value = p.ProductType;
                sheet.Cells[x, 9].Value = p.ProductCategory;
                y = 10;
                foreach (var feat in p.Features)
                {
                    if (y < 15)
                    {
                        sheet.Cells[x, y].Value = feat.Value;
                        y++;
                    }
                }
                sheet.Cells[x, 15].Value = p.CountryOfOrigin;
                x++;
            }
            ex.SaveAs(new System.IO.FileInfo(filepath));
            return filepath;
        }

        [HttpPost]
        public string GetBulkTemplateBySKU()
        {
            string filepath = @"\\fred\PMFiles\BulkEditTemplateBySKU.xls";
            ExcelPackage ex = new ExcelPackage();
            ExcelWorksheet sheet = ex.Workbook.Worksheets.Add("Template");
            var prod =itemRepo.GetAll().ToList();
            int x = 1;
            int y = 6;
            sheet.Cells[x, 1].Value = "Base Part Number";
            sheet.Cells[x, 2].Value = "Name";
            sheet.Cells[x, 3].Value = "Description";
            sheet.Cells[x, 4].Value = "Marketing Copy";
            sheet.Cells[x, 5].Value = "Brand";
            sheet.Cells[x, 6].Value = "Division";
            sheet.Cells[x, 7].Value = "Product Type";
            sheet.Cells[x, 8].Value = "Product Category";
            sheet.Cells[x, 9].Value = "Dimension Profile";
            sheet.Cells[x, 10].Value = "Features 1";
            sheet.Cells[x, 11].Value = "Features 2";
            sheet.Cells[x, 12].Value = "Features 3";
            sheet.Cells[x, 13].Value = "Features 4";
            sheet.Cells[x, 14].Value = "Features 5";
            sheet.Cells[x, 15].Value = "Icon";
            sheet.Cells[x, 16].Value = "Size Range";
            sheet.Cells[x, 17].Value = "New?";
            sheet.Cells[x, 18].Value = "Photo Needed";
            x++;
            sheet.Cells[x, 2].Value = "SKU";
            sheet.Cells[x, 3].Value = "Size";
            sheet.Cells[x, 4].Value = "Item Length";
            sheet.Cells[x, 5].Value = "Item Width";
            sheet.Cells[x, 6].Value = "Item Height";
            sheet.Cells[x, 7].Value = "Inner Height";
            sheet.Cells[x, 8].Value = "Inner Width";
            sheet.Cells[x, 9].Value = "Inner Length";
            sheet.Cells[x, 10].Value = "Case Length";
            sheet.Cells[x, 11].Value = "Case Width";
            sheet.Cells[x, 12].Value = "Case Height";
            sheet.Cells[x, 13].Value = "Golden Sample Location";
            x++;

            foreach (Item i in prod)
            {
                var p = i.Product;
                if (p!=null)
                {
                    sheet.Cells[x, 1].Value = i.BasePartNumber;
                    sheet.Cells[x, 2].Value = p.Name;
                    sheet.Cells[x, 3].Value = p.Description;
                    sheet.Cells[x, 4].Value = p.MarketingCopy;
                    sheet.Cells[x, 5].Value = p.Brand;
                    sheet.Cells[x, 6].Value = p.Division;
                    sheet.Cells[x, 7].Value = p.ProductType;
                    sheet.Cells[x, 8].Value = p.ProductCategory;
                    sheet.Cells[x, 9].Value = p.DimensionProfileName;
                    y = 10;
                    foreach (var feat in p.Features)
                    {
                        if (y < 15)
                        {
                            sheet.Cells[x, y].Value = feat.Value;
                            y++;
                        }
                    }
                    sheet.Cells[x, 15].Value = "";
                    sheet.Cells[x, 16].Value = "";
                    sheet.Cells[x, 17].Value = (p.NewProductDevelopment ? "Yes" : "No");
                    sheet.Cells[x, 18].Value = "";
                    x++;
                    foreach (var ps in i.PackSizes)
                    {
                        sheet.Cells[x, 2].Value = ps.SKU;
                        sheet.Cells[x, 3].Value = ps.Size;
                        sheet.Cells[x, 4].Value = ps.ItemLength;
                        sheet.Cells[x, 5].Value = ps.ItemWidth;
                        sheet.Cells[x, 6].Value = ps.ItemHeight;
                        sheet.Cells[x, 7].Value = ps.InnerHeight;
                        sheet.Cells[x, 8].Value = ps.InnerWidth;
                        sheet.Cells[x, 9].Value = ps.InnerLength;
                        sheet.Cells[x, 10].Value = ps.CaseLength;
                        sheet.Cells[x, 11].Value = ps.CaseWidth;
                        sheet.Cells[x, 12].Value = ps.CaseHeight;
                        sheet.Cells[x, 13].Value = ps.GoldenSampleLocation;
                        x++;
                    }
                }

            }

            ex.SaveAs(new System.IO.FileInfo(filepath));
            return filepath;
        }









        [HttpPost]
        public string GetBulkTemplateBySKURedux()
        {
            string filepath = @"\\fred\PMFiles\BulkEditTemplateBySKUNoGroup.xls";
            ExcelPackage ex = new ExcelPackage();
            ExcelWorksheet sheet = ex.Workbook.Worksheets.Add("Template");
            var prod = itemRepo.GetAll().ToList();
            int x = 1;
            int y = 6;
            sheet.Cells[x, 2].Value = "SKU";
            sheet.Cells[x, 3].Value = "Size";
            sheet.Cells[x, 4].Value = "Base Part Number";
            sheet.Cells[x, 5].Value =  "Name";
            sheet.Cells[x, 6].Value = "Item Length";
            sheet.Cells[x, 7].Value = "Item Width";
            sheet.Cells[x, 8].Value = "Item Height";
            sheet.Cells[x, 9].Value = "Inner Height";
            sheet.Cells[x, 10].Value = "Inner Width";
            sheet.Cells[x, 11].Value = "Inner Length";
            sheet.Cells[x, 12].Value = "Case Length";
            sheet.Cells[x, 13].Value = "Case Width";
            sheet.Cells[x, 14].Value = "Case Height";
            sheet.Cells[x, 15].Value = "Golden Sample";
            sheet.Cells[x, 16].Value = "Description";
            sheet.Cells[x, 17].Value = "Brand";
            sheet.Cells[x, 18].Value = "Division";
            sheet.Cells[x, 19].Value = "Product Type";
            sheet.Cells[x, 20].Value = "Product Category";
            sheet.Cells[x, 21].Value = "Dimension Profile";
            sheet.Cells[x, 22].Value = "UPC";
            sheet.Cells[x, 23].Value = "Feature 1";
            sheet.Cells[x, 24].Value = "Feature 2";
            sheet.Cells[x, 25].Value = "Feature 3";
            sheet.Cells[x, 26].Value = "Feature 4";
            sheet.Cells[x, 27].Value = "Feature 5";
            sheet.Cells[x, 28].Value = "Units Per Pack";
            sheet.Cells[x, 29].Value = "Units Per Case";
            sheet.Cells[x, 30].Value = "Unit Of Measure";
            x++;

            foreach (Item i in prod)
            {
                if (i.Product != null)
                {
                    foreach (var ps in i.PackSizes)
                    {
                        Image image = null;
                        Photo photo = i.Product.PrimaryPhoto;
                        string lastImagePath="";
                            string path = @"\\fred\images\ProductImages\Thumbnails\" + photo.Name;
                            if (System.IO.File.Exists(path))
                            {
                                // just use the last image if it's the same
                                if (lastImagePath != path || image == null)
                                {
                                    image = Image.FromFile(path);
                                    image = imageService.ScaleImage(image, 50, 50);
                                }
                                   var picture = sheet.Drawings.AddPicture(ps.SKU, image);
                                    picture.SetPosition(50 * x + 25 + (x * 16), 0);
                            }
                        lastImagePath = path;

                            sheet.Row(x).Height = 50;


                        sheet.Cells[x, 2].Value = ps.SKU;
                        sheet.Cells[x, 3].Value = ps.Size;
                        sheet.Cells[x, 4].Value = i.BasePartNumber;
                        sheet.Cells[x, 5].Value = i.Product.Name;
                        sheet.Cells[x, 6].Value = ps.ItemLength;
                        sheet.Cells[x, 7].Value = ps.ItemWidth;
                        sheet.Cells[x, 8].Value = ps.ItemHeight;
                        sheet.Cells[x, 9].Value = ps.InnerHeight;
                        sheet.Cells[x, 10].Value = ps.InnerWidth;
                        sheet.Cells[x, 11].Value = ps.InnerLength;
                        sheet.Cells[x, 12].Value = ps.CaseLength;
                        sheet.Cells[x, 13].Value = ps.CaseWidth;
                        sheet.Cells[x, 14].Value = ps.CaseHeight;
                        sheet.Cells[x, 15].Value = ps.GoldenSampleLocation;
                        sheet.Cells[x, 16].Value = i.Product.Description;
                        sheet.Cells[x, 17].Value = i.Product.Brand;
                        sheet.Cells[x, 18].Value = i.Product.Division;
                        sheet.Cells[x, 19].Value = i.Product.ProductType;
                        sheet.Cells[x, 20].Value = i.Product.ProductCategory;
                        sheet.Cells[x, 21].Value = i.Product.DimensionProfileName;
                        sheet.Cells[x, 22].Value = ps.UPC;
                        y = 23;
                        foreach (var feat in i.Product.Features)
                        {
                            if (y < 27)
                            {
                                sheet.Cells[x, y].Value = feat.Value;
                                y++;
                            }
                        }
                        sheet.Cells[x, 28].Value = ps.UnitsPerPack;
                        sheet.Cells[x, 29].Value = ps.UnitsPerCase;
                        sheet.Cells[x, 30].Value = ps.UnitOfMeasure;
                        x++;
                    }
                }

            }

            ex.SaveAs(new System.IO.FileInfo(filepath));
            return filepath;
        }











        public FileResult DownloadBulkEditFileBySKURedux()
        {
            return File(new System.IO.FileStream(@"\\fred\PMFiles\BulkEditTemplateBySKUNoGroup.xls", System.IO.FileMode.Open), "application/vnd.ms-excel");
        }



        public FileResult DownloadBulkEditFile()
        {
            return File(new System.IO.FileStream(@"\\fred\PMFiles\BulkEditTemplate.xls", System.IO.FileMode.Open), "application/vnd.ms-excel");
        }
        public FileResult DownloadBulkEditFileBySKU()
        {
            return File(new System.IO.FileStream(@"\\fred\PMFiles\BulkEditTemplateBySKU.xls", System.IO.FileMode.Open), "application/vnd.ms-excel");
        }
        [HttpPost]
        public ActionResult MassCreate(ProductMassCreateViewModel file)
        {
            try
            {
                if (file.outfile.ContentLength > 0)
                {
                    List<string> failures = ProcessUpload(file);
                    ViewBag.Message = "File Processed Successfully<br />";
                    ViewBag.Message += "The Following Part Numbers Failed:<br />";
                    foreach (string failure in failures)
                    {
                        ViewBag.Message += failure + "<br />";
                    }
                }
            } catch (Exception ex)
            {

                ViewBag.Message = ex.Message + " - " + ex.StackTrace;
            }
            return View();
        }

        public ActionResult UpdateBrand()
        {
            ExcelPackage ex = new ExcelPackage(System.IO.File.Open(@"\\fred\PMFiles\BrandNameUpdates.xlsx",System.IO.FileMode.Open));
            ExcelWorksheet sheet = ex.Workbook.Worksheets[1];
            List<string> failures = new List<string>();
            string sku;
            string brand;
            bool success = false;
            int successcnt = 0;


            for(int x=2; x<sheet.Dimension.Rows; x++)
            {
                sku = sheet.Cells[x, 1].Value.ToString();
                brand = sheet.Cells[x, 3].Value.ToString();
                var search = itemRepo.GetAll().Select(i=>i.PackSizes);
                success = false;
                foreach (var packlist in search)
                {
                    try
                    {

                        var bpn = packlist.SingleOrDefault(pl => pl.SKU.ToUpper().Trim() == sku.ToUpper().Trim());
                        if (bpn != null)
                        {
                            var prod = productRepo.Get(p => p.BasePartNumber == bpn.BasePartNumber);
                            if (prod != null)
                            {
                                prod.Brand = brand;
                                productRepo.Update(prod);
                                Response.Write(prod.BasePartNumber + " - " + sku + " - " + prod.Brand + "<br />");
                                success = true;
                                successcnt++;
                                break;
                            }
                        }
                    }catch(Exception e)
                    {
                        Response.Write(sku + " - " + brand);
                        Response.Write("<br /><br /><br /><br /><br /><br />" + e.Message + "<br />" + e.InnerException + "<br />" + e.StackTrace + "<br /><br /><br /><br /><br /><br />");
                        failures.Add(sku);
                    }
                }
                if (!success)
                {
                    failures.Add(sku);
                }
            }
            Response.Write("Total Success:  " + successcnt.ToString()+"<br />");
            foreach (string failure in failures)
            {
                ViewBag.Message += failure + "<br />";
            }
            ViewBag.Message += "Total Failed:  " + failures.Count.ToString();

            return View();
        }

        private List<string> EditProducts(ProductMassCreateViewModel mod)
        {
            ExcelPackage ex = new ExcelPackage(mod.outfile.InputStream);
            ExcelWorksheet sheet = ex.Workbook.Worksheets[1];
            Feature[] features = new Feature[5];
            List<string> failures = new List<string>();
            for (int x = 2; x <= sheet.Dimension.Rows; x++)
            {
                var basepartnumber = sheet.Cells[x, 1].Value.ToString().Trim();
                var name = sheet.Cells[x, 2].Value.ToString().Trim();
                string brand;
                try { brand = sheet.Cells[x, 3].Value.ToString().Trim(); } catch(Exception e) { brand = ""; }
                string sapgroup;
                try { sapgroup = sheet.Cells[x, 4].Value.ToString().Trim(); }catch(Exception e) { sapgroup = ""; }
                string desc;
                try { desc = sheet.Cells[x, 5].Value.ToString().Trim(); } catch(Exception e) { desc = ""; }
                string markettingcopy;
                try { markettingcopy = sheet.Cells[x, 6].Value.ToString().Trim(); } catch(Exception e) { markettingcopy = ""; }
                var division = sheet.Cells[x, 7].Value.ToString().Trim();
                var prodtype = sheet.Cells[x, 8].Value.ToString().Trim();
                var procat = sheet.Cells[x, 9].Value.ToString().Trim();

                for (var i =0; i<5; i++)
                {
                    features[i] = new Feature();
                    features[i].Rank = i + 1;
                    if (sheet.Cells[x, i + 11].Value != null)
                    {
                        try { features[i].Value = sheet.Cells[x, i + 11].Value.ToString().Trim(); } catch(Exception e) { features[i].Value = ""; }
                    } else
                    {
                        features[i].Value = "";
                    }
                    features[i].BasePartNumber = basepartnumber;
                }
                var coo = sheet.Cells[x, 15].Value.ToString().Trim();
                List<string> temp =new List<string>();
                foreach(var ori in coo.Split(','))
                {
                    temp.Add(ori.Trim());
                }
                coo = String.Join(",",temp.ToArray());

                var product = productRepo.Get(y => y.BasePartNumber == basepartnumber);
                if (product != null) { 
                    product.Features.Clear();
                    productRepo.Update(product);

                    product.Name = name;
                    product.Description = desc;
                    product.MarketingCopy = markettingcopy;
                    product.Division = division;
                    product.Brand = brand;
                    product.SAPGroupNumber = sapgroup;
                    product.ProductType = prodtype;
                    product.ProductCategory = procat;
                    product.CountryOfOrigin = coo;
                    for (var i = 0; i < 5; i++)
                    {
                        if (features[i].Value != "")
                        {
                            product.Features.Add(features[i]);
                        }
                    }
                    product.UpdatePending = false;
                    product.Discontinued = false;
                    product.SpecComplete = false;
                    product.PackComplete = false;
                    product.Sensormatic = false;
                    //product.Active = false;
                    product.dtModified = DateTime.UtcNow;
                    productRepo.Update(product);
                } else
                {
                    failures.Add(basepartnumber);
                }
            }
            return failures;
        }
        private List<string> ProcessUpload(ProductMassCreateViewModel mod)
        {

            ExcelPackage ex = new ExcelPackage(mod.infile.InputStream);
            ExcelWorksheet sheet = ex.Workbook.Worksheets[1];
            Product product;
            List<string> failures =new List<string>();
            for (int x = 1; x < sheet.Dimension.Rows; x++)
            {
                var sku = sheet.Cells[x, 1].Value.ToString();
                var basepartnumber = sheet.Cells[x, 2].Value.ToString();
                var type = sheet.Cells[x, 3].Value.ToString();
                var desc = sheet.Cells[x, 4].Value.ToString();
                var markettingcopy = sheet.Cells[x, 5].Value.ToString();
                if (type == "Glove")
                    product = new Glove();
                else
                    product = new Apparel();
                var temp = productRepo.GetAll().Where(y => y.BasePartNumber == basepartnumber).ToList();
                if (temp.Count()==0) { 
                    product.Name = sku;
                    product.BasePartNumber = basepartnumber;
                    product.Description = desc;
                    product.MarketingCopy = markettingcopy;
                    product.UpdatePending = false;
                    product.Discontinued = false;
                    product.SpecComplete = false;
                    product.PackComplete = false;
                    product.Sensormatic = false;
                    product.Active = false;
                    product.dtModified = DateTime.UtcNow;
                    productRepo.Create(product);
                }
                else
                {
                    failures.Add(basepartnumber);
                }
            }
            return failures;
        }

        private List<string> ProcessUploadBySKU(ProductMassCreateViewModel mod)
        {
            ExcelPackage ex = new ExcelPackage(mod.infile.InputStream);
            ExcelWorksheet sheet = ex.Workbook.Worksheets[1];
            List<string> failures = new List<string>();
            for (int x = 3; x < sheet.Dimension.Rows; x++)
            {
                var item = itemRepo.Get(sheet.Cells[x, 1]);
                item.Product.Name = sheet.Cells[x, 2].ToString();
                item.Product.Description = sheet.Cells[x, 3].ToString();
                item.Product.MarketingCopy = sheet.Cells[x, 4].ToString();
                item.Product.Brand = sheet.Cells[x, 5].ToString();
                item.Product.Division = sheet.Cells[x, 6].ToString();
                item.Product.ProductType = sheet.Cells[x, 7].ToString();
                item.Product.ProductCategory = sheet.Cells[x, 8].ToString();
                //item.Product.Name = sheet.Cells[x, 9].ToString();
                var y = 10;
                item.Product.Features.Clear();
                itemRepo.Update(item);

                while (sheet.Cells[x, y].ToString() != "")
                {
                    Feature feature = new Feature();
                    feature.BasePartNumber = item.BasePartNumber;
                    feature.Rank = y - 9;
                    feature.Value = sheet.Cells[x, y].ToString();
                    item.Product.Features.Add(feature);
                    y++;
                }
                itemRepo.Update(item);

                x++;
                while (sheet.Cells[x, 1].ToString() == "")
                {
                    var pack = item.PackSizes.SingleOrDefault(p=>p.SKU==sheet.Cells[x,2].ToString());
                    pack.Size = sheet.Cells[x, 3].ToString();
                    pack.ItemLength = double.Parse(sheet.Cells[x, 4].ToString());
                    pack.ItemWidth = double.Parse(sheet.Cells[x, 5].ToString());
                    pack.ItemHeight = double.Parse(sheet.Cells[x, 6].ToString());
                    pack.CaseLength = double.Parse(sheet.Cells[x, 10].ToString());
                    pack.CaseWidth = double.Parse(sheet.Cells[x, 11].ToString());
                    pack.CaseHeight = double.Parse(sheet.Cells[x, 12].ToString());
                }
            }
            return failures;
        }


        [HttpPost]
        public void ProductDelete(string deleteproduct)
        {
            string q = "";
            
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ProductManagerConnection"].ConnectionString);
            conn.Open();
            q = "Delete From ProductBase where BasePartNumber = @basepartnumber";
            SqlCommand cmd = new SqlCommand(q, conn);
            try
            {
                cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@basepartnumber"].Value = deleteproduct;
                //Console.WriteLine(cmd.CommandText);
                cmd.ExecuteNonQuery();
                conn.Close();
                conn.Open();
            } catch (Exception e) { }
            try
            {
                q = "Delete From ProductBasePackaging where BasePartNumber=@basepartnumber";
                cmd = new SqlCommand(q, conn);
                cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@basepartnumber"].Value = deleteproduct;
                //Console.WriteLine(cmd.CommandText);
                cmd.ExecuteNonQuery();
                conn.Close();
                conn.Open();
            }catch(Exception e) { }

            try { 
            q = "Delete From ProductBaseGlove2 where BasePartNumber=@basepartnumber";
            cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@basepartnumber"].Value = deleteproduct;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Open();
            }
            catch (Exception e) { }
            try { 
            q = "Delete From ProductBaseClothing2 where BasePartNumber=@basepartnumber";
            cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@basepartnumber"].Value = deleteproduct;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Open();
            }
            catch (Exception e) { }
            try { 
            q = "Delete From ProductCrossReference where BasePartNumber=@basepartnumber";
            cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@basepartnumber"].Value = deleteproduct;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Open();
            }
            catch (Exception e) { }
            try
            {

                q = "Delete From ProductFeaturesBenefits where BasePartNumber=@basepartnumber";
            cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@basepartnumber"].Value = deleteproduct;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Open();
            }
            catch (Exception e) { }
            try
            {
                q = "Delete From Item2 where BasePartNumber=@basepartnumber";
            cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@basepartnumber"].Value = deleteproduct;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Open();
            }
            catch (Exception e) { }
            try
            {
                q = "Delete From ProductPhoto where BasePartNumber=@basepartnumber";
            cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@basepartnumber"].Value = deleteproduct;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Open();
            }
            catch (Exception e) { }
            try
            {
                q = "Delete From PackSize Where BasePartNumber=@basepartnumber";
            cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@basepartnumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@basepartnumber"].Value = deleteproduct;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            
            }
            catch (Exception e) { }
            conn.Close();

            //var prod = productRepo.Get(deleteproduct);
            //productRepo.Delete(prod);
        }


    }
}