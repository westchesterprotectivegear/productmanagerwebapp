﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;
using ProductManager.Services;
using SAP.Entities;

namespace ProductManager.Controllers
{
    [Authorize]
    public class EditMeasurementController : _ApplicationController
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }
        [Inject]
        public IRepository<Measurement, ProductManagerDatabaseContext> measurementRepo { get; set; }
        [Inject]
        public IRepository<DimensionProfile, ProductManagerDatabaseContext> profileRepo { get; set; }
        [Inject]
        public IProductService productService { get; set; }
        [Inject]
        public IHistoryService historyService { get; set; }
        [Inject]
        public IOptionsService optionsService { get; set; }

        public ActionResult EditProductMeasurements(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);
            var orderedItems = productService.OrderBySize(product.Items);

            var model = new EditProductMeasurementsViewModel();
            model.BasePartNumber = product.BasePartNumber;
            model.DimensionProfile = product.DimensionProfileName;
            model.Items = Mapper.Map<List<EditItemViewModel>>(orderedItems);

            var existingSizes = orderedItems.Select(x => x.Size);
            model.SizeOptions = optionsService.GetSizes().Where(x => !existingSizes.Contains(x)).ToList();
            foreach (var dim in product.DimensionProfile.Dimensions.OrderBy(x => x.Code))
            {
                var dimList = new List<EditItemDimensionViewModel>();
                foreach (var item in orderedItems)
                {
                    if (item.Measurements.Any(x => x.DimensionID == dim.ID))
                    {
                        dimList.Add(Mapper.Map<EditItemDimensionViewModel>(item.Measurements.First(x => x.DimensionID == dim.ID)));
                    }
                    else
                    {
                        var dimModel = Mapper.Map<EditItemDimensionViewModel>(dim);
                        dimModel.ItemID = item.ID;
                        dimList.Add(dimModel);
                    }
                }

                model.Dimensions.Add(dimList);
            }

            if (product is Glove)
                model.ShowHemColors = true;

            return PartialView("_EditProductMeasurementsPartial", model);
        }

        [HttpPost]
        public ActionResult EditProductMeasurements(EditProductMeasurementsViewModel model)
        {
            // get the existing measurements
            var existing = model.Dimensions.SelectMany(x => x)
                .Where(x => x.MeasurementID != Guid.Empty).ToList();

            // get new ones based on empty guid, only where there are not whitespace values
            var additions = model.Dimensions.SelectMany(x => x)
                .Where(x => x.MeasurementID == Guid.Empty
                && !string.IsNullOrWhiteSpace(x.Value)).ToList();

            foreach (var measModel in existing)
            {
                // get db instance of the measurement
                var measurement = measurementRepo.Get(measModel.MeasurementID);

                // delete it if empty value, else map and update
                if (string.IsNullOrWhiteSpace(measModel.Value))
                    measurementRepo.Delete(measurement);
                else
                {
                    Mapper.Map<EditItemDimensionViewModel, Measurement>(measModel, measurement);
                    measurementRepo.Update(measurement);
                }
            }

            foreach (var measModel in additions)
            {
                var measurement = Mapper.Map<Measurement>(measModel);
                measurementRepo.Create(measurement);
            }

            var product = productService.Get(model.BasePartNumber);
            foreach (var itemModel in model.Items)
            {
                var item = product.Items.First(x => x.Size == itemModel.Size);
                item.Active = itemModel.Active;
                item.HemColor = itemModel.HemColor;
            }

            return RedirectToAction("EditProductMeasurements", new { partNumber = model.BasePartNumber });
        }

        public ActionResult ChangeDimensionProfile(string partNumber)
        {
            var product = productService.Get(partNumber);
            var profile = product.DimensionProfile;

            var model = new ChangeDimensionProfileViewModel();
            model.BasePartNumber = product.BasePartNumber;
            model.DimensionProfile = profile.Name;
            model.ToDimensionProfile = profile.Name;
            model.DimensionProfileOptions = profileRepo.GetAll()
                .Select(x => x.Name).ToList();

            foreach (var dimension in profile.Dimensions
                .OrderBy(x => x.Code))
            {
                model.Dimensions.Add(new ChangeDimensionViewModel()
                {
                    ID = dimension.ID,
                    Name = dimension.Value
                });
            }

            return PartialView("_ChangeDimensionProfile", model);
        }

        [HttpPost]
        public ActionResult ChangeDimensionProfile(ChangeDimensionProfileViewModel model)
        {
            var isDupe = model.Dimensions.GroupBy(x => x.ToID)
                .Where(g => g.Count() > 1).Count() > 1;

            if (isDupe)
            {
                return Json(null);
            }
            else
            {
                var product = productService.Get(model.BasePartNumber);
                var currentProfile = profileRepo.Get(model.DimensionProfile);
                var newProfile = profileRepo.Get(model.ToDimensionProfile);

                var measurements = product.Items
                    .SelectMany(x => x.Measurements).ToList();

                var newMeasurements = new List<Measurement>();
                foreach (var dim in model.Dimensions)
                {
                    var dimMeasurements = measurements.Where(x => x.DimensionID == dim.ID);

                    foreach (var dimMeasurement in dimMeasurements)
                    {
                        var newMeas = Mapper.Map<Measurement>(dimMeasurement);
                        newMeas.DimensionID = dim.ToID;

                        newMeasurements.Add(newMeas);
                    }
                }

                var newMeasGroups = newMeasurements.GroupBy(x => x.ItemID).Select(grp => grp.ToList()).ToList();

                foreach (var grp in newMeasGroups)
                {
                    var item = itemRepo.Get(grp[0].ItemID);

                    foreach (var meas in grp)
                    {
                        item.Measurements.Add(meas);
                    }
                }

                // delete old
                foreach (var oldMeas in measurements)
                {
                    var item = itemRepo.Get(oldMeas.ItemID);
                    item.Measurements.Remove(oldMeas);
                }

                product.DimensionProfileName = model.ToDimensionProfile;
                product.dtModified = DateTime.UtcNow;
                productRepo.Update(product);

                return RedirectToAction("EditProductMeasurements", new { partNumber = product.BasePartNumber });
            }
        }
    }
}