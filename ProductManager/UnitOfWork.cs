﻿using System.Diagnostics;
using NHibernate;

namespace ProductManager
{
    public interface IUnitOfWork<TContext>
    {
        void BeginTransaction();
        void Commit();
    }

    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DatabaseContext
    {
        private ISessionFactory _sessionFactory;
        private ITransaction _transaction;

        public ISession Session { get; private set; }

        public UnitOfWork(TContext context)
        {
            if (_sessionFactory == null)
            {
                _sessionFactory = context.GetSessionFactory();
            }

            Session = _sessionFactory.OpenSession();
        }

        public void BeginTransaction()
        {
            if (Session == null || !Session.IsOpen)
                Session = _sessionFactory.OpenSession();

            _transaction = Session.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                Session.Close();
                Session.Dispose();
            }
        }
    }


    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            Debug.WriteLine(sql.ToString());
            return sql;
        }
    }
}