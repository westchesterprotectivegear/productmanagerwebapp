
function addFeature() {
    $.ajax({
        type: "GET",
        url: "/EditProduct/AddFeature/",
        success: function (responseText) {
            $("#featureTable").append(responseText);
            $("#saveButton").show();
            $("#productDetailsForm").data("changed", true);
        },
        error: function (errorData) { onError(errorData); }
    });
}

function removeFeature(el) {
    $(el).closest('tr').remove();
    $("#saveButton").show();
    $("#productDetailsForm").data("changed", true);
}