﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using Ghostscript.NET.Rasterizer;
using ImageMagick;
using Ninject;
using ProductManager.Entities;

namespace ProductManager.Services
{
    public interface IPhotoService
    {
        void AddPhoto(string fileName, Stream fileStream, Photo photo);
    }

    public class PhotoService : IPhotoService
    {
        [Inject]
        public IImageService imageService { get; set; }
        [Inject]
        public IRepository<Photo, ProductManagerDatabaseContext> photoRepo { get; set; }

        private string publicDir = @"\\fred\images\ProductImages";
        private string privateDir = HttpContext.Current.Server.MapPath("~/Content/images/ProductImages");
        private string thumbDir = HttpContext.Current.Server.MapPath("~/Content/images/ProductImages/Thumbnails");

        public void AddPhoto(string fileName, Stream fileStream, Photo photo)
        {
            FileInfo publicFile = new System.IO.FileInfo(Path.Combine(publicDir, Uri.UnescapeDataString(fileName)));
            FileInfo privateFile = new System.IO.FileInfo(Path.Combine(privateDir, Uri.UnescapeDataString(fileName)));
            FileInfo thumbFile = new System.IO.FileInfo(Path.Combine(thumbDir, Uri.UnescapeDataString(fileName)));

            if (photo.Type == "Primary")
            {
                var oldPhoto = photoRepo.Get(x => x.BasePartNumber == photo.BasePartNumber && x.Type == "Primary");
                if (oldPhoto != null)
                    photoRepo.Delete(oldPhoto);
            }

            if (Path.GetExtension(fileName) == ".pdf")
            {
                var images = imageService.GetImagesFromPDF(fileStream);

                // if it's the primary photo, only allow 1 image
                int limit = photo.Type == "Primary" ? 1 : images.Length;
                for (int i = 0; i < limit; i++)
                {
                    var image = images[i];
                    string name = Path.GetFileNameWithoutExtension(publicFile.FullName)
                        + (images.Length == 1 ? "" : " - Page " + (i + 1))
                        + ".png";

                    // save everywhere
                    image.Save(Path.Combine(privateFile.DirectoryName, name), ImageFormat.Png);
                    image.Save(Path.Combine(publicFile.DirectoryName, name), ImageFormat.Png);

                    // and thumbnail it
                    image = imageService.ScaleImage(image, 100, 100);
                    image.Save(Path.Combine(thumbFile.DirectoryName, name), ImageFormat.Png);

                    photo.Name = name;
                    photo.Created = DateTime.UtcNow;
                    photoRepo.Create(photo);
                }
            }
            else
            {
                var bytes = ReadData(fileStream);

                File.WriteAllBytes(publicFile.FullName, bytes);
                File.WriteAllBytes(privateFile.FullName, bytes);
                File.WriteAllBytes(thumbFile.FullName, bytes);
                photo.Created = DateTime.UtcNow;
                photoRepo.Create(photo);
            }
        }

        public byte[] ReadData(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];

            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }
        string GetRelativePath(string filespec, string folder)
        {
            Uri pathUri = new Uri(filespec);
            // Folders must end in a slash
            if (!folder.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                folder += Path.DirectorySeparatorChar;
            }
            Uri folderUri = new Uri(folder);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }
    }
}
