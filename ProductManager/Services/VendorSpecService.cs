﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Ninject;
using PdfSharp.Pdf;
using ProductManager.Entities;

namespace ProductManager.Services
{
    public interface IVendorSpecService
    {
        void Generate(string partNumber);
        void GenerateOutdated();
    }

    public class VendorSpecService : IVendorSpecService
    {
        [Inject]
        public IImageService imageService { get; set; }
        [Inject]
        public IProductService productService { get; set; }
        [Inject]
        public IHistoryService historyService { get; set; }
        [Inject]
        public EmailService emailService { get; set; }
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<Measurement, ProductManagerDatabaseContext> measurementRepo { get; set; }
        [Inject]
        public IRepository<HistoryItem, ProductManagerDatabaseContext> historyRepo { get; set; }
        [Inject]
        public IRepository<Prop65Chemical, ProductManagerDatabaseContext> chemRepo { get; set; }
        Dictionary<string, string> BrandTMR = new Dictionary<string, string>();
        private Product product { get; set; }
        private List<Item> orderedItems { get; set; }
        private int rowCount = 0;
        private string tempDir = @"\\fred\pmfiles\temp";
        private string vendorSpecDir = @"\\fred\specs";
        private List<string> paths { get; set; }

        public void GenerateOutdated()
        {
            setTMR();
            var mods = historyRepo.GetAll().Where(x => x.Date.Date == DateTime.Today.Date).ToList();
            var products = mods.Select(x => x.BasePartNumber)
                .OrderBy(x => x)
                .Distinct()
                .ToList();

            foreach (var product in products)
            {
                Generate(product);
            }

            string updateString = "No specs modified today";

            if (products.Count > 0)
            {
                updateString = "Specs modified today:\n\n";

                updateString += string.Join("\n", products);
            }

            MailMessage message = new MailMessage();
            message.Subject = "Updated Specs - " + DateTime.Today.ToShortDateString();
            message.Body = updateString;
            message.To.Add(@"PM_VendorSpecSheetGenerated@westchestergear.com");
            emailService.Send(message);
        }

        public void setTMR()
        {
            //  \u00A9 - Copywrite      \u00AE - Registered
            BrandTMR["West Chester"] = " \u2122"; //Trademark Symbol
            BrandTMR["Anchor"] = "\u00AE";  
            BrandTMR["Barracuda"] = "\u00AE";
            BrandTMR["Big Lots"] = "\u2122";
            BrandTMR["Black Canyon DAS"] = "\u2122";
            BrandTMR["Blackstone"] = "\u2122";
            BrandTMR["Blue Hawk"] = "\u2122";
            BrandTMR["Blue Mountain"] = "\u2122"; 
            BrandTMR["BodyGuard"] = "\u00AE"; 
            BrandTMR["Brass Knuckles"] = "\u2122"; 
            BrandTMR["Brigade"] = "\u2122"; 
            BrandTMR["Bullard"] = "\u00AE";	
            BrandTMR["C E Schmidt"] = "\u00AE";
            BrandTMR["DNOW"] = "\u2122"; 
            BrandTMR["Dirty Work"] = "\u2122";
            BrandTMR["Everbilt"] = "\u2122";
            BrandTMR["Extreme Work"] = "\u00AE";
            BrandTMR["Gravel Gear"] = "\u00AE";
            BrandTMR["HDX"] = "\u2122";
            BrandTMR["Home Depot"] = "\u00AE";
            BrandTMR["Husky"] = "\u00AE"; 
            BrandTMR["Hyper Tough"] = "\u2122";
            BrandTMR["Ironcat"] = "\u00AE";	
            BrandTMR["Ironton"] = "\u00AE"; 
            BrandTMR["Lawnscapes"] = "\u2122";
            BrandTMR["Lowes"] = "\u00AE";
            BrandTMR["Lowes store use"] = "\u00AE"; 
            BrandTMR["Master Gear"] = "\u00AE";	
            BrandTMR["Menards"] = "\u00AE"; 
            BrandTMR["Miracle - Gro"] = "\u00AE"; 
            BrandTMR["NorthernTool"] = "\u00AE"; 
            BrandTMR["Perf.Select"] = "\u2122";
            BrandTMR["Performance Value Gear"] = "\u2122";
            BrandTMR["PosiGrip"] = "\u00AE";
            BrandTMR["PosiWear"] = "\u00AE";
            BrandTMR["Pro Series"] = "\u00AE";
            BrandTMR["ProStar"] = "\u2122";
            BrandTMR["R2"] = "\u00AE";
            BrandTMR["Radnor"] = "\u00AE";
            BrandTMR["Real Tree"] = "\u00AE";
            BrandTMR["Rugged Wear"] = "\u00AE";
            BrandTMR["Scotts"] = "\u00AE";
            BrandTMR["Stihl"] = "\u00AE";
            BrandTMR["Style Selections"] = "\u2122";
            BrandTMR["Sumo Grip"] = "\u00AE"; 
            BrandTMR["West Chester"] = "\u00AE";
            BrandTMR["Wilson"] = "\u2122";
            BrandTMR["Zee Medical"] = "\u00AE";
            BrandTMR["Zone Defense"] = "\u00AE";


        }

        public void Generate(string partNumber)
        {
            setTMR();
            try
            {
                product = productRepo.Get(x => x.BasePartNumber == partNumber);
                orderedItems = productService.OrderBySize(product.Items)
                    .Where(x => x.Active).ToList();
                paths = new List<string>();

                // create a MigraDoc document
                Document document = CreateDocument();
                document.UseCmykColor = true;
                const bool unicode = false;
                const PdfFontEmbedding embedding = PdfFontEmbedding.Always;
                PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, embedding);

                pdfRenderer.Document = document;

                pdfRenderer.RenderDocument();

                // save the document
                string filename = Path.Combine(vendorSpecDir, partNumber.Replace("/", "~") + ".pdf");
                pdfRenderer.PdfDocument.Save(filename);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                // get rid of temporary images
                CleanupTemp();
            }
        }

        private Document CreateDocument()
        {
            // Create a new MigraDoc document
            Document document = new Document();
            var style = document.Styles["Normal"];
            style.Font.Name = "Arial";

            // Add a section to the document
            Section section = document.AddSection();
            section.PageSetup.LeftMargin = "1cm";
            section.PageSetup.RightMargin = "1cm";
            section.PageSetup.TopMargin = "3cm";
            section.PageSetup.BottomMargin = "1cm";

            HeaderFooter hf = CreateHeaderFooter();
            section.Headers.Primary = hf;

            var table = CreateTopInfoSection();
            section.Add(table);

            table = CreateItemDetailsSection();
            section.Add(table);

            section.AddPageBreak();
            table = CreateMeasurementsSection();
            section.Add(table);

            section.AddPageBreak();
            table = CreateTagAssemblySection();
            section.Add(table);

            table = CreatePhotosSection("Hang Tag");
            section.Add(table);

            section.AddPageBreak();
            table = CreatePackagingSection();
            section.Add(table);

            table = CreateWarningLabelSection();
            section.Add(table);

            table = CreatePhotosSection("Neck Label");
            section.Add(table);

            table = CreatePhotosSection("Carton Marking");
            section.Add(table);

            table = CreatePhotosSection("Case Bumper");
            section.Add(table);

            table = CreatePhotosSection("Violator");
            section.Add(table);

            table = CreatePhotosSection("Additional");
            section.Add(table);

            table = CreateHistorySection();
            section.Add(table);

            return document;
        }

        private HeaderFooter CreateHeaderFooter()
        {
            HeaderFooter hf = new HeaderFooter();

            var table = hf.AddTable();

            var column = table.AddColumn("6.33cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = table.AddColumn("6.33cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("6.33cm");
            column.Format.Alignment = ParagraphAlignment.Right;

            // add primary to left, first cell
            var row = table.AddRow();
            var image = row.Cells[0].AddParagraph().AddImage(@"\\fred\PMFiles\logo.jpg");
            image.Height = "1cm";
            image.LockAspectRatio = true;
            image.RelativeVertical = RelativeVertical.Line;
            image.RelativeHorizontal = RelativeHorizontal.Margin;
            image.Top = ShapePosition.Top;
            image.Left = ShapePosition.Left;
            image.WrapFormat.Style = WrapStyle.Through;

            var par = row.Cells[1].AddParagraph(product.BasePartNumber);
            par.Format.Font.Size = 25;

            par = row.Cells[2].AddParagraph(DateTime.Now.ToShortDateString());
            par.Format.Font.Size = 8;

            return hf;
        }

        private Table CreateItemDetailsSection()
        {
            // Create the item table
            var table = new Table();
            table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("15cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.BottomPadding = 5;
            row.TopPadding = 5;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
            row.Cells[0].AddParagraph("Item Details");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);
            row.Cells[0].Format.Font.Size = 20;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeRight = 1;

            rowCount = 0;
            if (product is Glove)
            {
                var glove = (Glove)product;
                AddItemDetailRow(table, "Glove Pattern:", glove.Pattern);
                AddItemDetailRow(table, "Color Pattern:", glove.ColorPattern);
                AddItemDetailRow(table, "Color Detail:", glove.ColorDetail);
                AddItemDetailRow(table, "Coating:", glove.Coating);
                AddItemDetailRow(table, "Mil Gauge:", glove.MilGauge);
                AddItemDetailRow(table, "Weight:", glove.GramsMils);
                AddItemDetailRow(table, "Lining:", glove.Lining);
                AddItemDetailRow(table, "Shell:", glove.Shell);
                AddItemDetailRow(table, "Palm Style:", glove.PalmStyle);
                AddItemDetailRow(table, "Back of Hand:", glove.BackOfHand);
                AddItemDetailRow(table, "Cuff Type:", glove.CuffType);
                AddItemDetailRow(table, "Leather Type:", glove.LeatherType);
                AddItemDetailRow(table, "Leather Location:", glove.LeatherLocation);
                AddItemDetailRow(table, "Grade:", glove.Grade);
                AddItemDetailRow(table, "Quality Level:", glove.QualityLevel);
                AddItemDetailRow(table, "Thumb Type:", glove.ThumbType);
                AddItemDetailRow(table, "Components:", glove.Components);
                AddItemDetailRow(table, "Material Content:", glove.MaterialContent);
                AddItemDetailRow(table, "Cuff:", glove.CuffConstruction);
                AddItemDetailRow(table, "Hem:", glove.HemConstruction);
                AddItemDetailRow(table, "Knuckle:", glove.KnuckleConstruction);
                AddItemDetailRow(table, "Back of Hand:", glove.BackOfHandConstruction);
                AddItemDetailRow(table, "Palm:", glove.PalmConstruction);
                AddItemDetailRow(table, "Thread Type:", glove.ThreadType);
                AddItemDetailRow(table, "Stitches Per Inch:", glove.StitchesPerInch);
                AddItemDetailRow(table, "Sewing Instructions:", glove.SewingInstructions);
                AddItemDetailRow(table, "Washing Instructions:", glove.WashingInstructions);
                AddItemDetailRow(table, "Reinforced Features:", glove.ReinforcedFeatures);
                AddItemDetailRow(table, "Dip Type:", glove.DipType);
                AddItemDetailRow(table, "Special Instructions:", glove.SpecialInstructions);
                AddItemDetailRow(table, "RN:", glove.RNNumber);
                AddItemDetailRow(table, "Testing:", glove.Testing);
                AddItemDetailRow(table, "Country of Origin:", glove.CountryOfOrigin);
            }
            else
            {
                var apparel = (Apparel)product;
                AddItemDetailRow(table, "Color Pattern:", apparel.ColorPattern);
                AddItemDetailRow(table, "Base Material:", apparel.BaseMaterial);
                AddItemDetailRow(table, "Configuration:", apparel.Configuration);
                AddItemDetailRow(table, "No. of Pockets:", apparel.NumberOfPockets);
                AddItemDetailRow(table, "Elastic Type:", apparel.ElasticType);
                AddItemDetailRow(table, "Seam Construction:", apparel.SeamConstruction);
                AddItemDetailRow(table, "Button Type:", apparel.ButtonType);
                AddItemDetailRow(table, "Zipper Grade:", apparel.ZipperGrade);
                AddItemDetailRow(table, "Storm Flaps:", apparel.StormFlaps);
                AddItemDetailRow(table, "Thread Type:", apparel.ThreadType);
                AddItemDetailRow(table, "Stitches Per Inch:", apparel.StitchesPerInch);
                AddItemDetailRow(table, "Mils:", apparel.GramsMils);
                AddItemDetailRow(table, "Components:", apparel.Components);
                AddItemDetailRow(table, "Material Content:", apparel.MaterialContent);
                AddItemDetailRow(table, "RN:", apparel.RNNumber);
                AddItemDetailRow(table, "Testing:", apparel.Testing);
                AddItemDetailRow(table, "Country of Origin:", apparel.CountryOfOrigin);
                AddItemDetailRow(table, "Garment Logo:", apparel.GarmentLogo);
            }

            if (product.Prop65LabelRequired && product.Prop65Chemicals != null)
            {
                var productChems = product.Prop65Chemicals.Split(',').ToList();
                var chems = chemRepo.GetGroup(x => productChems.Contains(x.Name));

                foreach (var chem in chems)
                {
                    AddItemDetailRow(table, "Prop 65 - " + chem.Name + ":", chem.Warning);
                }
            }
            rowCount = 0;

            return table;
        }

        private void AddItemDetailRow(Table table, string header, string data)
        {
            if (!string.IsNullOrWhiteSpace(data))
            {
                MigraDoc.DocumentObjectModel.Color color;
                if (rowCount % 2 == 0)
                    color = MigraDoc.DocumentObjectModel.Color.FromCmyk(19.3, 10.53, 0, 10.59);
                else
                    color = MigraDoc.DocumentObjectModel.Color.FromCmyk(8.71, 4.56, 0, 5.49);

                ParagraphFormat rowHeaderFormat = new ParagraphFormat();
                rowHeaderFormat.Alignment = ParagraphAlignment.Center;
                rowHeaderFormat.Font.Bold = true;
                rowHeaderFormat.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 100);
                rowHeaderFormat.Font.Size = 10;
                rowHeaderFormat.Alignment = ParagraphAlignment.Left;

                ParagraphFormat rowDataFormat = new ParagraphFormat();
                rowDataFormat.Alignment = ParagraphAlignment.Left;
                rowDataFormat.Font.Bold = false;
                rowDataFormat.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 100);
                rowDataFormat.Font.Size = 10;
                rowDataFormat.Alignment = ParagraphAlignment.Left;

                var row = table.AddRow();
                row.BottomPadding = 3;
                row.TopPadding = 3;
                row.Shading.Color = color;
                row.Cells[0].AddParagraph(header);
                row.Cells[0].Format = rowHeaderFormat;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                row.Cells[1].AddParagraph(data);
                row.Cells[1].Format = rowDataFormat;
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;

                rowCount++;
            }
        }

        private Table CreateTopInfoSection()
        {
            // get the primary photo
            var photo = product.PrimaryPhoto;

            // get containing table for top elements
            Table table = new Table();
            table.Rows.LeftIndent = 0;
            table.Format.SpaceAfter = 50;

            // create columns for top elements
            var column = table.AddColumn("7cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("12cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            // get scaled primary
            System.Drawing.Image image = System.Drawing.Image.FromFile(@"\\fred\images\productimages\" + photo.Name);
            image = imageService.ScaleImage(image, 250, 250);
            string path = CreateTempImage(image);

            // add primary to left, first cell
            var row = table.AddRow();
            row.Cells[0].AddParagraph().AddImage(path);

            // create a second table to represent text
            var subTable = new Table();

            // text columns
            var subColumn = subTable.AddColumn("4cm");
            subColumn.Format.Alignment = ParagraphAlignment.Left;

            subColumn = subTable.AddColumn("8cm");
            subColumn.Format.Alignment = ParagraphAlignment.Left;

            var sizes = orderedItems.Select(x => x.Size).ToList();
            var sizeString = string.Join(", ", sizes);
            string brandtmrlabel = "";
            try
            {
                brandtmrlabel = BrandTMR[product.Brand];
            } catch (Exception e)
            {
                brandtmrlabel = "";
            }
            var subRow = subTable.AddRow();
            subRow.Cells[0].AddParagraph("Brand: ");
            subRow.Cells[0].Format.Font.Bold = true;
            subRow.Cells[1].AddParagraph(product.Brand == null ? "" : product.Brand+brandtmrlabel);

            subRow = subTable.AddRow();
            subRow.Cells[0].AddParagraph("Available Sizes: ");
            subRow.Cells[0].Format.Font.Bold = true;
            subRow.Cells[1].AddParagraph(sizeString == null ? "" : sizeString);

            subRow = subTable.AddRow();
            subRow.Cells[0].AddParagraph("Description: ");
            subRow.Cells[0].Format.Font.Bold = true;
            subRow.Cells[1].AddParagraph(product.Description == null ? "" : product.Description);

            if (product.Prop65LabelRequired)
            {
                subRow = subTable.AddRow();
                subRow.TopPadding = 10;
                subRow.Cells[0].AddParagraph("PROP 65 WARNING LABEL REQUIRED");
                subRow.Cells[0].Format.Font.Bold = true;
                subRow.Cells[0].Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 99, 100, 0);
                subRow.Cells[0].MergeRight = 1;
            }

            row.Cells[1].Elements.Add(subTable);

            return table;
        }

        private Table CreateCartonDataTable(PackSize packSize)
        {
            // Create the item table
            var table = new Table();
            table.Borders.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 100);
            table.Borders.Width = 0.25;
            table.Borders.Left.Width = 0.5;
            table.Borders.Right.Width = 0.5;
            table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            var column = table.AddColumn("2.11cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = table.AddColumn("4.22cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2.11cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2.11cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2.11cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("2.11cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("4.22cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            var row = table.AddRow();
            row.Cells[0].AddParagraph(packSize.SKU);
            row.Cells[0].Format.Font.Bold = true;
            row.Format.Font.Size = 14;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[0].MergeRight = 6;

            row = table.AddRow();
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Format.Font.Size = 10;
            row.Cells[0].AddParagraph("SIZE: " + packSize.Size);
            row.Cells[0].MergeRight = 1;
            row.Cells[2].AddParagraph("UPC: " + packSize.UPC);
            row.Cells[2].MergeRight = 2;
            row.Cells[5].AddParagraph("PIECE WT: " + packSize.PieceWeight);
            row.Cells[5].MergeRight = 1;

            row = table.AddRow();
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Format.Font.Size = 10;
            row.Cells[0].AddParagraph("");
            row.Cells[1].AddParagraph("GTIN");
            row.Cells[2].AddParagraph("L");
            row.Cells[3].AddParagraph("W");
            row.Cells[4].AddParagraph("H");
            row.Cells[5].AddParagraph("V");
            row.Cells[6].AddParagraph("WEIGHT(LB)");

            row = table.AddRow();
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = false;
            row.Format.Font.Size = 10;
            row.Cells[0].AddParagraph("MASTER");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[1].AddParagraph(packSize.MasterCaseGTIN == null ? "" : packSize.MasterCaseGTIN);
            row.Cells[2].AddParagraph(packSize.CaseLength.ToString());
            row.Cells[3].AddParagraph(packSize.CaseWidth.ToString());
            row.Cells[4].AddParagraph(packSize.CaseHeight.ToString());
            row.Cells[5].AddParagraph(Math.Round(packSize.CaseVolume, 2).ToString("0.00"));
            row.Cells[6].AddParagraph(packSize.GrossCaseWeight.ToString());

            if (packSize.InnerDimensionsOnSpec)
            {
                row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Font.Bold = false;
                row.Format.Font.Size = 10;
                row.Cells[0].AddParagraph("INNER");
                row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[0].Format.Font.Bold = true;
                row.Cells[1].AddParagraph(packSize.InnerPackGTIN == null ? "" : packSize.InnerPackGTIN);
                row.Cells[2].AddParagraph(packSize.InnerLength.ToString());
                row.Cells[3].AddParagraph(packSize.InnerWidth.ToString());
                row.Cells[4].AddParagraph(packSize.InnerHeight.ToString());
                row.Cells[5].AddParagraph(Math.Round(packSize.InnerVolume, 2).ToString("0.00"));
                row.Cells[6].AddParagraph(packSize.InnerWeight.ToString());
            }

            row = table.AddRow();
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = false;
            row.Format.Font.Size = 10;
            row.Cells[0].AddParagraph("PALLET");
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[1].AddParagraph(packSize.PalletGTIN == null ? "" : packSize.PalletGTIN);
            row.Cells[2].AddParagraph("");
            row.Cells[3].AddParagraph("");
            row.Cells[4].AddParagraph("");
            row.Cells[5].AddParagraph("");
            row.Cells[6].AddParagraph("");

            return table;
        }

        private Table CreatePhotosSection(string type)
        {
            // Create the photos table, one column, and a row for each photo
            var table = new Table();
            table.Rows.LeftIndent = 0;

            var column = table.AddColumn("19cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // loop through additional photos
            var photos = product.Photos.Where(x => x.Type == type).ToList();
            foreach (var photo in photos)
            {
                // get image, resize, and add description text
                System.Drawing.Image image = System.Drawing.Image.FromFile(@"\\fred\images\productimages\" + photo.Name);
                image = imageService.ScaleImage(image, 500, 500);
                image = imageService.ImposeTextOnImageBottom(image, photo.AssociatedText);

                // save temp image
                string path = CreateTempImage(image);

                // add image to sheet
                var row = table.AddRow();
                row.BottomPadding = 10;
                row.TopPadding = 10;
                row.Cells[0].AddParagraph().AddImage(path);
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            }

            return table;
        }

        private Table CreateWarningLabelSection()
        {
            // Create the warning label table, one column, and a row for each warning label
            var table = new Table();
            table.Rows.LeftIndent = 0;

            var column = table.AddColumn("19cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            var packSizes = orderedItems.SelectMany(x => x.PackSizes
                .OrderBy(p => p.SKU));

            var groups = packSizes.Where(x => x.WarningLabel != null)
                .GroupBy(x => x.WarningLabel)
                .Select(grp => grp.ToList())
                .ToList();

            foreach (var group in groups)
            {
                var skus = group.Select(x => x.SKU).ToList();
                string message = "Warning Label for " + string.Join(", ", skus);

                string path = @"\\wcmktg.com\WCPG-HQ\Public\edi\exe\MarketingDatabase\WarningLabels\" + group[0].WarningLabel + ".jpg";
                if (File.Exists(path))
                {
                    // get image, resize, and add description text
                    System.Drawing.Image image = System.Drawing.Image.FromFile(path);
                    image = imageService.ImposeTextOnImageBottom(image, message);

                    // save temp image
                    path = CreateTempImage(image);

                    // add image to sheet
                    var row = table.AddRow();
                    row.BottomPadding = 10;
                    row.TopPadding = 10;
                    row.Cells[0].AddParagraph().AddImage(path);
                    row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                }
            }

            return table;
        }

        private Table CreateMeasurementsSection()
        {
            var table = new Table();

            var column = table.AddColumn("19cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            var itemGroups = orderedItems
                .Chunk(7)
                .Select(chunk => chunk.ToList())
                .ToList();

            var profile = product.DimensionProfile;

            // get image, resize, and add description text
            System.Drawing.Image image;
            if (File.Exists(@"\\fred\images\DimensionProfileImages\" + profile.Image))
                image = System.Drawing.Image.FromFile(@"\\fred\images\DimensionProfileImages\" + profile.Image);
            else
                image = System.Drawing.Image.FromFile(@"\\fred\images\DimensionProfileImages\NoImage.png");

            image = imageService.ScaleImage(image, 500, 500);

            // save temp image
            string path = CreateTempImage(image);

            // add image to sheet
            var mainRow = table.AddRow();
            mainRow.BottomPadding = 10;
            mainRow.TopPadding = 10;
            mainRow.Cells[0].AddParagraph().AddImage(path);
            mainRow.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            mainRow.Format.Alignment = ParagraphAlignment.Center;

            foreach (var items in itemGroups)
            {
                mainRow = table.AddRow();
                mainRow.BottomPadding = 5;
                mainRow.TopPadding = 5;
                mainRow.VerticalAlignment = VerticalAlignment.Center;

                // Create the photos table, one column, and a row for each photo
                var subTable = new Table();
                subTable.Rows.LeftIndent = 0;
                subTable.BottomPadding = 3;
                subTable.TopPadding = 3;

                column = subTable.AddColumn("3.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("1.7cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                var row = subTable.AddRow();
                row.Cells[0].MergeRight = 9;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
                row.Cells[0].Format.Font.Bold = true;
                row.Cells[0].Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);
                row.Cells[0].AddParagraph("Measurements in Inches");

                row = subTable.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Size = 6;
                row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
                row.Format.Font.Bold = true;
                row.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);
                row.Cells[0].AddParagraph("Dimension");
                row.Cells[0].Format.Font.Size = 9;
                row.Cells[1].AddParagraph("Code");
                row.Cells[2].AddParagraph("Tolerance");

                // add size headers
                for (int i = 0; i < items.Count; i++)
                {
                    row.Cells[i + 3].AddParagraph(items[i].Size);
                }

                // get dims ordered by code and the entire list of measurements to query against
                var orderedDims = profile.Dimensions.OrderBy(x => x.Code).ToList();
                var measurements = items.SelectMany(x => x.Measurements);
                foreach (var dimension in orderedDims)
                {
                    MigraDoc.DocumentObjectModel.Color rowColor;
                    MigraDoc.DocumentObjectModel.Color rowHeaderColor;
                    if (rowCount % 2 == 0)
                    {
                        rowColor = MigraDoc.DocumentObjectModel.Color.FromCmyk(19.3, 10.53, 0, 10.59);
                        rowHeaderColor = MigraDoc.DocumentObjectModel.Color.FromCmyk(21.05, 0, 6.7, 10.59);
                    }
                    else
                    {
                        rowColor = MigraDoc.DocumentObjectModel.Color.FromCmyk(8.71, 4.56, 0, 5.49);
                        rowHeaderColor = MigraDoc.DocumentObjectModel.Color.FromCmyk(16.12, 0, 5.55, 5.1);
                    }

                    // check if any item has this dimension, only add if one does
                    if (measurements.Any(x => x.Dimension == dimension))
                    {
                        row = subTable.AddRow();
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Shading.Color = rowColor;
                        row.Cells[0].AddParagraph(dimension.Value);
                        row.Cells[0].Shading.Color = rowHeaderColor;
                        row.Cells[0].Format.Font.Bold = true;
                        row.Cells[1].AddParagraph(dimension.Code);
                        row.Cells[1].Shading.Color = rowHeaderColor;
                        row.Cells[1].Format.Font.Bold = true;
                        row.Cells[2].AddParagraph(dimension.Tolerance);
                        row.Cells[2].Shading.Color = rowHeaderColor;
                        row.Cells[2].Format.Font.Bold = true;

                        // loop items
                        for (int i = 0; i < items.Count; i++)
                        {
                            var item = items[i];

                            // check if measurement exists for item, if so add
                            var meas = item.Measurements.FirstOrDefault(x => x.Dimension == dimension);
                            if (meas != null)
                            {
                                row.Cells[i + 3].AddParagraph(meas.Value);
                            }
                        }

                        rowCount++;
                    }
                }

                if (product is Glove)
                {
                    row = subTable.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 50, 0);
                    row.Cells[0].AddParagraph("Hem Color");
                    row.Cells[0].Format.Font.Bold = true;

                    for (int i = 0; i < items.Count; i++)
                    {
                        row.Cells[i + 3].AddParagraph(items[i].HemColor == null ? "" : items[i].HemColor);
                    }
                }

                mainRow.Cells[0].Elements.Add(subTable);
            }

            return table;
        }

        private Table CreatePackagingSection()
        {
            var table = new Table();

            var column = table.AddColumn("19cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            var packSizeGroups = orderedItems.SelectMany(x => x.PackSizes
                .OrderBy(p => p.SKU))
                .Chunk(7)
                .Select(chunk => chunk.ToList())
                .ToList();

            foreach (var packSizes in packSizeGroups)
            {
                var mainRow = table.AddRow();
                mainRow.BottomPadding = 5;
                mainRow.TopPadding = 5;
                mainRow.VerticalAlignment = VerticalAlignment.Center;

                // Create the photos table, one column, and a row for each photo
                var subTable = new Table();
                subTable.Rows.LeftIndent = 0;
                subTable.BottomPadding = 3;
                subTable.TopPadding = 3;

                column = subTable.AddColumn("3.6cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("2.2cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("2.2cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("2.2cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("2.2cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("2.2cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("2.2cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                subTable.AddColumn("2.2cm");
                column.Format.Alignment = ParagraphAlignment.Left;

                // Create the header of the table
                var row = subTable.AddRow();
                row.BottomPadding = 5;
                row.TopPadding = 5;
                row.HeadingFormat = true;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
                row.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);
                row.Cells[0].AddParagraph("Packaging");
                row.Cells[0].Format.Font.Size = 20;
                row.Cells[0].MergeRight = 7;

                row = subTable.AddRow();
                row.Format.Font.Bold = true;
                row.Format.Font.Size = 6;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
                row.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);

                for (int i = 0; i < packSizes.Count; i++)
                {
                    row.Cells[i + 1].AddParagraph(packSizes[i].SKU);
                }

                row = subTable.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph("Units("+packSizes[0].UnitOfMeasure+") in Pack:");
                row.Cells[0].Format.Font.Bold = true;
                row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(19.3, 10.53, 0, 10.59);

                for (int i = 0; i < packSizes.Count; i++)
                {
                    row.Cells[i + 1].AddParagraph(packSizes[i].UnitsPerPack.ToString());
                }

                row = subTable.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph("Units(" + packSizes[0].UnitOfMeasure + ") Per Case:");
                row.Cells[0].Format.Font.Bold = true;
                row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(8.71, 4.56, 0, 5.49);

                for (int i = 0; i < packSizes.Count; i++)
                {
                    row.Cells[i + 1].AddParagraph(packSizes[i].UnitsPerCase.ToString());
                }

                mainRow.Cells[0].Elements.Add(subTable);
            }

            var allPackSizes = orderedItems.SelectMany(x => x.PackSizes
                .OrderBy(p => p.SKU))
                .ToList();

            foreach (var pack in allPackSizes)
            {
                var mainRow = table.AddRow();
                mainRow.BottomPadding = 5;
                mainRow.TopPadding = 5;
                mainRow.VerticalAlignment = VerticalAlignment.Center;

                var subTable = CreateCartonDataTable(pack);
                mainRow.Cells[0].Elements.Add(subTable);
            }

            return table;
        }

        private Table CreateTagAssemblySection()
        {
            var table = new Table();

            var column = table.AddColumn("19cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            if (product.Photos.Any(x => x.Type == "Neck Label"))
            {
                //Row row = table.AddRow();
                //row.BottomPadding = 5;
                //row.TopPadding = 5;
                //row.HeadingFormat = true;
                //row.VerticalAlignment = VerticalAlignment.Center;
                //row.Format.Font.Bold = true;
                //row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
                //row.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);
                //row.Cells[0].AddParagraph("Neck Label");
                //row.Cells[0].Format.Font.Size = 20;

                //var subTable = CreatePhotosSection("Neck Label");
                //row = table.AddRow();

                //row.Cells[0].Elements.Add(subTable);
            }
            else
            {
                var packSizeGroups = orderedItems.SelectMany(x => x.PackSizes
                    .OrderBy(p => p.SKU))
                    .Chunk(7)
                    .Select(chunk => chunk.ToList())
                    .ToList();

                foreach (var packSizes in packSizeGroups)
                {
                    var mainRow = table.AddRow();
                    mainRow.BottomPadding = 5;
                    mainRow.TopPadding = 5;
                    mainRow.VerticalAlignment = VerticalAlignment.Center;

                    // Create the photos table, one column, and a row for each photo
                    var subTable = new Table();
                    subTable.Rows.LeftIndent = 0;
                    subTable.BottomPadding = 3;
                    subTable.TopPadding = 3;

                    column = subTable.AddColumn("3.6cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    subTable.AddColumn("2.2cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    subTable.AddColumn("2.2cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    subTable.AddColumn("2.2cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    subTable.AddColumn("2.2cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    subTable.AddColumn("2.2cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    subTable.AddColumn("2.2cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    subTable.AddColumn("2.2cm");
                    column.Format.Alignment = ParagraphAlignment.Left;

                    // Create the header of the table
                    Row row = subTable.AddRow();
                    row.BottomPadding = 5;
                    row.TopPadding = 5;
                    row.HeadingFormat = true;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Format.Font.Bold = true;
                    row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
                    row.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);
                    row.Cells[0].AddParagraph("Tag Assembly");
                    row.Cells[0].Format.Font.Size = 20;
                    row.Cells[0].MergeRight = 7;

                    row = subTable.AddRow();
                    row.Format.Font.Bold = true;
                    row.Format.Font.Size = 6;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
                    row.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);

                    for (int i = 0; i < packSizes.Count; i++)
                    {
                        row.Cells[i + 1].AddParagraph(packSizes[i].SKU);
                    }

                    row = subTable.AddRow();
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Cells[0].AddParagraph("Security Tag:");
                    row.Cells[0].Format.Font.Bold = true;
                    row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(19.3, 10.53, 0, 10.59);

                    for (int i = 0; i < packSizes.Count; i++)
                    {
                        row.Cells[i + 1].AddParagraph(packSizes[i].Sensormatic ? "Yes" : "No");
                    }

                    mainRow.Cells[0].Elements.Add(subTable);
                }

                var sewnInRow = table.AddRow();
                sewnInRow.BottomPadding = 5;
                sewnInRow.TopPadding = 5;
                sewnInRow.VerticalAlignment = VerticalAlignment.Center;
                sewnInRow.Format.Alignment = ParagraphAlignment.Center;

                var sewnInTable = CreateSewnInLabel();
                sewnInRow.Cells[0].Elements.Add(sewnInTable);

                var tagAssGroups = orderedItems.SelectMany(x => x.PackSizes
                    .Where(p => !string.IsNullOrWhiteSpace(p.TagAssembly))
                    .OrderBy(p => p.SKU))
                    .GroupBy(x => x.TagAssembly)
                    .Select(grp => grp.ToList())
                    .ToList();

                foreach (var group in tagAssGroups)
                {
                    var skus = group.Select(x => x.SKU).ToList();
                    string message = "Tag Assembly for " + string.Join(", ", skus);
                    string path = @"\\wcmktg.com\WCPG-HQ\Public\edi\exe\MarketingDatabase\HangTags\" + group[0].TagAssembly + ".png";

                    if (File.Exists(path))
                    {
                        try
                        {
                            // get image, resize, and add description text
                            System.Drawing.Image image = System.Drawing.Image.FromFile(path);
                            image = imageService.ScaleImage(image, 700, 1000);
                            image = imageService.ImposeTextOnImageBottom(image, message);
                            var row = table.AddRow();
                            // save temp image
                            path = CreateTempImage(image);
                            /*if (path.Contains("exception")){
                                row.BottomPadding = 10;
                                row.TopPadding = 10;
                                row.Cells[0].AddParagraph().AddText("An error Occurred:  " + path);
                                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                            }*/
                            // add image to sheet
                            row = table.AddRow();
                            row.BottomPadding = 10;
                            row.TopPadding = 10;
                            row.Cells[0].AddParagraph().AddImage(path);
                            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        } catch (Exception e)
                        {
                            var row = table.AddRow();
                            row.BottomPadding = 10;
                            row.TopPadding = 10;
                            row.Cells[0].AddParagraph().AddText("An error Occurred:  " + e.ToString());
                            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                        }
                    } else
                    {
                        var row = table.AddRow();
                        row.BottomPadding = 10;
                        row.TopPadding = 10;
                        row.Cells[0].AddParagraph().AddText(message + "  " + path);
                        row.Cells[0].VerticalAlignment=VerticalAlignment.Center;
                    }
                }
            }

            return table;
        }

        private Table CreateSewnInLabel()
        {
            var table = new Table();
            table.Rows.LeftIndent = "7.5cm";
            table.BottomPadding = 3;
            table.TopPadding = 3;

            var column = table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;
            column.Format.Font.Size = 12;

            Row row = table.AddRow();
            row.TopPadding = 5;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].AddParagraph(product.BasePartNumber);

            row = table.AddRow();
            row.TopPadding = 5;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].AddParagraph("Large");

            row = table.AddRow();
            row.TopPadding = 10;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].AddParagraph(product.MaterialContent == null ? "" : product.MaterialContent);

            row = table.AddRow();
            row.TopPadding = 5;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].AddParagraph("RN# " + (product.RNNumber == null ? "" : product.RNNumber));

            row = table.AddRow();
            row.TopPadding = 5;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].AddParagraph("Made in " + (product.CountryOfOrigin == null ? "" : product.CountryOfOrigin));

            row = table.AddRow();
            row.BottomPadding = 5;
            row.TopPadding = 5;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 99, 100, 0);
            row.Cells[0].AddParagraph("PO#00000000000");

            row = table.AddRow();
            row.TopPadding = 15;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].Format.Font.Underline = Underline.Single;
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Sewn-in Label");

            table.SetEdge(0, 0, 1, table.Rows.Count - 1, Edge.Box, BorderStyle.Single, 2);

            return table;
        }

        private Table CreateHistorySection()
        {
            // Create the item table
            var table = new Table();
            table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn("15cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.BottomPadding = 5;
            row.TopPadding = 5;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(61.03, 33.8, 0, 16.47);
            row.Cells[0].AddParagraph("Revision History");
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 0);
            row.Cells[0].Format.Font.Size = 20;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeRight = 1;


            var historyItemGroups = historyService.GetHistoryForProduct(product.BasePartNumber)
                .Where(x => x.Date.Date > DateTime.Now.AddDays(-90))
                .GroupBy(x => x.GetType())
                .Select(grp => grp.ToList())
                .ToList();

            List<HistoryItem> historyItems = new List<HistoryItem>();

            foreach (var group in historyItemGroups)
            {
                List<HistoryItem> subList = new List<HistoryItem>();
                if (group[0] is SKUHistoryItem)
                {
                    var propertyOrdered = group.Select(x => x as SKUHistoryItem)
                        .GroupBy(x => new { x.SKU, x.Property })
                        .Select(x => x.ToList())
                        .ToList();

                    foreach (var subGroup in propertyOrdered)
                    {
                        var mostRecentChange = subGroup
                            .OrderByDescending(x => x.Date)
                            .First();

                        historyItems.Add(mostRecentChange);
                    }
                }
                else if (group[0] is ProductHistoryItem)
                {
                    var propertyOrdered = group.Select(x => x as ProductHistoryItem)
                        .GroupBy(x => new { x.BasePartNumber, x.Property })
                        .Select(x => x.ToList())
                        .ToList();

                    foreach (var subGroup in propertyOrdered)
                    {
                        var mostRecentChange = subGroup
                            .OrderByDescending(x => x.Date)
                            .First();

                        historyItems.Add(mostRecentChange);
                    }
                }
            }

            var historyDateGroups = historyItems
                .OrderByDescending(x => x.Date.Date)
                .GroupBy(x => x.Date.Date)
                .Select(grp => grp.ToList())
                .ToList();

            for (int i = 0; i < historyDateGroups.Count; i++)
            {
                var group = historyDateGroups[i];

                MigraDoc.DocumentObjectModel.Color color;
                if (i % 2 == 0)
                    color = MigraDoc.DocumentObjectModel.Color.FromCmyk(19.3, 10.53, 0, 10.59);
                else
                    color = MigraDoc.DocumentObjectModel.Color.FromCmyk(8.71, 4.56, 0, 5.49);

                ParagraphFormat rowHeaderFormat = new ParagraphFormat();
                rowHeaderFormat.Alignment = ParagraphAlignment.Center;
                rowHeaderFormat.Font.Bold = true;
                rowHeaderFormat.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 100);
                rowHeaderFormat.Font.Size = 10;
                rowHeaderFormat.Alignment = ParagraphAlignment.Left;

                ParagraphFormat rowDataFormat = new ParagraphFormat();
                rowDataFormat.Alignment = ParagraphAlignment.Left;
                rowDataFormat.Font.Bold = false;
                rowDataFormat.Font.Color = MigraDoc.DocumentObjectModel.Color.FromCmyk(0, 0, 0, 100);
                rowDataFormat.Font.Size = 10;
                rowDataFormat.Alignment = ParagraphAlignment.Left;

                row = table.AddRow();
                row.BottomPadding = 3;
                row.TopPadding = 3;
                row.Shading.Color = color;
                row.Cells[0].AddParagraph(group[0].Date.ToShortDateString());
                row.Cells[0].Format = rowHeaderFormat;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

                string changes = "";
                foreach (var item in group)
                {
                    changes += "• " + item.Message + "\n";
                }

                row.Cells[1].AddParagraph(changes);
                row.Cells[1].Format = rowDataFormat;
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }

            return table;
        }

        private string CreateTempImage(System.Drawing.Image image)
        {
            string path;
                path = Path.Combine(tempDir, Guid.NewGuid().ToString() + ".jpg");
                image.Save(path);
                paths.Add(path);
            return path;

        }

        private void CleanupTemp()
        {
            foreach (var path in paths)
            {
                try
                {
                    File.Delete(path);
                }
                catch
                {
                    throw;
                }
            }
        }
    }
}