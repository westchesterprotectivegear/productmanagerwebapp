﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using AutoMapper;
using Ninject;
using ProductManager.Entities;
using ProductManager.Models;

namespace ProductManager.Services
{
    public interface IProductService
    {
        Product Get(string partNumber);
        bool ProductExists(string partNumber);
        bool ItemExists(string sku);
        List<Item> OrderBySize(IEnumerable<Item> items);
        List<string> OrderBySize(List<string> items);
        Product Copy(string copyFrom, string copyTo, List<string> ignoreProperties);
    }

    public class ProductService : IProductService
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }
        [Inject]
        public IRepository<Glove, ProductManagerDatabaseContext> gloveRepo { get; set; }
        [Inject]
        public IRepository<Apparel, ProductManagerDatabaseContext> apparelRepo { get; set; }
        [Inject]
        public IOptionsService optionsService { get; set; } 
        [Inject]
        public IMapper Mapper { get; set; }

        public Product Get(string partNumber)
        {
            return productRepo.Get(x => x.BasePartNumber == partNumber);
        }

        public bool ProductExists(string partNumber)
        {
            return productRepo.Get(x => x.BasePartNumber == partNumber) != null;
        }

        public bool ItemExists(string sku)
        {
            return itemRepo.Get(sku) != null;
        }

        public List<Item> OrderBySize(IEnumerable<Item> items)
        {
            var sizes = optionsService.GetSizes();
            return items.OrderBy(x => sizes.IndexOf(x.Size)).ToList();
        }

        public List<string> OrderBySize(List<string> items)
        {
            var sizes = optionsService.GetSizes();
            return items.OrderBy(x => sizes.IndexOf(x)).ToList();
        }

        public Product Copy(string copyFrom, string copyTo, List<string> ignoreProperties)
        {
            Product productToCopy = Get(copyFrom);
            Product clone;

            // add non copy props
            if (ignoreProperties == null)
                ignoreProperties = new List<string>();

            ignoreProperties.AddRange(productToCopy.GetType().GetProperties()
                .Where(x => x.GetCustomAttributes(typeof(CanCopyAttribute), false).Count() == 0
                    && (x.PropertyType.IsPrimitive || x.PropertyType == typeof(string))
                    && x.Name != "DimensionProfileName")
                .Select(x => x.Name));

            var config = new MapperConfiguration(cfg =>
            {
                if (productToCopy is Glove)
                {
                    var map = cfg.CreateMap<Glove, Glove>();
                    map.ForMember("BasePartNumber", opt => opt.UseValue(copyTo));
                    map.ForMember("History", opt => opt.Ignore());
                    foreach (var prop in ignoreProperties)
                    {
                        map.ForMember(prop, opt => opt.Ignore());
                    }
                }
                else
                {
                    var map = cfg.CreateMap<Apparel, Apparel>();
                    map.ForMember("BasePartNumber", opt => opt.UseValue(copyTo));
                    map.ForMember("History", opt => opt.Ignore());
                    foreach (var prop in ignoreProperties)
                    {
                        map.ForMember(prop, opt => opt.Ignore());
                    }
                }

                cfg.CreateMap<Measurement, Measurement>().IgnoreId()
                    .ForMember("ItemID", opt => opt.Ignore());
                cfg.CreateMap<FilePM, FilePM>().IgnoreId()
                    .ForMember("BasePartNumber", opt => opt.UseValue(copyTo));
                cfg.CreateMap<Facet, Facet>().IgnoreId()
                    .ForMember("BasePartNumber", opt => opt.UseValue(copyTo));
                cfg.CreateMap<Photo, Photo>().IgnoreId()
                    .ForMember("BasePartNumber", opt => opt.UseValue(copyTo));
                cfg.CreateMap<Feature, Feature>().IgnoreId()
                    .ForMember("BasePartNumber", opt => opt.UseValue(copyTo));
                cfg.CreateMap<CrossReference, CrossReference>().IgnoreId()
                    .ForMember("BasePartNumber", opt => opt.UseValue(copyTo));
                cfg.CreateMap<Item, Item>().IgnoreId()
                    .ForMember("BasePartNumber", opt => opt.UseValue(copyTo))
                    .ForMember("PackSizes", opt => opt.Ignore());
            });

            var dynaMapper = config.CreateMapper();

            if (productToCopy is Glove)
                clone = dynaMapper.Map<Glove>(productToCopy);
            else
                clone = dynaMapper.Map<Apparel>(productToCopy);

            // remove some photo types
            var noCopyPhotos = clone.Photos.Where(x => x.Type == "Carton Marking" || x.Type == "Hang Tag").ToList();
            foreach (var photo in noCopyPhotos)
            {
                clone.Photos.Remove(photo);
            }

            return clone;
        }
    }

    public static class IgnoreIdExtensions
    {
        public static IMappingExpression<TSource, TDestination> IgnoreId<TSource, TDestination>(
                   this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceType = typeof(TSource);

            foreach (var property in sourceType.GetProperties())
            {
                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(sourceType)[property.Name];
                IdAttribute attribute = (IdAttribute)descriptor.Attributes[typeof(IdAttribute)];
                if (attribute != null)
                    expression.ForMember(property.Name, opt => opt.Ignore());
            }
            return expression;
        }
    }
}