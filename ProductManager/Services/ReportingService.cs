﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using Ninject;
using OfficeOpenXml;
using ProductManager.Entities;

namespace ProductManager.Services
{
    public interface IReportingService
    {
        List<string> GetSKUs();
        List<string> GetFields();
        List<string> GetCopyableFields(Type type);
        byte[] CreateReport(List<string> skus, List<string> fields, bool features, bool xrefs, bool thumbnails, bool specs, bool pricing);
        byte[] MeasurementReport(string partNumber);
    }

    public class ReportingService : IReportingService
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<PackSize, ProductManagerDatabaseContext> packRepo { get; set; }
        [Inject]
        public IRepository<Feature, ProductManagerDatabaseContext> featureRepo { get; set; }
        [Inject]
        public IRepository<CrossReference, ProductManagerDatabaseContext> xrefRepo { get; set; }
        [Inject]
        public IImageService imageService { get; set; }

        public List<string> GetSKUs()
        {
            return (from x in packRepo.GetAll() select x.SKU).OrderBy(x => x).ToList();
        }

        public List<string> GetFields()
        {
            var props = (from x in typeof(PackSize).GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)) select x.Name).ToList();
            props.AddRange((from x in typeof(Product).GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)) select x.Name));
            props.AddRange((from x in typeof(Glove).GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)) select x.Name));
            props.AddRange((from x in typeof(Apparel).GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)) select x.Name));
            return props.OrderBy(x => x).ToList();
        }

        public List<string> GetCopyableFields(Type type)
        {
            var props = (from x in type.GetProperties()
                         .Where(x => x.GetCustomAttributes(typeof(CanCopyAttribute), false).Count() > 0
                         && (x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)))
                         select x.Name);
            return props.OrderBy(x => x).ToList();
        }

        public byte[] CreateReport(List<string> skus, List<string> fields, bool features, bool xrefs, bool thumbnails, bool specs, bool pricing)
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet sheet = package.Workbook.Worksheets.Add("SKU Data");

                // first column will always be sku, freeze it so it's always visible. color first row/column
                sheet.Cells[1, 1].Value = "SKU";
                sheet.View.FreezePanes(2, 2);
                
                // fill rest of column headers from fields list
                for (int i = 0; i < fields.Count; i++)
                    sheet.Cells[1, i + 2].Value = fields[i];

                int endCol = fields.Count + 1;
                // fill pricing headers if necessary
                if (pricing)
                {
                    int col = fields.Count + 2;
                    sheet.Cells[1, col].Value = "A Price";
                    sheet.Cells[1, col + 1].Value = "C Price";
                    sheet.Cells[1, col + 2].Value = "D Price";
                    sheet.Cells[1, col + 3].Value = "E Price";

                    endCol += 4;
                }

                var skuRange = sheet.Cells[2, 1, skus.Count + 1, 1];
                skuRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                skuRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);

                var fieldRange = sheet.Cells[1, 1, 1, endCol];
                fieldRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                fieldRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Navy);
                fieldRange.Style.Font.Color.SetColor(System.Drawing.Color.White);

                // fill data.
                for (int i = 0; i < skus.Count; i++)
                {
                    var pack = packRepo.Get(skus[i]);
                    sheet.Cells[i + 2, 1].Value = pack.SKU;

                    var props = typeof(PackSize).GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string));
                    var apparelProps = typeof(Apparel).GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string));
                    var gloveProps = typeof(Glove).GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string));
                    if (pack != null && pack.Item != null)
                    {
                        for (int j = 0; j < fields.Count; j++)
                        {
                            if (props.Any(x => x.Name == fields[j]))
                            {
                                // get property value, check null and write to sheet
                                var value = props.First(x => x.Name == fields[j]).GetValue(pack);
                                sheet.Cells[i + 2, j + 2].Value = value == null ? "" : value.ToString();
                            }
                            else if (pack.Item.Apparel != null && apparelProps.Any(x => x.Name == fields[j]))
                            {
                                // get property value, check null and write to sheet
                                var value = apparelProps.First(x => x.Name == fields[j]).GetValue(pack.Item.Apparel);
                                sheet.Cells[i + 2, j + 2].Value = value == null ? "" : value.ToString();
                            }
                            else if (pack.Item.Glove != null && gloveProps.Any(x => x.Name == fields[j]))
                            {
                                // get property value, check null and write to sheet
                                var value = gloveProps.First(x => x.Name == fields[j]).GetValue(pack.Item.Glove);
                                sheet.Cells[i + 2, j + 2].Value = value == null ? "" : value.ToString();
                            }
                        }
                    }
                    // fill pricing data if necessary
                    if (pricing && pack.PriceListData != null)
                    {
                        int col = fields.Count + 2;
                        sheet.Cells[i + 2, col].Value = pack.PriceListData.APrice;
                        sheet.Cells[i + 2, col + 1].Value = pack.PriceListData.CPrice;
                        sheet.Cells[i + 2, col + 2].Value = pack.PriceListData.DPrice;
                        sheet.Cells[i + 2, col + 3].Value = pack.PriceListData.EPrice;
                    }
                }

                // Add Features
                if (features)
                {
                    sheet = package.Workbook.Worksheets.Add("Features");
                    sheet.Cells[1, 1].Value = "SKU";

                    // int to keep track of number of columns
                    int mostFeatures = 0;
                    for (int i = 0; i < skus.Count; i++)
                    {
                        var product = packRepo.Get(skus[i]);
                        sheet.Cells[i + 2, 1].Value = product.SKU;

                        var productFeatures = featureRepo.GetAll().Where(x => x.BasePartNumber == product.BasePartNumber).ToList();
                        for (int j = 0; j < productFeatures.Count; j++)
                        {
                            sheet.Cells[i + 2, j + 2].Value = productFeatures[j].Value;
                            sheet.Cells[1, j + 2].Value = "Feature " + (j + 1);
                        }

                        mostFeatures = productFeatures.Count > mostFeatures ? productFeatures.Count : mostFeatures;
                    }

                    // freeze panes and style
                    sheet.View.FreezePanes(2, 2);
                    skuRange = sheet.Cells[2, 1, skus.Count + 1, 1];
                    skuRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    skuRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);

                    var featureRange = sheet.Cells[1, 1, 1, mostFeatures + 1];
                    featureRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    featureRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Navy);
                    featureRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                }

                // Add Cross References
                if (xrefs)
                {
                    sheet = package.Workbook.Worksheets.Add("Cross References");
                    sheet.Cells[1, 1].Value = "SKU";

                    // get list of unique comptitors for column headers
                    List<string> competitors = new List<string>();
                    foreach (var sku in skus)
                    {
                        var product = packRepo.Get(sku);
                        var crossRefs = xrefRepo.GetAll().Where(x => x.BasePartNumber == product.BasePartNumber);
                        competitors.AddRange(from x in crossRefs select x.Company);
                    }
                    competitors = competitors.Distinct().OrderBy(x => x).ToList();

                    for (int i = 0; i < competitors.Count; i++)
                        sheet.Cells[1, i + 2].Value = competitors[i];

                    for (int i = 0; i < skus.Count; i++)
                    {
                        var product = packRepo.Get(skus[i]);
                        sheet.Cells[i + 2, 1].Value = product.SKU;

                        var productXRefs = xrefRepo.GetAll().Where(x => x.BasePartNumber == product.BasePartNumber).ToList();
                        foreach (var xref in productXRefs)
                        {
                            // find where to place xref number
                            int columnIndex = competitors.IndexOf(xref.Company) + 2;
                            sheet.Cells[i + 2, columnIndex].Value = xref.CrossReferenceNumber;
                        }
                    }

                    // freeze panes and style
                    sheet.View.FreezePanes(2, 2);
                    skuRange = sheet.Cells[2, 1, skus.Count + 1, 1];
                    skuRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    skuRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);

                    var xRefRange = sheet.Cells[1, 1, 1, competitors.Count + 1];
                    xRefRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    xRefRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Navy);
                    xRefRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                }

                // Add Specs
                if (specs)
                {
                    // get all items and split into smaller groups to avoid 2100 item max with contain
                    var items = packRepo.GetAll().ToList();

                    var itemGroups = items
                        .Select((x, i) => new { Index = i, Value = x })
                        .GroupBy(x => x.Index / 1000)
                        .Select(x => x.Select(v => v.Value).ToList())
                        .ToList();

                    // get selected items
                    var selectedItems = new List<PackSize>();
                    foreach (var group in itemGroups)
                    {
                        selectedItems.AddRange(group.Where(x => skus.Contains(x.SKU)));
                    }

                    // get groups of items by profile
                    var itemsByProfile = selectedItems.OrderBy(x => x.SKU)
                        .GroupBy(x => x.Item.Product.DimensionProfile)
                        .OrderByDescending(g => g.Count())
                        .Select(g => g.ToList()).ToList();

                    foreach (var group in itemsByProfile)
                    {
                        var profile = group.First().Item.Product.DimensionProfile;
                        sheet = package.Workbook.Worksheets.Add(profile.Name);

                        var orderedDimensions = profile.Dimensions.OrderBy(x => x.Code).ToList();
                        for (int i = 0; i < orderedDimensions.Count; i++)
                        {
                            sheet.Cells[1, 2 + i].Value = orderedDimensions[i].Value;

                            for (int j = 0; j < group.Count; j++)
                            {
                                var pack = group[j];
                                sheet.Cells[2 + j, 1].Value = pack.SKU;

                                var meas = pack.Item.Measurements.FirstOrDefault(x => x.Dimension == orderedDimensions[i]);
                                if (meas != null)
                                    sheet.Cells[2 + j, 2 + i].Value = meas.Value;
                            }
                        }

                        // freeze panes and style
                        sheet.View.FreezePanes(2, 2);
                        skuRange = sheet.Cells[2, 1, group.Count + 1, 1];
                        skuRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        skuRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightYellow);

                        var dimRange = sheet.Cells[1, 1, 1, profile.Dimensions.Count + 1];
                        dimRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        dimRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Navy);
                        dimRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    }
                }

                // Add Thumbnails
                if (thumbnails)
                {
                    // insert a new column at the beginning of each sheet
                    for (int i = 1; i <= package.Workbook.Worksheets.Count; i++)
                    {
                        sheet = package.Workbook.Worksheets[i];
                        sheet.InsertColumn(1, 1);
                    }

                    Image image = null;
                    string lastImagePath = "";
                    for (int i = 0; i < skus.Count; i++)
                    {
                        string sku = skus[i];
                        PackSize pack = packRepo.Get(sku);
                        Photo photo = pack.Item.Product.PrimaryPhoto;

                        string path = @"\\fred\images\ProductImages\Thumbnails\" + photo.Name;
                        if (File.Exists(path))
                        {
                            // just use the last image if it's the same
                            if (lastImagePath != path || image == null)
                            {
                                image = Image.FromFile(path);
                                image = imageService.ScaleImage(image, 50, 50);
                            }

                            // insert image into each sheet
                            double imageTop = 0;
                            for (int j = 1; j <= package.Workbook.Worksheets.Count; j++)
                            {
                                sheet = package.Workbook.Worksheets[j];
                                var picture = sheet.Drawings.AddPicture(sku, image);
                                picture.SetPosition(50 * i + 25 + (i * 16), 0);
                            }

                            lastImagePath = path;
                        }

                        sheet.Row(i + 2).Height = 50;
                    }

                    sheet.View.FreezePanes(2, 3);
                }

                return package.GetAsByteArray();
            }
        }

        public byte[] MeasurementReport(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);
            var items = product.Items;

            byte[] report;
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet sheet = package.Workbook.Worksheets.Add("Measurements");

                var orderedDimensions = product.DimensionProfile.Dimensions.OrderBy(x => x.Code).ToList();
                for (int i = 0; i < orderedDimensions.Count; i++)
                {
                    sheet.Cells[1, 2 + i].Value = orderedDimensions[i].Value;

                    for (int j = 0; j < items.Count; j++)
                    {
                        var item = items[j];
                        sheet.Cells[2 + j, 1].Value = item.Size;

                        var meas = item.Measurements.FirstOrDefault(x => x.Dimension == orderedDimensions[i]);
                        if (meas != null)
                            sheet.Cells[2 + j, 2 + i].Value = meas.Value;
                    }
                }

                report = package.GetAsByteArray();
            }

            return report;
        }
    }
}