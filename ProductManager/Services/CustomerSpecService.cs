﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Ninject;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using ProductManager.Entities;

namespace ProductManager.Services
{
    public interface ICustomerSpecService
    {
        void GenerateSpec(string partNumber);
        void GenerateOutdated();
    }

    public class CustomerSpecService : ICustomerSpecService
    {
        [Inject]
        public IImageService imageService { get; set; }
        [Inject]
        public IProductService productService { get; set; }
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<HistoryItem, ProductManagerDatabaseContext> historyRepo { get; set; }
        [Inject]
        public IRepository<Collection, ProductManagerDatabaseContext> collectionRepo { get; set; }
        [Inject]
        public IRepository<PackSize, ProductManagerDatabaseContext> packRepo { get; set; }
        [Inject]
        public IRepository<Prop65Chemical, ProductManagerDatabaseContext> chemRepo { get; set; }


        private int mainColumnWidth = 278;

        private string specBackground = "";
        private Color primary = Color.FromCmyk(100, 69, 0, 9);
        private Color white = Color.FromCmyk(0, 0, 0, 0);
        private Color black = Color.FromCmyk(75, 68, 67, 90);
        private Color grey = Color.FromCmyk(0, 0, 0, 50);

        private Document document { get; set; }
        private Product product { get; set; }
        private List<Item> orderedItems { get; set; }

        private string tempImageName { get; set; }

        public void GenerateOutdated()
        {
            var mods = historyRepo.GetAll().ToList();
            var products = mods.Where(x => !string.IsNullOrWhiteSpace(x.BasePartNumber))
                .Select(x => x.BasePartNumber)
                .OrderBy(x => x)
                .Distinct()
                .ToList();

            //var products = productRepo.GetGroup(x => x.ActiveIndustrial).Select(x => x.BasePartNumber).ToList();
            foreach (var product in products)
            {
                try
                {
                    GenerateSpec(product);
                }
                catch (Exception e)
                {
                    // do something useful, right now it's mostly images that are too large.
                }
            }

           // var propProducts = productRepo.GetAll().Where(x => x.Prop65Chemicals != null).ToList();
            //foreach (var prod in propProducts)
            //{
            //    GenerateSpec(prod.BasePartNumber);
            //}
        }

        private void LoadSpecSettings(string template)
        {
            string backgroundPath = HttpContext.Current.Server.MapPath("~/Content/SpecBackgrounds/");

            switch (template)
            {
                //case "Barracuda":
                //specBackground = Path.Combine(backgroundPath, "Barracuda.pdf");
                //primary = Color.FromCmyk(73.91, 0, 55.07, 45.88);
                //break;
                //case "Extreme Work":
                //specBackground = Path.Combine(backgroundPath, "ExtremeWork.pdf");
                //primary = Color.FromCmyk(17.22, 0, 67.94, 18.04);
                //break;
                case "IronCat":
                    specBackground = Path.Combine(backgroundPath, "IronCat.pdf");
                    primary = Color.FromCmyk(18.39, 3.45, 0, 65.88);
                    break;
                //case "PosiWear":
                //specBackground = Path.Combine(backgroundPath, "PosiWear.pdf");
                //primary = Color.FromCmyk(0, 18.54, 73.66, 19.61);
                //break;
                //case "Sumo Grip":
                //specBackground = Path.Combine(backgroundPath, "SumoGrip.pdf");
                //primary = Color.FromCmyk(87.77, 38.30, 0, 26.27);
                //break;
                case "Radnor":
                    specBackground = Path.Combine(backgroundPath, "Radnor.pdf");
                    primary = Color.FromCmyk(92.86, 48.70, 0, 39.61);
                    break;
                case "Performance Value Gear":
                    specBackground = Path.Combine(backgroundPath, "PVG.pdf");
                    primary = Color.FromCmyk(4.35, 2.17, 0, 45.88);
                    break;
                case "West Chester":
                default:
                    specBackground = Path.Combine(backgroundPath, "WestChester.pdf");
                    primary = Color.FromCmyk(100, 69, 0, 9);
                    break;
            }
        }

        public void GenerateSpec(string partNumber)
        {
            PdfDocument pdfDoc = new PdfDocument();
            specBackground = "";

            PdfPage page = pdfDoc.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            gfx.MFEH = PdfFontEmbedding.Always;
            page.Width = A4Width;
            page.Height = A4Height;

            product = productRepo.Get(x => x.BasePartNumber == partNumber);

            if (product == null)
                return;

            orderedItems = OrderBySize(product.Items.Where(x => x.Active));

            LoadSpecSettings(product.Brand);

            var bg = GenerateBackground();
            DocumentRenderer bgRenderer = new DocumentRenderer(bg);
            bgRenderer.PrepareDocument();

            var doc = GenerateFirstColumn();
            DocumentRenderer docRenderer = new DocumentRenderer(doc);
            docRenderer.PrepareDocument();

            var doc2 = GenerateSecondColumn();
            DocumentRenderer docRenderer2 = new DocumentRenderer(doc2);
            docRenderer2.PrepareDocument();

            XRect A4Rect = new XRect(0, 0, A4Width, A4Height);
            int pageCount = Math.Max(docRenderer.FormattedDocument.PageCount, docRenderer2.FormattedDocument.PageCount);
            for (int i = 0; i < pageCount; i++)
            {
                int pageNumber = i + 1;

                if (pageNumber > 1)
                {
                    page = pdfDoc.AddPage();
                    gfx = XGraphics.FromPdfPage(page);
                    gfx.MFEH = PdfFontEmbedding.Always;
                }

                XRect rect = new XRect(0, 0, A4Width, A4Height);
                XGraphicsContainer container = gfx.BeginContainer(rect, A4Rect, XGraphicsUnit.Point);
                bgRenderer.RenderPage(gfx, 1);
                gfx.EndContainer(container);

                if (pageNumber <= docRenderer.FormattedDocument.PageCount)
                {
                    rect = new XRect(18, 128, A4Width, A4Height);
                    container = gfx.BeginContainer(rect, A4Rect, XGraphicsUnit.Point);
                    docRenderer.RenderPage(gfx, i + 1);
                    gfx.EndContainer(container);
                }

                if (pageNumber <= docRenderer2.FormattedDocument.PageCount)
                {
                    rect = new XRect(mainColumnWidth + 36, 128, A4Width, A4Height);
                    container = gfx.BeginContainer(rect, A4Rect, XGraphicsUnit.Point);
                    docRenderer2.RenderPage(gfx, i + 1);
                    gfx.EndContainer(container);
                }
            }

            pdfDoc.Save(@"\\fred\PMFiles\CustomerSpecs\" + partNumber.Replace("/", "~") + ".pdf");
        }

        double A4Width = XUnit.FromInch(8.5).Point;
        double A4Height = XUnit.FromInch(11).Point;

        private Document GenerateBackground()
        {
            // Create a new MigraDoc document
            document = new Document();

            // Add a section to the document
            Section section = document.AddSection();
            section.PageSetup.LeftMargin = Unit.FromPoint(18);
            section.PageSetup.RightMargin = Unit.FromPoint(18);
            section.PageSetup.TopMargin = Unit.FromPoint(18);
            section.PageSetup.BottomMargin = Unit.FromPoint(100);
            section.PageSetup.PageWidth = Unit.FromInch(8.5);
            section.PageSetup.PageHeight = Unit.FromInch(11);

            var image = section.AddImage(specBackground);
            image.RelativeVertical = MigraDoc.DocumentObjectModel.Shapes.RelativeVertical.Margin;
            image.RelativeHorizontal = MigraDoc.DocumentObjectModel.Shapes.RelativeHorizontal.Margin;
            image.Width = Unit.FromPoint(574);

            return document;
        }

        private Document GenerateFirstColumn()
        {
            // Create a new MigraDoc document
            document = new Document();

            CreateStyles();

            // Add a section to the document
            Section section = document.AddSection();
            section.PageSetup.LeftMargin = 0;
            section.PageSetup.RightMargin = 0;
            section.PageSetup.TopMargin = 0;
            section.PageSetup.BottomMargin = 0;
            section.PageSetup.PageWidth = mainColumnWidth + 1;
            section.PageSetup.PageHeight = Unit.FromInch(11) - Unit.FromPoint(170);

            // get containing table for top elements
            Table table = new Table();
            table.Rows.LeftIndent = 0;
            table.LeftPadding = 0;
            table.RightPadding = 0;

            // create columns for top elements
            var column = table.AddColumn(Unit.FromPoint(mainColumnWidth));

            var row = table.AddRow();

            var par = row.Cells[0].AddNonNullParagraph((product.Brand + " " + product.BasePartNumber).ToUpper());
            par.Format.Font.Size = 25;
            par.Format.Font.Bold = true;

            par = row.Cells[0].AddNonNullParagraph(product.Name == null ? "" : product.Name.ToUpper());
            par.Format.SpaceBefore = -7;
            par.Format.Font.Size = 15;
            par.Format.Font.Bold = true;
            par.Format.Font.Name = "Avenir LT Std 55 Roman";

            XPdfFontOptions options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
            XFont font = new XFont("Avenir LT Std 55 Roman", 15, XFontStyle.Bold, options);

            par = row.Cells[0].AddNonNullParagraph(product.Description);
            par.Style = "regText";

            if (product.Features.Count > 0)
            {
                row = table.AddRow();
                par = row.Cells[0].AddNonNullParagraph("FEATURES");
                par.Style = "sectionTitle";

                var featureTable = new Table();
                featureTable.Rows.LeftIndent = 0;
                featureTable.LeftPadding = 0;
                featureTable.RightPadding = 0;

                featureTable.AddColumn(Unit.FromPoint(6));
                featureTable.AddColumn(Unit.FromPoint(272));

                foreach (var feature in product.Features.OrderBy(x => x.Rank))
                {
                    var featureRow = featureTable.AddRow();
                    featureRow.Style = "regText";

                    featureRow.Cells[0].AddNonNullParagraph("•");
                    featureRow.Cells[1].AddNonNullParagraph(feature.Value);
                }

                row.Cells[0].Elements.Add(featureTable);
            }

            if (!string.IsNullOrWhiteSpace(product.Applications))
            {
                row = table.AddRow();
                par = row.Cells[0].AddNonNullParagraph("APPLICATIONS");
                par.Style = "sectionTitle";

                var applicationsTable = new Table();
                applicationsTable.Rows.LeftIndent = 0;
                applicationsTable.LeftPadding = 0;
                applicationsTable.RightPadding = 0;

                applicationsTable.AddColumn(Unit.FromPoint(6));
                applicationsTable.AddColumn(Unit.FromPoint(272));

                foreach (var application in product.Applications.Split(','))
                {
                    var applicationRow = applicationsTable.AddRow();
                    applicationRow.Style = "regText";

                    applicationRow.Cells[0].AddNonNullParagraph("•");
                    applicationRow.Cells[1].AddNonNullParagraph(application);
                }

                row.Cells[0].Elements.Add(applicationsTable);
            }

            row = table.AddRow();
            var detailsTable = CreateBorderedTable();

            detailsTable.AddColumn(Unit.FromPoint(76));
            detailsTable.AddColumn(Unit.FromPoint(200));

            int showDetailsSection = 0;

            if (product is Glove)
            {
                var glove = product as Glove;
                showDetailsSection += AddDetailRow(detailsTable, "COLOR DETAIL", glove.ColorDetail);
                showDetailsSection += AddDetailRow(detailsTable, "COATING", glove.Coating);
                showDetailsSection += AddDetailRow(detailsTable, "LEATHER TYPE", glove.LeatherType);
                showDetailsSection += AddDetailRow(detailsTable, "THUMB TYPE", glove.ThumbType);
                showDetailsSection += AddDetailRow(detailsTable, "CUFF STYLE", glove.CuffType);
                
                if (product.Prop65LabelRequired)
                {
                    var imageService = new ProductManager.Services.ImageService();

                    string imageName = @"\\fred\Images\ProductImages\500\Warning.png";
                    //if (!File.Exists(imageName))
                    //{
                    string primaryImage = @"\\fred\Images\ProductImages\Warning.png";
                    var tempImage = File.Exists(primaryImage) ? System.Drawing.Image.FromFile(primaryImage) : System.Drawing.Image.FromFile(@"\\fred\Images\ProductImages\ImageUnavailable.png");
                    tempImage = imageService.ScaleImage(tempImage, 20, 20);
                    tempImage = imageService.PadWhite(tempImage);
                    tempImage.Save(imageName);
                    //}

                    var productChems = product.Prop65Chemicals.Split(',').ToList();
                    var chems = chemRepo.GetGroup(x => productChems.Contains(x.Name));

                    foreach (var chem in chems)
                    {
                        var detailRow = detailsTable.AddRow();

                        detailRow.Cells[0].Shading.Color = primary;
      
                        par = detailRow.Cells[0].AddNonNullParagraph("PROP 65 -"+chem.Name);
                        par.Style = "blueCell";

                        detailRow.Cells[1].Shading.Color = white;
                        
                        var image = detailRow.Cells[1].AddImage(imageName);
                        image.Width = Unit.FromPoint(15);
                        image.WrapFormat.Style=WrapStyle.Through;
                        //par = detailRow.Cells[1].AddNonNullParagraph(chem.Warning.PadLeft(5));
                        var frm = detailRow.Cells[1].AddTextFrame();
                        par = frm.AddParagraph(chem.Warning);
                        frm.MarginLeft = 15;
                        frm.MarginTop = 5;
                        frm.Height = 50;
                        frm.Width = 200;
                        par.Format.Font.Size = 7;
                        par.Format.Font.Bold = false;
                        par.Format.Font.Name = "Avenir LT Std 65 Medium";
                        //par.Style.
                        //par.Style.PadLeft(20);
                        //par = detailRow.Cells[1].;
                        //par.Style = "whiteCell";
                        //par.Style
                        showDetailsSection++;
                        options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
                        font = new XFont("Avenir LT Std 65 Medium", 7, XFontStyle.Regular, options);

                        //showDetailsSection += AddDetailRow(detailsTable, "PROP 65 - " + chem.Name, chem.Warning);
                    }
                }
            }
            else
            {
                var apparel = product as Apparel;

                if (apparel.SpecSheetCategory == "Hi-Vis")
                {
                    showDetailsSection += AddDetailRow(detailsTable, "ANSI PERFORMANCE CLASS", apparel.ANSIPerformanceClass);
                    showDetailsSection += AddDetailRow(detailsTable, "ANSI GARMENT TYPE", apparel.ANSIGarmentType);
                    showDetailsSection += AddDetailRow(detailsTable, "COLOR", apparel.ColorPattern);
                    showDetailsSection += AddDetailRow(detailsTable, "MATERIAL", apparel.BaseMaterial);
                    showDetailsSection += AddDetailRow(detailsTable, "REFLECTIVE TAPE", apparel.ReflectiveTape);
                    showDetailsSection += AddDetailRow(detailsTable, "NUMBER OF POCKETS", apparel.NumberOfPockets);
                    showDetailsSection += AddDetailRow(detailsTable, "CLOSURE", apparel.Closure);
                }
                else
                {
                    showDetailsSection += AddDetailRow(detailsTable, "COLOR PATTERN", apparel.ColorPattern);
                    showDetailsSection += AddDetailRow(detailsTable, "BASE MATERIAL", apparel.BaseMaterial);
                }
                if (product.Prop65LabelRequired)
                {
                    var imageService = new ProductManager.Services.ImageService();

                    string imageName = @"\\fred\Images\ProductImages\500\Warning.png";
                    //if (!File.Exists(imageName))
                    //{
                    string primaryImage = @"\\fred\Images\ProductImages\Warning.png";
                    var tempImage = File.Exists(primaryImage) ? System.Drawing.Image.FromFile(primaryImage) : System.Drawing.Image.FromFile(@"\\fred\Images\ProductImages\ImageUnavailable.png");
                    tempImage = imageService.ScaleImage(tempImage, 20, 20);
                    tempImage = imageService.PadWhite(tempImage);
                    tempImage.Save(imageName);
                    //}

                    var productChems = product.Prop65Chemicals.Split(',').ToList();
                    var chems = chemRepo.GetGroup(x => productChems.Contains(x.Name));

                    foreach (var chem in chems)
                    {
                        var detailRow = detailsTable.AddRow();

                        detailRow.Cells[0].Shading.Color = primary;

                        par = detailRow.Cells[0].AddNonNullParagraph("PROP 65 -" + chem.Name);
                        par.Style = "blueCell";

                        detailRow.Cells[1].Shading.Color = white;

                        var image = detailRow.Cells[1].AddImage(imageName);
                        image.Width = Unit.FromPoint(15);
                        image.WrapFormat.Style = WrapStyle.Through;
                        //par = detailRow.Cells[1].AddNonNullParagraph(chem.Warning.PadLeft(5));
                        var frm = detailRow.Cells[1].AddTextFrame();
                        par = frm.AddParagraph(chem.Warning);
                        frm.MarginLeft = 15;
                        frm.MarginTop = 5;
                        frm.Height = 50;
                        frm.Width = 200;
                        par.Format.Font.Size = 7;
                        par.Format.Font.Bold = false;
                        par.Format.Font.Name = "Avenir LT Std 65 Medium";
                        //par.Style.
                        //par.Style.PadLeft(20);
                        //par = detailRow.Cells[1].;
                        //par.Style = "whiteCell";
                        //par.Style
                        showDetailsSection++;
                        options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
                        font = new XFont("Avenir LT Std 65 Medium", 7, XFontStyle.Regular, options);
                        //showDetailsSection += AddDetailRow(detailsTable, "PROP 65 - " + chem.Name, chem.Warning);
                    }
                }

            }

            if (showDetailsSection > 0)
            {
                par = row.Cells[0].AddNonNullParagraph("SPEC DETAILS");
                par.Style = "sectionTitle";

                row.Cells[0].Elements.Add(detailsTable);
            }

            if (!string.IsNullOrWhiteSpace(product.Testing))
            {
                row = table.AddRow();
                par = row.Cells[0].AddNonNullParagraph("TEST STANDARDS");
                par.Style = "sectionTitle";

                var testingTable = new Table();
                testingTable.Rows.LeftIndent = 0;
                testingTable.LeftPadding = 0;
                testingTable.RightPadding = 0;

                testingTable.AddColumn(Unit.FromPoint(6));
                testingTable.AddColumn(Unit.FromPoint(272));

                foreach (var application in product.Testing.Split(','))
                {
                    var testingRow = testingTable.AddRow();
                    testingRow.Style = "regText";

                    testingRow.Cells[0].AddNonNullParagraph("•");
                    testingRow.Cells[1].AddNonNullParagraph(application);
                }

                row.Cells[0].Elements.Add(testingTable);
            }

            if (!string.IsNullOrWhiteSpace(product.WashingInstructions))
            {
                row = table.AddRow();
                par = row.Cells[0].AddNonNullParagraph("CARE INSTRUCTIONS");
                par.Style = "sectionTitle";

                par = row.Cells[0].AddNonNullParagraph(product.WashingInstructions);
                par.Style = "regText";
            }

            row = table.AddRow();
            par = row.Cells[0].AddNonNullParagraph("SIZES");
            par.Style = "sectionTitle";

            var sizes = orderedItems.Select(x => x.Size).ToList();
            row = table.AddRow();
            par = row.Cells[0].AddNonNullParagraph(string.Join(", ", sizes));
            par.Style = "regText";

            row = table.AddRow();
            par = row.Cells[0].AddNonNullParagraph("DIMENSIONS");
            par.Style = "sectionTitle";

            bool hasHemColor = orderedItems.Any(x => !string.IsNullOrWhiteSpace(x.HemColor) &&
                (x.HemColor.ToUpper() != "NA" && x.HemColor.ToUpper() != "N/A"));

            var showDims = product.DimensionProfile.Dimensions.Where(x => x.ActiveOnCustomerSpec).ToList();

            int columnCount = 1 + showDims.Count + (hasHemColor ? 1 : 0);

            var dimTable = CreateBorderedTable();

            double colWidth = (double)mainColumnWidth / (double)columnCount;

            for (int i = 0; i < columnCount; i++)
            {
                dimTable.AddColumn(Unit.FromPoint(colWidth));
            }

            var dimRow = dimTable.AddRow();
            dimRow.Style = "centerBlueCell";
            dimRow.Shading.Color = primary;

            par = dimRow.Cells[0].AddNonNullParagraph("ITEM SIZE");

            for (int i = 0; i < showDims.Count; i++)
            {
                par = dimRow.Cells[i + 1].AddNonNullParagraph(showDims[i].Value.ToUpper());
            }

            if (hasHemColor)
                par = dimRow.Cells[columnCount - 1].AddNonNullParagraph("HEM COLOR");

            foreach (var item in orderedItems)
            {
                dimRow = dimTable.AddRow();
                dimRow.Style = "centerWhiteCell";

                par = dimRow.Cells[0].AddNonNullParagraph(item.Size);

                for (int i = 0; i < showDims.Count; i++)
                {
                    var meas = item.Measurements.FirstOrDefault(x => x.DimensionID == showDims[i].ID);
                    if (meas != null)
                        par = dimRow.Cells[i + 1].AddNonNullParagraph(meas.Value + "\"");
                }

                if (hasHemColor)
                    par = dimRow.Cells[columnCount - 1].AddNonNullParagraph(item.HemColor);
            }

            row.Cells[0].Elements.Add(dimTable);

            section.Add(table);

            return document;
        }

        private Document GenerateSecondColumn()
        {
            // Create a new MigraDoc document
            document = new Document();

            CreateStyles();

            // Add a section to the document
            Section section = document.AddSection();
            section.PageSetup.LeftMargin = 0;
            section.PageSetup.RightMargin = 0;
            section.PageSetup.TopMargin = 0;
            section.PageSetup.BottomMargin = 0;
            section.PageSetup.PageWidth = mainColumnWidth + 1;
            section.PageSetup.PageHeight = Unit.FromInch(11) - Unit.FromPoint(235);

            // get containing table for top elements
            Table table = new Table();
            table.Rows.LeftIndent = 0;
            table.LeftPadding = 0;
            table.RightPadding = 0;

            // create columns for top elements
            var column = table.AddColumn(Unit.FromPoint(mainColumnWidth));

            var row = table.AddRow();

            var imageService = new ProductManager.Services.ImageService();

            string imageName = @"\\fred\Images\ProductImages\500\" + product.BasePartNumber.Replace('/', '~') + ".png";
            //if (!File.Exists(imageName))
            //{
                string primaryImage = @"\\fred\Images\ProductImages\" + product.PrimaryPhoto.Name;
                var tempImage = File.Exists(primaryImage) ? System.Drawing.Image.FromFile(primaryImage) : System.Drawing.Image.FromFile(@"\\fred\Images\ProductImages\ImageUnavailable.png");
                tempImage = imageService.ScaleImage(tempImage, 500, 500);
                tempImage = imageService.PadWhite(tempImage);
                tempImage.Save(imageName);
            //}

            var image = row.Cells[0].AddImage(imageName);
            image.Width = Unit.FromPoint(mainColumnWidth);

            var packSizes = orderedItems.SelectMany(x => x.PackSizes).ToList();
            if (packSizes.Count > 0)
            {
                row = table.AddRow();
                var par = row.Cells[0].AddNonNullParagraph("PACKING INFO");

                var packTable = CreateBorderedTable();

                packTable.AddColumn(Unit.FromPoint(69.5));
                packTable.AddColumn(Unit.FromPoint(69.5));
                packTable.AddColumn(Unit.FromPoint(69.5));
                packTable.AddColumn(Unit.FromPoint(69.5));

                var packRow = packTable.AddRow();

                packRow.Cells[0].Shading.Color = primary;
                par = packRow.Cells[0].AddNonNullParagraph("PACKAGING");
                par.Style = "centerBlueCell";

                packRow.Cells[1].Shading.Color = white;
                par = packRow.Cells[1].AddNonNullParagraph(packSizes[0].UnitsPerPack + " Units("+packSizes[0].UnitOfMeasure+") Per Pack, " + packSizes[0].UnitsPerCase + " Units(" + packSizes[0].UnitOfMeasure + ") Per Case");
                par.Style = "whiteCell";
                par.Format.Font.Bold = false;
                packRow.Cells[1].MergeRight = 2;

                packRow = packTable.AddRow();
                packRow.Shading.Color = primary;
                packRow.Style = "centerBlueCell";

                par = packRow.Cells[0].AddParagraph("ITEM");
                par = packRow.Cells[1].AddParagraph("PAIR UPC");
                par = packRow.Cells[2].AddParagraph("INNER PACK GTIN");
                par = packRow.Cells[3].AddParagraph("MASTER PACK GTIN");

                foreach (var packSize in packSizes)
                {
                    packRow = packTable.AddRow();
                    packRow.Style = "centerWhiteCell";

                    par = packRow.Cells[0].AddNAParagraph(packSize.SKU);
                    par = packRow.Cells[1].AddNAParagraph(packSize.UPC);
                    par = packRow.Cells[2].AddNAParagraph(packSize.InnerPackGTIN);
                    par = packRow.Cells[3].AddNAParagraph(packSize.MasterCaseGTIN);
                }

                row.Cells[0].Elements.Add(packTable);

                // start second table
                row = table.AddRow();
                packTable = CreateBorderedTable();
                packTable.AddColumn(Unit.FromPoint(69.5));
                packTable.AddColumn(Unit.FromPoint(69.5));
                packTable.AddColumn(Unit.FromPoint(69.5));
                packTable.AddColumn(Unit.FromPoint(69.5));

                packRow = packTable.AddRow();
                packRow.Shading.Color = primary;
                packRow.Style = "blueCell";

                par = packRow.Cells[0].AddParagraph("PACKAGING DIMENSIONS AND WEIGHTS");
                par.Format.LeftIndent = Unit.FromPoint(16);
                packRow.Cells[0].MergeRight = 3;

                packRow = packTable.AddRow();
                packRow.Shading.Color = primary;
                packRow.Style = "centerBlueCell";

                par = packRow.Cells[0].AddParagraph("ITEM");
                par = packRow.Cells[1].AddParagraph("PC WEIGHT (LBS)");
                par = packRow.Cells[2].AddParagraph("MASTER PACK (IN)");
                par = packRow.Cells[3].AddParagraph("MASTER PACK (LBS)");

                foreach (var packSize in packSizes)
                {
                    packRow = packTable.AddRow();
                    packRow.Style = "centerWhiteCell";

                    par = packRow.Cells[0].AddNAParagraph(packSize.SKU);
                    par = packRow.Cells[1].AddNAParagraph(packSize.PieceWeight.ToString());
                    par = packRow.Cells[2].AddNAParagraph(packSize.CaseLength.ToString() + " x " + packSize.CaseWidth.ToString() + " x " + packSize.CaseHeight.ToString());
                    par = packRow.Cells[3].AddNAParagraph(packSize.GrossCaseWeight.ToString());
                }

                packRow = packTable.AddRow();

                packRow.Cells[0].Shading.Color = primary;
                par = packRow.Cells[0].AddParagraph("COUNTRY OF ORIGIN");
                par.Style = "centerBlueCell";

                packRow.Cells[1].Shading.Color = white;
                par = packRow.Cells[1].AddNAParagraph(product.CountryOfOrigin);
                par.Style = "whiteCell";
                packRow.Cells[1].MergeRight = 2;

                row.Cells[0].Elements.Add(packTable);
            }

            section.Add(table);

            return document;
        }

        private void CreateStyles()
        {
            var style = document.Styles["Normal"];
            style.Font.Name = "Avenir Next Condensed Demi Bold";
            style.Font.Color = black;
            XPdfFontOptions options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);

            var regTextStyle = document.AddStyle("regText", "Normal");
            regTextStyle.ParagraphFormat.Font.Size = 9;
            regTextStyle.ParagraphFormat.Font.Bold = false;
            regTextStyle.ParagraphFormat.Font.Name = "Avenir LT Std 65 Medium";
            XFont font = new XFont("Avenir LT Std 65 Medium", 9, XFontStyle.Regular, options);

            var sectionTitleStyle = document.AddStyle("sectionTitle", "Normal");
            sectionTitleStyle.ParagraphFormat.SpaceBefore = Unit.FromPoint(10);
            sectionTitleStyle.ParagraphFormat.Font.Size = 12;
            sectionTitleStyle.ParagraphFormat.Font.Bold = true;
            sectionTitleStyle.ParagraphFormat.Font.Name = "Avenir LT Std 55 Roman";
            font = new XFont("Avenir LT Std 55 Roman", 12, XFontStyle.Bold, options);

            var tableCellStyle = document.AddStyle("tableCell", "Normal");
            tableCellStyle.ParagraphFormat.LeftIndent = 2;
            tableCellStyle.ParagraphFormat.Font.Size = 7;
            tableCellStyle.ParagraphFormat.Font.Bold = false;
            font = new XFont("Avenir Next Condensed Demi Bold", 7, XFontStyle.Regular, options);

            var blueCellStyle = document.AddStyle("blueCell", "tableCell");
            blueCellStyle.ParagraphFormat.Shading.Color = primary;
            blueCellStyle.ParagraphFormat.Font.Color = white;

            var centerBlueCellStyle = document.AddStyle("centerBlueCell", "blueCell");
            centerBlueCellStyle.ParagraphFormat.LeftIndent = 0;
            centerBlueCellStyle.ParagraphFormat.Alignment = ParagraphAlignment.Center;

            var whiteCellStyle = document.AddStyle("whiteCell", "tableCell");
            whiteCellStyle.ParagraphFormat.Shading.Color = white;
            whiteCellStyle.ParagraphFormat.Font.Color = black;

            var centerWhiteStyle = document.AddStyle("centerWhiteCell", "whiteCell");
            centerWhiteStyle.ParagraphFormat.LeftIndent = 0;
            centerWhiteStyle.ParagraphFormat.Alignment = ParagraphAlignment.Center;
        }

        private int AddDetailRow(Table detailsTable, string label, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var detailRow = detailsTable.AddRow();

                detailRow.Cells[0].Shading.Color = primary;
                var par = detailRow.Cells[0].AddNonNullParagraph(label);
                par.Style = "blueCell";

                detailRow.Cells[1].Shading.Color = white;
                par = detailRow.Cells[1].AddNonNullParagraph(value);
                par.Style = "whiteCell";

                return 1;
            }

            return 0;
        }

        private Table CreateBorderedTable()
        {
            var table = new Table();
            table.Rows.Height = 12;
            table.Rows.LeftIndent = 0;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Font.Size = 7;
            table.LeftPadding = 0;
            table.RightPadding = 0;
            table.Borders.Color = grey;
            return table;
        }

        public List<Item> OrderBySize(IEnumerable<Item> items)
        {
            var collection = collectionRepo.Get(x => x.Name == "Sizes");
            var colValues = collection.Values.OrderBy(x => x.CollectionIndex);

            var sizes = colValues.Select(x => x.Value).ToList();
            return items.OrderBy(x => sizes.IndexOf(x.Size)).ToList();
        }

        public List<string> OrderBySize(List<string> items)
        {
            var collection = collectionRepo.Get(x => x.Name == "Sizes");
            var colValues = collection.Values.OrderBy(x => x.CollectionIndex);

            var sizes = colValues.Select(x => x.Value).ToList();
            return items.OrderBy(x => sizes.IndexOf(x)).ToList();
        }
    }

    public static class MigradocExtensions
    {
        public static Paragraph AddNonNullParagraph(this Cell cell, string paragraph)
        {
            return cell.AddParagraph(paragraph == null ? "" : paragraph);
        }

        public static Paragraph AddNAParagraph(this Cell cell, string paragraph)
        {
            return cell.AddParagraph(string.IsNullOrWhiteSpace(paragraph) ? "N/A" : paragraph);
        }
    }
}