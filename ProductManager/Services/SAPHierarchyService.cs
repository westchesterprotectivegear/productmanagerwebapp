﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using SAP.Entities;

namespace ProductManager.Services
{
    public interface ISAPHierarchyService
    {
        Hierarchy GetHierarchy(string id);
        bool HasChildren(string id);
    }

    public class SAPHierarchyService : ISAPHierarchyService
    {
        [Inject]
        public IRepository<Hierarchy, SAPDatabaseContext> hierarchyRepo { get; set; }

        public Hierarchy GetHierarchy(string id)
        {
            return hierarchyRepo.Get(id);
        }

        public bool HasChildren(string id)
        {
            var hiers = hierarchyRepo.GetAll();

            foreach (var hier in hiers)
            {
                if (hier.ParentID == id)
                    return true;
            }

            return false;
        }
    }
}