﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using Ghostscript.NET.Rasterizer;
using ImageMagick;

namespace ProductManager.Services
{
    public interface IImageService
    {
        Image ScaleImage(Image image, int toWidth, int toHeight);
        Image PadWhite(Image image);
        Image ImposeTextOnImageBottom(Image image, string text);
        Image GetImageFromClipboard();
        Image DrawBorder(Image image);
        Image[] GetImagesFromPDF(Stream pdfStream);
    }

    public class ImageService : IImageService
    {

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool OpenClipboard(IntPtr hWndNewOwner);

        [DllImport("user32.dll")]
        static extern IntPtr GetClipboardData(uint uFormat);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool CloseClipboard();

        [DllImport("user32.dll")]
        static extern bool EmptyClipboard();

        [DllImport("gdi32.dll")]
        static extern IntPtr CopyEnhMetaFile(IntPtr hemfSrc, string lpszFile);

        [DllImport("gdi32.dll")]
        static extern bool DeleteEnhMetaFile(IntPtr hemf);

        public Image ScaleImage(Image image, int toWidth, int toHeight)
        {
            using (MagickImage mImage = new MagickImage((Bitmap)image))
            {
                mImage.Density = new Density(300);
                mImage.Scale(toWidth, toHeight);
                return mImage.ToBitmap();
            }
        }

        public Image PadWhite(Image image)
        {
            int largestDimension = Math.Max(image.Height, image.Width);
            using (MagickImage mImage = new MagickImage((Bitmap)image))
            {
                mImage.Extent(largestDimension, largestDimension, Gravity.Center, MagickColor.FromRgb(255, 255, 255));
                return mImage.ToBitmap();
            }
        }

        public Image ImposeTextOnImageBottom(Image image, string text)
        {
            Bitmap bitmap = new Bitmap(image.Width, image.Height + 500);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.DrawImage(image, 0, 0);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            SolidBrush brush = new SolidBrush(Color.Black);
            Font font = new System.Drawing.Font("Arial Bold", 11, FontStyle.Underline);

            int height = (int)graphics.MeasureString(text, font, bitmap.Width).Height;

            DrawCenteredString(text, graphics, new RectangleF(0, image.Height, bitmap.Width, height + 10), font);
            return cropImage(bitmap, new Rectangle(0, 0, bitmap.Width, image.Height + height + 10));
        }

        public Image DrawBorder(Image image)
        {
            Bitmap bitmap = new Bitmap(image.Width + 2, image.Height + 2);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.DrawImage(image, 1, 1);
            graphics.DrawRectangle(new Pen(Brushes.LightGray, 1), new Rectangle(0, 0, image.Width, image.Height));
            return (Image)bitmap;
        }

        private void DrawCenteredString(string text, Graphics graphics, RectangleF rect, Font font)
        {
            SolidBrush brush = new SolidBrush(Color.Black);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            rect.Y += (rect.Height / 2) - (graphics.MeasureString(text, font, (int)rect.Width).Height / 2);

            graphics.DrawString(text, font, brush, rect, format);
        }

        private static Image cropImage(Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);
            return (Image)(bmpCrop);
        }

        public Image GetImageFromClipboard()
        {
            Image image = null;

            try
            {
                OpenClipboard(IntPtr.Zero);

                IntPtr pointer = GetClipboardData(14);

                string fileName = @"\\wcmktg.com\WCPG-HQ\Public\edi\exe\PMServiceHost\VendorSpecs\Temp\" + Guid.NewGuid().ToString() + ".emf";

                IntPtr handle = CopyEnhMetaFile(pointer, fileName);

                using (Metafile metafile = new Metafile(fileName))
                {
                    image = new Bitmap(metafile.Width, metafile.Height);
                    Graphics g = Graphics.FromImage(image);

                    EmptyClipboard();
                    CloseClipboard();

                    g.DrawImage(metafile, 0, 0, image.Width, image.Height);
                }

                DeleteEnhMetaFile(handle);
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                int line = (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber();
            }

            return image;
        }

        public Image[] GetImagesFromPDF(Stream pdfStream)
        {
            MagickReadSettings settings = new MagickReadSettings();
            // Settings the density to 300 dpi will create an image with a better quality
            settings.Density = new Density(300, 300);

            using (MagickImageCollection images = new MagickImageCollection())
            {
                // Add all the pages of the pdf file to the collection
                images.Read(pdfStream, settings);
                
                var returnImages = new Image[images.Count];
                foreach (MagickImage image in images)
                {
                    returnImages[0] = image.ToBitmap();
                }

                return returnImages;
            }
        }
    }
}
