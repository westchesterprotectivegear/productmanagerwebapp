﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Ninject;
using ProductManager.Entities;
using SAP.Entities;

namespace ProductManager.Services
{
    public interface IOptionsService
    {
        List<string> GetBrands();
        List<string> GetCountriesOfOrigin();
        List<string> GetSizes();
        List<string> GetUnitsOfMeasure();
        List<string> GetGlovePatterns();
        List<string> GetQualityLevels();
        List<string> GetHangTags();
        List<string> GetViolators();
        List<string> GetCuffTypess();
        List<string> GetGrades();
        List<string> GetWarningLabels();
        List<string> GetRNNumbers();
        List<string> GetDivisions();
        List<string> GetProductLines();
        List<string> GetProp65Chemicals();
        List<string> GetThumbTypes();
        List<string> GetTagAssemblies();
        List<string> GetDisplayTypes();
        Dictionary<string, string> GetSAPHierarchyOptions(string root);
        List<MaterialGroup> GetSAPGroups();
        List<string> GetPriceListSKUs();
    }

    public class OptionsService : IOptionsService
    {
        [Inject]
        public IRepository<Collection, ProductManagerDatabaseContext> collectionRepo { get; set; }
        [Inject]
        public IRepository<Hierarchy, SAPDatabaseContext> hierarchyRepo { get; set; }
        [Inject]
        public IRepository<MaterialGroup, SAPDatabaseContext> matGroupRepo { get; set; }
        [Inject]
        public IRepository<PriceListData, ProductManagerDatabaseContext> priceListRepo { get; set; }
        [Inject]
        public IRepository<Prop65Chemical, ProductManagerDatabaseContext> chemRepo { get; set; }

        public List<MaterialGroup> GetSAPGroups()
        {
            return matGroupRepo.GetAll().ToList();
        }

        public List<string> GetBrands()
        {
            DataSet dataSet = new DataSet();
            var conn = new SqlConnection("Data Source=Jed;Initial Catalog=PRD;User ID=spongebob;Password=benjamin");
            SqlDataAdapter adapter = new SqlDataAdapter("SELECT BEZEI From prd.TVM2T WHERE (MANDT = '100') AND (SPRAS = 'E')", conn);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            adapter.Fill(dataSet);
            conn.Close();

            List<string> brands = new List<string>();
            foreach (DataRow row in dataSet.Tables[0].Rows)
                brands.Add(row[0].ToString());

            brands.Sort();
            brands.Insert(0, "");
            return brands;
        }

        public List<string> GetSizes()
        {
            return GetCollectionValues("Sizes");
        }

        public List<string> GetUnitsOfMeasure()
        {
            return GetCollectionValues("UnitOfMeasure");
        }

        public List<string> GetGlovePatterns()
        {
            return GetCollectionValues("Patterns");
        }

        public List<string> GetQualityLevels()
        {
            return GetCollectionValues("QualityLevels");
        }

        public List<string> GetHangTags()
        {
            return GetCollectionValues("HangTags");
        }

        public List<string> GetViolators()
        {
            return GetCollectionValues("Violators");
        }

        public List<string> GetCuffTypess()
        {
            return GetCollectionValues("CuffTypes");
        }

        public List<string> GetGrades()
        {
            return GetCollectionValues("Grades");
        }

        public List<string> GetWarningLabels()
        {
            return GetCollectionValues("WarningLabels");
        }

        public List<string> GetRNNumbers()
        {
            return GetCollectionValues("RNNumbers");
        }

        public List<string> GetDivisions()
        {
            return GetCollectionValues("Divisions");
        }

        public List<string> GetProductLines()
        {
            return GetCollectionValues("ProductLines");
        }

        public List<string> GetThumbTypes()
        {
            return GetCollectionValues("ThumbTypes");
        }

        public List<string> GetTagAssemblies()
        {
            return GetCollectionValues("TagAssemblies");
        }

        public List<string> GetDisplayTypes()
        {
            return GetCollectionValues("DisplayTypes");
        }

        public List<string> GetCountriesOfOrigin()
        {
            return GetCollectionValues("CountriesOfOrigin");
        }

        public List<string> GetProp65Chemicals()
        {
            return chemRepo.GetAll().Select(x => x.Name).ToList();
        }

        public List<string> GetCollectionValues(string collectionName)
        {
            var collection = collectionRepo.Get(x => x.Name == collectionName);
            var colValues = collection.Values.OrderBy(x => x.CollectionIndex);
            return colValues.Select(x => x.Value).ToList();
        }

        public Dictionary<string, string> GetSAPHierarchyOptions(string root)
        {
            var hiers = hierarchyRepo.GetAll();
            var returnHiers = new Dictionary<string, string>();
            foreach (var hier in hiers)
            {
                if (hier.ParentID == root)
                    returnHiers.Add(hier.ID, hier.Text);
            }
            return returnHiers;
        }

        public List<string> GetPriceListSKUs()
        {
            return priceListRepo.GetAll().Select(x => x.SKU).ToList();
        }
    }
}