﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ProductManager.Services
{
    public class WCPGEmailService : SmtpClient
    {
        // Gmail user-name
        public string UserName { get; set; }

        public WCPGEmailService() : base(ConfigurationManager.AppSettings["EmailHost"], Int32.Parse(ConfigurationManager.AppSettings["EmailPort"]))
        {
            //Get values from web.config file:
            this.UserName = ConfigurationManager.AppSettings["EmailUserName"];
            this.UseDefaultCredentials = false;
            this.Credentials = new System.Net.NetworkCredential(this.UserName, ConfigurationManager.AppSettings["EmailPassword"]);
        }
    }
}