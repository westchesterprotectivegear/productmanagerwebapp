﻿using System;
using System.IO;
using System.Linq;
using ProductManager.Web.Entities;
using Ninject;
using ProductManager.Entities;
using System.Net;
using System.Threading;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using CookComputing.XmlRpc;
using ProductManager.Services;
using System.Drawing;

namespace ProductManager
{
    public interface DrupalWebService : IXmlRpcProxy
    {
        [XmlRpcMethod("node.retrieve")]
        XmlRpcStruct NodeLoad(int nid, string[] field);
        [XmlRpcMethod("node.create")]
        XmlRpcStruct NodeSave(XmlRpcStruct node);
        [XmlRpcMethod("node.update")]
        XmlRpcStruct NodeUpdate(XmlRpcStruct node);
        [XmlRpcMethod("user.login")]
        XmlRpcStruct Login(string username, string password);
    }

    [XmlRpcUrl("http://www.westchesterprotects.com/pm/")]
    public interface IIndustrialServices : DrupalWebService { }

    [XmlRpcUrl("http://www.westchesterfits.com/pm/")]
    public interface IRetailServices : DrupalWebService { }

    [XmlRpcUrl("http://www.ironcatgear.com/pm/")]
    public interface IIroncatServices : DrupalWebService { }

    public interface IWebsiteUpdateService<TContext> where TContext : DatabaseContext
    {
        void UpdateProduct(string partNumber, bool connectionEst);
        void UpdateProducts(bool all);
    }

    public class WebsiteUpdateService<TContext> : IWebsiteUpdateService<TContext> where TContext : DatabaseContext
    {
        [Inject]
        public IUnitOfWork<TContext> UnitOfWork { get; set; }
        [Inject]
        public IRepository<Web.Entities.Node, TContext> nodeRepo { get; set; }
        [Inject]
        public IRepository<ProductNode, TContext> productNodeRepo { get; set; }
        [Inject]
        public IRepository<NodeRevision, TContext> nodeRevisionRepo { get; set; }
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<Field, TContext> fieldRepo { get; set; }
        [Inject]
        public IRepository<ProductImage, TContext> ProductImageRepo { get; set; }
        [Inject]
        public IRepository<FileManaged, TContext> fileManagedRepo { get; set; }
        [Inject]
        public IRepository<FileUsage, TContext> fileUsageRepo { get; set; }
        [Inject]
        public IRepository<NodeCommentStatistics, TContext> statsRepo { get; set; }
        [Inject]
        public IRepository<URLAlias, TContext> aliasRepo { get; set; }
        [Inject]
        public IImageService imageService { get; set; }
        [Inject]
        public IHistoryService historyService { get; set; }

        private const string specLocation = @"\\fred\PMFiles\CustomerSpecs\";
        private const string imageLocation = @"\\fred\Images\ProductImages\";

        private Image image { get; set; }
        private int nid { get; set; }
        private int vid { get; set; }
        private int fid { get; set; }
        private int epoch { get; set; }
        private ProductNode productNode { get; set; }
        private Product product { get; set; }
        DrupalWebService drupalWebService { get; set; }

        public void UpdateProducts(bool all)
        {
            var products = new List<Product>();
            if (all)
                products = productRepo.GetGroup(x => x.TestProduct == false && x.PrivateLabel == false).ToList();
            else
                // if its outdated industrial, should be outdated all
                products = productRepo.GetGroup(x => x.OutdatedIndustrial && x.TestProduct == false && x.PrivateLabel == false).ToList();

            string connectionString = "";
            string solrURI = "";
            string cron = "";

            switch (typeof(TContext).ToString())
            {
                case "ProductManager.IndustrialWebsiteDatabaseContext":
                    connectionString = "Server=192.168.109.4; Port=3306; Database=westchesterprotects_drupal; User ID=wcdatacon; Password=prdmgrUP;";
                    solrURI = "http://westchesterprotects.com:8080/solr/westchesterprotects/update";
                    cron = "http://westchesterprotects.com/cron.php?cron_key=Nyqd9hTkBOQasRhu5WueDlRoa6w5BfuOJbVdgnN_-B0";
                    products = products.Where(x => x.Facets.Any(y => y.FacetValue.Site == "Industrial")).ToList();
                    drupalWebService = XmlRpcProxyGen.Create<IIndustrialServices>();
                    break;
                case "ProductManager.RetailWebsiteDatabaseContext":
                    connectionString = "Server=192.168.109.4; Port=3306; Database=westchesterfits_drupal; User ID=wcdatacon; Password=prdmgrUP;";
                    solrURI = "http://westchesterfits.com:8080/solr/westchesterfits/update";
                    cron = "http://westchesterfits.com/cron.php?cron_key=Nyqd9hTkBOQasRhu5WueDlRoa6w5BfuOJbVdgnN_-B0";
                    products = products.Where(x => x.Facets.Any(y => y.FacetValue.Site == "Retail")).ToList();
                    drupalWebService = XmlRpcProxyGen.Create<IRetailServices>();
                    break;
                case "ProductManager.IroncatWebsiteDatabaseContext":
                    connectionString = "Server=192.168.109.4; Port=3306; Database=ironcat_drupal; User ID=wcdatacon; Password=prdmgrUP;";
                    solrURI = "http://ironcatgear.com:8080/solr/ironcat/update";
                    cron = "http://ironcatgear.com/cron.php?cron_key=Nyqd9hTkBOQasRhu5WueDlRoa6w5BfuOJbVdgnN_-B0";
                    products = products.Where(x => x.Facets.Any(y => y.FacetValue.Site == "Ironcat")).ToList();
                    drupalWebService = XmlRpcProxyGen.Create<IIroncatServices>();
                    break;
            }
            var user = drupalWebService.Login("nmoore", "myw0rdpass");

            drupalWebService.Headers.Add("Cookie", user["session_name"].ToString() + "=" + user["sessid"].ToString());
            drupalWebService.Headers.Add("X-CSRF-Token", user["token"].ToString());
            drupalWebService.KeepAlive = false;

            var productBases = from x in products select x.BasePartNumber;
            var invalidNodes = productNodeRepo.GetGroup(x => x.Type == "glove" && !productBases.Contains(x.Title)).ToList();

            UnitOfWork.BeginTransaction();
            foreach (var invalidNode in invalidNodes)
            {
                invalidNode.Status = 0;
                nodeRepo.Update(invalidNode);

                foreach (var rev in invalidNode.Revisions)
                {
                    rev.Status = 0;
                    nodeRevisionRepo.Update(rev);
                }
            }
            UnitOfWork.Commit();

            foreach (var product in products)
            {
                System.Diagnostics.Debug.WriteLine(products.IndexOf(product) + " " + product.BasePartNumber);
                try
                {
                    UpdateProduct(product.BasePartNumber, true);
                }
                catch (Exception ex)
                {
                    string filePath = @"C:\Test\Error.txt";

                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        writer.WriteLine(product.BasePartNumber);
                        writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                           "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                    }
                }
            }

            // clear the search indices
            SendPost(solrURI, "stream.body=<delete><query>*:*</query></delete>");
            SendPost(solrURI, "stream.body=<commit />");
            SendPost(solrURI, "stream.body=<optimize />");

            // clear cache
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                MySqlCommand command = new MySqlCommand("ClearCache", connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection.Open();
                command.ExecuteNonQuery();
            }

            // run cron
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(cron);
            request.KeepAlive = true;
            request.ProtocolVersion = HttpVersion.Version10;
            request.Method = "POST";
            request.Timeout = Timeout.Infinite;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                Console.WriteLine(streamReader.ReadToEnd());
            }
        }

        public void UpdateProduct(string partNumber, bool connectionEst)
        {
            UnitOfWork.BeginTransaction();

            product = productRepo.Get(x => x.BasePartNumber == partNumber);
            epoch = GetSecondsSinceEpoch();
            nid = 0;
            
            productNode = productNodeRepo.Get(x => x.Type == "glove" && x.Title == partNumber);

            if (!connectionEst)
            {
                // create new node with drupal api
                switch (typeof(TContext).ToString())
                {
                    case "ProductManager.IndustrialWebsiteDatabaseContext":
                        drupalWebService = XmlRpcProxyGen.Create<IIndustrialServices>();
                        break;
                    case "ProductManager.RetailWebsiteDatabaseContext":
                        drupalWebService = XmlRpcProxyGen.Create<IRetailServices>();
                        break;
                    case "ProductManager.IroncatWebsiteDatabaseContext":
                        drupalWebService = XmlRpcProxyGen.Create<IIroncatServices>();
                        break;
                }
                var user = drupalWebService.Login("nmoore", "myw0rdpass");

                drupalWebService.Headers.Add("Cookie", user["session_name"].ToString() + "=" + user["sessid"].ToString());
                drupalWebService.Headers.Add("X-CSRF-Token", user["token"].ToString());
                drupalWebService.KeepAlive = false;
            }

            if (productNode != null)
            {
                nid = productNode.NodeID;
            }
            else
            {
                XmlRpcStruct node = new XmlRpcStruct();

                node["title"] = product.BasePartNumber;
                node["body"] = "";
                node["format"] = 1;
                node["type"] = "glove";
                node["promote"] = false;

                var result = drupalWebService.NodeSave(node);
                nid = Convert.ToInt32(result["nid"].ToString());

                // load the product node after creation
                productNode = productNodeRepo.Get(x => x.Type == "glove" && x.Title == partNumber);
            }

            vid = nodeRevisionRepo.Get(x => x.NodeID == nid).VersionID;

            AddFacets();
            AddSkus();
            AddFeatures();
            AddProductData();
            AddPhoto();
            AddSpecSheet();
            AddCrossReferences();

            // make sure the node status is correct
            switch (typeof(TContext).ToString())
            {
                case "ProductManager.IndustrialWebsiteDatabaseContext":
                    productNode.Status = product.ActiveIndustrial ? 1 : 0;
                    break;
                case "ProductManager.RetailWebsiteDatabaseContext":
                    productNode.Status = product.ActiveRetail ? 1 : 0;
                    break;
                case "ProductManager.IroncatWebsiteDatabaseContext":
                    productNode.Status = product.ActiveIroncat ? 1 : 0;
                    break;
            }
            productNodeRepo.Update(productNode);
            foreach (var rev in productNode.Revisions)
            {
                rev.Status = productNode.Status;
                nodeRevisionRepo.Update(rev);
            }

            UnitOfWork.Commit();
        }

        private void AddSpecSheet()
        {
            string site = "";
            switch (typeof(TContext).ToString())
            {
                case "ProductManager.IndustrialWebsiteDatabaseContext":
                    site = "westchesterprotects.com";
                    break;
                case "ProductManager.RetailWebsiteDatabaseContext":
                    site = "westchesterfits.com";
                    break;
                case "ProductManager.IroncatWebsiteDatabaseContext":
                    site = "ironcatgear.com";
                    break;
            }

            string specLoc = @"\\fred\PMFiles\CustomerSpecs\";
            FileInfo spec = new FileInfo(specLoc + product.BasePartNumber.Replace('/', '~') + ".pdf");
            if (File.Exists(spec.FullName))
            {
                spec.CopyTo(@"\\192.168.109.4\WebsiteShare\" + site + @"\sites\default\files\data_sheets\" + spec.Name, true);

                if (productNode.SpecSheets.Count == 0)
                {
                    fieldRepo.Create(new SpecSheet()
                    {
                        EntityID = nid,
                        RevisionID = vid,
                        Value = spec.Name
                    });
                }
            }
        }

        private void AddPhoto()
        {
            string site = "";
            switch (typeof(TContext).ToString())
            {
                case "ProductManager.IndustrialWebsiteDatabaseContext":
                    site = "westchesterprotects.com";
                    break;
                case "ProductManager.RetailWebsiteDatabaseContext":
                    site = "westchesterfits.com";
                    break;
                case "ProductManager.IroncatWebsiteDatabaseContext":
                    site = "ironcatgear.com";
                    break;
            }

            // add photo
            image = imageService.ScaleImage(System.Drawing.Image.FromFile(imageLocation + product.PrimaryPhoto.Name), 500, 500);
            int imageSize = Convert.ToInt32(new FileInfo(imageLocation + product.PrimaryPhoto.Name).Length);
            string imageName = product.BasePartNumber.Replace('/', '~') + ".png";
            image.Save(@"\\192.168.109.4\WebsiteShare\" + site + @"\sites\default\files\product_images\" + imageName, System.Drawing.Imaging.ImageFormat.Png);

            var fileManaged = fileManagedRepo.Get(x => x.FileName == imageName);
            
            if (fileManaged != null)
            {
                fid = fileManaged.FileID;
                fileManaged.FileSize = imageSize;
                fileManaged.Timestamp = epoch;
                fileManagedRepo.Update(fileManaged);
            }
            else
            {
                fid = (int)fileManagedRepo.Create(new FileManaged()
                {
                    FileName = imageName,
                    FileSize = imageSize,
                    URI = "public://product_images/" + imageName,
                    Timestamp = epoch
                });
            }

            if (productNode.FileUsages.Count > 0)
            {
                var usage = productNode.FileUsages[0];
                usage.FileID = fid;
                fileUsageRepo.Update(usage);
            }
            else
            {
                fileUsageRepo.Create(new FileUsage()
                {
                    FileID = fid,
                    ID = nid
                });
            }

            if (productNode.ProductImages.Count > 0)
            {
                var productImage = productNode.ProductImages[0];
                productImage.Height = image.Height;
                productImage.Width = image.Width;
                fieldRepo.Update(productImage);
            }
            else
            {
                fieldRepo.Create(new ProductImage()
                {
                    EntityID = nid,
                    RevisionID = vid,
                    Title = product.BasePartNumber,
                    FileID = fid,
                    Alt = product.BasePartNumber,
                    Height = image.Height,
                    Width = image.Width
                });
            }
        }

        private void AddCrossReferences()
        {
            // create cross references
            for (int i = 0; i < product.CrossReferences.Count; i++)
            {
                var xref = product.CrossReferences[i];
                
                if (i < productNode.CrossReferences.Count)
                {
                    var xrefNode = productNode.CrossReferences[i];
                    
                    // brand
                    if (xrefNode.CompetitorBrands.Count > 0)
                    {
                        var brand = xrefNode.CompetitorBrands[0];
                        brand.Value = xref.Company;
                        fieldRepo.Update(brand);
                    }
                    else
                    {
                        fieldRepo.Create(new CompetitorBrand()
                        {
                            Value = xref.Company,
                            EntityID = xrefNode.NodeID,
                            RevisionID = xrefNode.VersionID,
                            Delta = i
                        });
                    }

                    // competitor part
                    if (xrefNode.CompetitorParts.Count > 0)
                    {
                        var part = xrefNode.CompetitorParts[0];
                        part.Value = xref.CrossReferenceNumber;
                        fieldRepo.Update(part);
                    }
                    else
                    {
                        fieldRepo.Create(new CompetitorPart()
                        {
                            Value = xref.CrossReferenceNumber,
                            EntityID = xrefNode.NodeID,
                            RevisionID = xrefNode.VersionID,
                            Delta = i
                        });
                    }

                    // west chester part
                    if (xrefNode.WestChesterParts.Count > 0)
                    {
                        var part = xrefNode.WestChesterParts[0];
                        part.Value = product.BasePartNumber;
                        fieldRepo.Update(part);
                    }
                    else
                    {
                        fieldRepo.Create(new WestChesterPart()
                        {
                            Value = product.BasePartNumber,
                            EntityID = xrefNode.NodeID,
                            RevisionID = xrefNode.VersionID,
                            Delta = i
                        });
                    }

                    // image
                    if (xrefNode.Images.Count > 0)
                    {
                        var image = xrefNode.Images[0];
                        image.Height = image.Height;
                        image.Width = image.Width;
                        fieldRepo.Update(image);
                    }
                    else
                    {
                        fieldRepo.Create(new CrossReferenceImage()
                        {
                            EntityID = xrefNode.NodeID,
                            RevisionID = xrefNode.VersionID,
                            Title = product.BasePartNumber,
                            FileID = fid,
                            Alt = product.BasePartNumber,
                            Height = image.Height,
                            Width = image.Width
                        });
                    }
                }
                else
                {
                    XmlRpcStruct xnode = new XmlRpcStruct();

                    xnode["title"] = product.BasePartNumber;
                    xnode["body"] = "";
                    xnode["format"] = 1;
                    xnode["type"] = "product_comparison";
                    xnode["promote"] = false;
                    
                    var xresult = drupalWebService.NodeSave(xnode);
                    int xrefnid = Convert.ToInt32(xresult["nid"].ToString());
                    int xrefvid = (int)nodeRevisionRepo.Get(x => x.NodeID == nid).VersionID;

                    fieldRepo.Create(new CompetitorBrand()
                    {
                        Value = xref.Company,
                        EntityID = xrefnid,
                        RevisionID = xrefvid,
                        Delta = i
                    });

                    fieldRepo.Create(new CompetitorPart()
                    {
                        Value = xref.CrossReferenceNumber,
                        EntityID = xrefnid,
                        RevisionID = xrefvid,
                        Delta = i
                    });

                    fieldRepo.Create(new WestChesterPart()
                    {
                        Value = product.BasePartNumber,
                        EntityID = xrefnid,
                        RevisionID = xrefvid,
                        Delta = i
                    });

                    fieldRepo.Create(new CrossReferenceImage()
                    {
                        EntityID = xrefnid,
                        RevisionID = xrefvid,
                        Title = product.BasePartNumber,
                        FileID = fid,
                        Alt = product.BasePartNumber,
                        Height = image.Height,
                        Width = image.Width
                    });
                }
            }

            // delete any extras
            for (int i = 0; i < productNode.CrossReferences.Count - product.CrossReferences.Count; i++)
            {
                var node = productNode.CrossReferences[productNode.CrossReferences.Count - (i + 1)];
                nodeRepo.Delete(node);
            }
        }

        private void AddProductData()
        {
            EditProductDataCollection<Name>(product.Name, productNode.Names);
            EditProductDataCollection<Copy>(product.MarketingCopy, productNode.Copies);
            EditProductDataCollection<Body>(product.MarketingCopy, productNode.Bodies);

            if (productNode.CommentStatistics.Count == 0)
            {
                statsRepo.Create(new NodeCommentStatistics()
                {
                    NodeID = nid,
                    LastCommentDate = epoch
                });
            }

            var alias = aliasRepo.Get(x => x.Alias == "product/" + productNode.Title);
            if (alias != null)
            {
                alias.Source = "node/" + productNode.NodeID;
                aliasRepo.Update(alias);
            }
            else
            {
                aliasRepo.Create(new URLAlias()
                {
                    Alias = "product/" + productNode.Title,
                    Source = "node/" + productNode.NodeID
                });
            }
        }

        private void EditProductDataCollection<T>(string value, IList<T> fields) where T : ValueField
        {
            // modify first if there is one, otherwise create new
            if (fields.Count > 0)
            {
                var field = fields[0];
                field.Value = value;
                field.EntityID = nid;
                field.RevisionID = vid;
                fieldRepo.Update(field);
            }
            else
            {
                var field = Activator.CreateInstance<T>();
                field.Value = value;
                field.EntityID = nid;
                field.RevisionID = vid;
                fieldRepo.Create(field);
            }

            // delete all but the first
            for (int i = 0; i < fields.Count; i++)
            {
                if (i > 0)
                {
                    var field = fields[i];
                    fieldRepo.Delete(field);
                }
            }
        }

        private void AddFeatures()
        {
            // create features
            for (int i = 0; i < product.Features.Count; i++)
            {
                var feature = product.Features[i];
                if (i < productNode.Features.Count)
                {
                    var field = productNode.Features[i];
                    field.Value = feature.Value;
                    field.EntityID = nid;
                    field.RevisionID = vid;
                    field.Delta = i;
                    fieldRepo.Update(field);
                }
                else
                {
                    var field = new Web.Entities.Feature();
                    field.Value = feature.Value;
                    field.EntityID = nid;
                    field.RevisionID = vid;
                    field.Delta = i;
                    fieldRepo.Create(field);
                }
            }

            // delete any extras
            for (int i = 0; i < productNode.Features.Count - product.Features.Count; i++)
            {
                var field = productNode.Features[productNode.Features.Count - (i + 1)];
                fieldRepo.Delete(field);
            }
        }

        private void AddSkus()
        {
            try
            {
                // create skus
                var activeSkus = product.Items.Where(x => x.Active)
                    .SelectMany(x => x.PackSizes).ToList();

                for (int i = 0; i < activeSkus.Count; i++)
                {
                    var packSize = activeSkus[i];
                    if (i < productNode.SKUs.Count)
                    {
                        var field = productNode.SKUs[i];
                        field.Value = packSize.SKU;
                        field.EntityID = nid;
                        field.RevisionID = vid;
                        field.Delta = i;
                        fieldRepo.Update(field);
                    }
                    else
                    {
                        var field = new SKU();
                        field.Value = packSize.SKU;
                        field.EntityID = nid;
                        field.RevisionID = vid;
                        field.Delta = i;
                        fieldRepo.Create(field);
                    }
                }

                // delete any extras
                for (int i = 0; i < productNode.SKUs.Count - activeSkus.Count; i++)
                {
                    var field = productNode.SKUs[productNode.SKUs.Count - (i + 1)];
                    fieldRepo.Delete(field);
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private void AddFacets()
        {
            // group facets by category and loop through groups
            List<List<Facet>> facetGroups = new List<List<Facet>>();
            switch (typeof(TContext).ToString())
            {
                case "ProductManager.IndustrialWebsiteDatabaseContext":
                    facetGroups = product.Facets.Where(x => x.FacetValue.Site == "Industrial").GroupBy(x => x.FacetValue.Category).Select(group => group.ToList()).ToList();
                    break;
                case "ProductManager.RetailWebsiteDatabaseContext":
                    facetGroups = product.Facets.Where(x => x.FacetValue.Site == "Retail").GroupBy(x => x.FacetValue.Category).Select(group => group.ToList()).ToList();
                    break;
                case "ProductManager.IroncatWebsiteDatabaseContext":
                    facetGroups = product.Facets.Where(x => x.FacetValue.Site == "Ironcat").GroupBy(x => x.FacetValue.Category).Select(group => group.ToList()).ToList();
                    break;
            }

            foreach (var group in facetGroups)
            {
                switch (group[0].FacetValue.Category)
                {
                    case "Application":
                        EditFacets<Application>(group, productNode.Applications);
                        break;
                    case "Audience":
                        EditFacets<Audience>(group, productNode.Audiences);
                        break;
                    case "Brand":
                        EditFacets<Brand>(group, productNode.Brands);
                        break;
                    case "Item Type":
                        EditFacets<ItemType>(group, productNode.ItemTypes);
                        break;
                    case "Item Style":
                        EditFacets<ItemStyle>(group, productNode.ItemStyles);
                        break;
                    case "Material":
                        EditFacets<Material>(group, productNode.Materials);
                        break;
                    case "Pack Size":
                        EditFacets<Web.Entities.PackSize>(group, productNode.PackSizes);
                        break;
                    case "Protection Need":
                        EditFacets<ProtectionNeed>(group, productNode.ProtectionNeeds);
                        break;
                    case "Quality":
                        EditFacets<Quality>(group, productNode.Qualities);
                        break;
                    case "Size":
                        EditFacets<Web.Entities.Size>(group, productNode.Sizes);
                        break;
                    case "Type of Job":
                        EditFacets<TypeOfJob>(group, productNode.TypeOfJobs);
                        break;
                    case "Warning":
                        EditFacets<Warning>(group, productNode.Warnings);
                        break;
                }
            }
        }

        private void EditFacets<T>(List<Facet> facets, IList<T> fields) where T : ValueField
        {
            for (int i = 0; i < facets.Count; i++)
            {
                var facet = facets[i];
                if (i < fields.Count)
                {
                    var field = fields[i];
                    field.Value = facet.FacetValue.Value;
                    field.EntityID = nid;
                    field.RevisionID = vid;
                    field.Delta = i;
                    fieldRepo.Update(field);
                }
                else
                {
                    var field = Activator.CreateInstance<T>();
                    field.Value = facet.FacetValue.Value;
                    field.EntityID = nid;
                    field.RevisionID = vid;
                    field.Delta = i;
                    fieldRepo.Create(field);
                }
            }

            // delete any extras
            for (int i = 0; i < fields.Count - facets.Count; i++)
            {
                var field = fields[fields.Count - (i + 1)];
                fieldRepo.Delete(field);
            }
        }

        private int GetSecondsSinceEpoch()
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return (int)t.TotalSeconds;
        }

        private void SendPost(string uri, string parameters)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string HtmlResult = wc.UploadString(uri, parameters);
            }
        }
    }
}
