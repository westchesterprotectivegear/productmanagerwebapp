﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Ninject;
using OfficeOpenXml;
using ProductManager.Entities;

namespace ProductManager.Services
{
    public interface IMassEditService
    {
        byte[] CreateUploadTemplate(List<string> skus, List<string> fields);
        void UploadData(Stream data);
    }

    public class MassEditService : IMassEditService
    {
        [Inject]
        public IRepository<Item, ProductManagerDatabaseContext> itemRepo { get; set; }

        public byte[] CreateUploadTemplate(List<string> skus, List<string> fields)
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet sheet = package.Workbook.Worksheets.Add("SKU Data");

                // first column will always be sku, freeze it so it's always visible. color first row/column
                sheet.Cells[1, 1].Value = "SKU";
                sheet.View.FreezePanes(2, 2);
                var skuRange = sheet.Cells[2, 1, skus.Count + 1, 1];
                skuRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                skuRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(113, 177, 209));

                var fieldRange = sheet.Cells[1, 1, 1, fields.Count + 1];
                fieldRange.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                fieldRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(0, 114, 198));
                fieldRange.Style.Font.Color.SetColor(System.Drawing.Color.White);

                // fill rest of column headers from fields list
                for (int i = 0; i < fields.Count; i++)
                    sheet.Cells[1, i + 2].Value = fields[i];

                // fill skus
                for (int i = 0; i < skus.Count; i++)
                {
                    var product = itemRepo.Get(skus[i]);
                    //sheet.Cells[i + 2, 1].Value = product.SKU;
                }

                return package.GetAsByteArray();
            }
        }

        public void UploadData(Stream data)
        {
            using (ExcelPackage package = new ExcelPackage(data))
            {
                ExcelWorksheet sheet = package.Workbook.Worksheets[1];

                // get sheet bounds
                int rowCount = sheet.Dimension.End.Row;
                int colCount = sheet.Dimension.End.Column;

                // get list of properties
                var productProps = typeof(Product).GetWritableProperties();
                var itemProps = typeof(Item).GetWritableProperties();
                
                for (int row = 2; row < rowCount + 1; row++)
                {
                    var item = itemRepo.Get(sheet.Cells[row, 1].Value.ToString());

                    for (int col = 2; col < colCount + 1; col++)
                    {
                        string propName = sheet.Cells[1, col].Value.ToString();
                        string propValue = "";
                        if (sheet.Cells[row, col].Value != null)
                            propValue = sheet.Cells[row, col].Value.ToString();

                        if (!string.IsNullOrWhiteSpace(propValue))
                        {
                            if (itemProps.Contains(propName))
                                item.SetPropertyValue(propName, propValue);
                            else if (productProps.Contains(propName))
                                item.Product.SetPropertyValue(propName, propValue);
                        }
                    }

                    itemRepo.Update(item);
                }
            }
        }
    }

    public static class Extensions
    {
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }

        public static List<string> GetWritableProperties(this Type t)
        {
            return (from x in t.GetProperties()
                    where x.GetCustomAttributes(typeof(ReadOnlyAttribute), false).Count() == 0
                    select x.Name).ToList();
        }

        public static void SetPropertyValue(this object obj, string prop, object value)
        {
            var property = obj.GetType().GetProperty(prop);
            var convertValue = Convert.ChangeType(value, property.PropertyType);
            property.SetValue(obj, convertValue);
        }
    }
}