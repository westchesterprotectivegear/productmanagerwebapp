﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using MoreLinq;
using NHibernate;
using Ninject;
using ProductManager.Entities;

namespace ProductManager.Services
{
    public interface IHistoryService
    {
        List<HistoryItem> GetHistoryForProduct(string partNumber);
        void CreateProductHistory(Product product, Product originalProduct, ISession session, string user);
        void CreateSKUHistory(PackSize pack, PackSize originalPack, ISession session, string user);
        DateTime GetModifiedDate(string partNumber);
        object GetHistoryItem(string id);
        List<Product> GetModifiedProducts();
    }

    public class HistoryService : IHistoryService
    {
        [Inject]
        public IRepository<Product, ProductManagerDatabaseContext> productRepo { get; set; }
        [Inject]
        public IRepository<HistoryItem, ProductManagerDatabaseContext> historyRepo { get; set; }

        public List<HistoryItem> GetHistoryForProduct(string partNumber)
        {
            var product = productRepo.Get(x => x.BasePartNumber == partNumber);

            // get all history for style, then remove any that aren't related to sku
            var styleHistory = historyRepo.GetGroup(x => x.BasePartNumber == product.BasePartNumber).ToList();

            return styleHistory.OrderByDescending(x => x.Date).ToList();
        }

        public DateTime GetModifiedDate(string partNumber)
        {
            var history = GetHistoryForProduct(partNumber);
            if (history.Count > 0)
                return history.Max(x => x.Date).Date;
            else
            {
                var product = productRepo.Get(x => x.BasePartNumber == partNumber);
                return product.DateCreated;
            }
        }

        public object GetHistoryItem(string id)
        {
            return historyRepo.Get(x => x.ID == Guid.Parse(id));
        }

        public void CreateSKUHistory(PackSize pack, PackSize originalPack, ISession session, string user)
        {
            PropertyInfo[] newProps = pack.GetType().GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)).ToArray();
            PropertyInfo[] originalProps = originalPack.GetType().GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)).ToArray();

            foreach (var prop in newProps)
            {
                if (session.IsDirtyProperty(pack, prop.Name))
                {
                    var originalProp = originalProps.First(x => x.Name == prop.Name);
                    historyRepo.Create(new SKUHistoryItem()
                    {
                        BasePartNumber = pack.BasePartNumber,
                        SKU = pack.SKU,
                        NewValue = prop.GetValue(pack) == null ? "" : prop.GetValue(pack).ToString(),
                        OriginalValue = originalProp.GetValue(originalPack) == null ? "" : originalProp.GetValue(originalPack).ToString(),
                        Property = prop.Name,
                        UserName = user
                    });
                }
            }
        }

        public void CreateProductHistory(Product product, Product originalProduct, ISession session, string user)
        {
            PropertyInfo[] newProps = product.GetType().GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)).ToArray();
            PropertyInfo[] originalProps = originalProduct.GetType().GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)).ToArray();

            foreach (var prop in newProps)
            {
                if (session.IsDirtyProperty(product, prop.Name))
                {
                    var originalProp = originalProps.First(x => x.Name == prop.Name);
                    historyRepo.Create(new ProductHistoryItem()
                    {
                        BasePartNumber = product.BasePartNumber,
                        NewValue = prop.GetValue(product) == null ? "" : prop.GetValue(product).ToString(),
                        OriginalValue = originalProp.GetValue(originalProduct) == null ? "" : originalProp.GetValue(originalProduct).ToString(),
                        Property = prop.Name,
                        UserName = user
                    });
                }
            }
        }

        public List<Product> GetModifiedProducts()
        {
            var todaysChanges = historyRepo.GetGroup(x => x.Date.Date >= new DateTime(2017, 1, 30).Date);
            var baseParts = new List<Product>();

            foreach (var change in todaysChanges)
            {
                baseParts.Add(productRepo.Get(x => x.BasePartNumber == change.BasePartNumber));
            }

            return baseParts.DistinctBy(x => x.BasePartNumber).ToList();
        }
    }
    static class extentions
    {
        public static List<Variance> DetailedCompare<T>(this T val1, T val2)
        {
            List<Variance> variances = new List<Variance>();
            PropertyInfo[] fi = val1.GetType().GetProperties().Where(x => x.PropertyType.IsPrimitive || x.PropertyType == typeof(string)).ToArray();
            foreach (PropertyInfo f in fi)
            {
                Variance v = new Variance();
                Console.WriteLine(f.Name);
                v.Prop = f.Name;
                v.valA = f.GetValue(val1);
                v.valB = f.GetValue(val2);
                if ((v.valA != null && !v.valA.Equals(v.valB)) || v.valA == null && v.valB != null)
                    variances.Add(v);
            }
            return variances;
        }


    }
    class Variance
    {
        public string Prop { get; set; }
        public object valA { get; set; }
        public object valB { get; set; }
    }
}