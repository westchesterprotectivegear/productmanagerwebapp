[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(ProductManager.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(ProductManager.App_Start.NinjectWebCommon), "Stop")]

namespace ProductManager.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Extensions.Conventions;
    using AutoMapper;
    using Entities;
    using Models;
    using System.ComponentModel;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork<ProductManagerDatabaseContext>>().To<UnitOfWork<ProductManagerDatabaseContext>>().InRequestScope();
            kernel.Bind<IUnitOfWork<IroncatWebsiteDatabaseContext>>().To<UnitOfWork<IroncatWebsiteDatabaseContext>>().InRequestScope();
            kernel.Bind<IUnitOfWork<IndustrialWebsiteDatabaseContext>>().To<UnitOfWork<IndustrialWebsiteDatabaseContext>>().InRequestScope();
            kernel.Bind<IUnitOfWork<RetailWebsiteDatabaseContext>>().To<UnitOfWork<RetailWebsiteDatabaseContext>>().InRequestScope();
            kernel.Bind(x => x.FromAssembliesMatching(new string[] { "ProductManager.Entities.dll", "ProductManager.dll" }).SelectAllClasses().BindDefaultInterface());
            
            var config = new MapperConfiguration(cfg =>
            {
                // Map null strings to empty
                cfg.CreateMap<string, string>().ConvertUsing(s => s ?? string.Empty);

                cfg.CreateMap<Product, EditProductDetailsViewModel>().ReverseMap();
                cfg.CreateMap<PackSize, EditPackSizeDetailsViewModel>().ReverseMap();
                cfg.CreateMap<PackSize, EditPackSizePackagingViewModel>().ReverseMap();
                cfg.CreateMap<Measurement, EditItemDimensionViewModel>()
                    .ForMember(x => x.MeasurementID, opt => opt.MapFrom(p => p.ID))
                    .ForMember(x => x.DimensionName, opt => opt.MapFrom(p => p.Dimension.Value))
                    .ForMember(x => x.DimensionID, opt => opt.MapFrom(p => p.Dimension.ID))
                    .ForMember(x => x.Tolerance, opt => opt.MapFrom(p => p.Dimension.Tolerance))
                    .ForMember(x => x.Code, opt => opt.MapFrom(p => p.Dimension.Code)).ReverseMap();
                cfg.CreateMap<Dimension, EditItemDimensionViewModel>()
                    .ForMember(x => x.DimensionName, opt => opt.MapFrom(p => p.Value))
                    .ForMember(x => x.DimensionID, opt => opt.MapFrom(p => p.ID))
                    .ForMember(x => x.Tolerance, opt => opt.MapFrom(p => p.Tolerance))
                    .ForMember(x => x.Code, opt => opt.MapFrom(p => p.Code))
                    .ForMember(x => x.Value, opt => opt.Ignore()).ReverseMap();
                cfg.CreateMap<DimensionProfile, DimensionProfileViewModel>().ReverseMap();
                cfg.CreateMap<DimensionProfile, ProfileImageViewModel>().ReverseMap();
                cfg.CreateMap<Dimension, EditDimensionViewModel>().ReverseMap();
                cfg.CreateMap<HistoryItem, HistoryItemViewModel>().ReverseMap();
                cfg.CreateMap<SKUHistoryItem, HistoryDetailViewModel>().ReverseMap();
                cfg.CreateMap<ProductHistoryItem, HistoryDetailViewModel>().ReverseMap();
                cfg.CreateMap<Note, NoteViewModel>().ReverseMap();
                cfg.CreateMap<Note, NoteDetailViewModel>().ReverseMap();
                cfg.CreateMap<Note, CreateNoteViewModel>().ReverseMap();
                cfg.CreateMap<Glove, EditGloveConstructionViewModel>().ReverseMap();
                cfg.CreateMap<Apparel, EditApparelConstructionViewModel>().ReverseMap();
                cfg.CreateMap<Collection, EditCollectionViewModel>().ReverseMap();
                cfg.CreateMap<CollectionValue, EditCollectionValueViewModel>().ReverseMap();
                cfg.CreateMap<Photo, PhotoViewModel>().ReverseMap();
                cfg.CreateMap<Photo, PhotoDetailViewModel>().ReverseMap();
                cfg.CreateMap<Photo, CreatePhotoViewModel>().ReverseMap();
                cfg.CreateMap<ProductManagerUser, AccountListViewModel>().ReverseMap();
                cfg.CreateMap<Feature, FeatureViewModel>().ReverseMap();
                cfg.CreateMap<CrossReference, CrossReferenceViewModel>().ReverseMap();
                cfg.CreateMap<PackSize, CopyPackSizeModel>().ReverseMap();
                cfg.CreateMap<Item, EditItemViewModel>().ReverseMap();
                cfg.CreateMap<Product, WebsitePresenceViewModel>().ReverseMap();

                cfg.CreateMap<Glove, Glove>();
                cfg.CreateMap<Apparel, Apparel>();
                cfg.CreateMap<Item, Item>();
                cfg.CreateMap<Measurement, Measurement>()
                    .ForMember(x => x.ID, opt => opt.Ignore());
                cfg.CreateMap<CrossReference, CrossReference>()
                    .ForMember(x => x.ID, opt => opt.Ignore());
                cfg.CreateMap<Photo, Photo>()
                    .ForMember(x => x.ID, opt => opt.Ignore());
                cfg.CreateMap<Feature, Feature>()
                    .ForMember(x => x.ID, opt => opt.Ignore());
            });

            kernel.Bind<IMapper>().ToConstant(config.CreateMapper());
        }
    }
}
