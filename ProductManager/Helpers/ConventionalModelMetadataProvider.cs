﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

public class ConventionalModelMetadataProvider : DataAnnotationsModelMetadataProvider
{
    protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
    {
        ModelMetadata modelMetadata = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);

        if (modelMetadata.DisplayName == null)
        {
            modelMetadata.DisplayName = modelMetadata.PropertyName.SplitWords();
        }

        return modelMetadata;
    }
}