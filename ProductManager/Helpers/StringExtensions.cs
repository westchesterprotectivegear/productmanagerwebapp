﻿using System.Text.RegularExpressions;

public static class StringExtensions
{
    public static string SplitWords(this string value)
    {
        return value != null ? Regex.Replace(value, "([a-z](?=[A-Z0-9])|[A-Z](?=[A-Z][a-z]))", "$1 ").Trim() : null;
    }
}