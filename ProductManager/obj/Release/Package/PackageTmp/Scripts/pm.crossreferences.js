
function addCrossReference() {
    $.ajax({
        type: "GET",
        url: "/EditProduct/AddCrossReference/",
        success: function (responseText) {
            $("#xrefTable").append(responseText);
            $("#saveButton").show();
            $("#productDetailsForm").data("changed", true);
        },
        error: function (errorData) { onError(errorData); }
    });
}

function removeCrossReference(el) {
    $(el).closest('tr').remove();
    $("#saveButton").show();
    $("#productDetailsForm").data("changed", true);
}