﻿
function viewPhotoDetail(id) {
    $.ajax({
        url: "/Photo/Detail",
        data: { id: id },
        success: function (data) {
            $("#photoDetail").html(data);
            showDialog('#photoDetailDialog', 'View Photo', 'fa-photo', 600, 725)
        }
    });
}

function savePhoto(form) {
    var $form = $(form);

    var validFileExtensions = [".jpg", ".jpeg", ".bmp", ".png", ".pdf",".gif"];

    var fileName = $($form.find('input[type=file]')[0]).val();
    if (fileName.length > 0) {
        var valid = false;
        for (var j = 0; j < validFileExtensions.length; j++) {
            var extension = validFileExtensions[j];
            if (fileName.substr(fileName.length - extension.length, extension.length).toLowerCase() == extension.toLowerCase()) {
                if (extension == ".pdf") {
                    var result = confirm("You are uploading a PDF file. This file will be split into pages and converted to PNG. Continue?");
                    if (!result) {
                        return false;
                    }
                }

                valid = true;
                break;
            }
        }

        if (!valid) {
            alert("Sorry, " + fileName + " is invalid, allowed extensions are: " + validFileExtensions.join(", "));
            $("#photoUploadFile").val("");
            return false;
        } else {
            var formdata = new FormData($form.get(0));
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: formdata,
                processData: false,
                contentType: false,
                async: false,
                success: function (data) {
                    pushMessage("success", $form.attr('message'));
                    $($form.attr('targetpage')).html(data);
                    document.getElementById("product-image").src = document.getElementById("primaryPhoto").src;
                    load();
                }
            });
        }
    }
}

function removePhoto(e, id) {
    e.stopPropagation();
    var result = confirm("Are you sure you want to delete this photo?");
    if (result) {
        $.ajax({
            type: "POST",
            url: "/Photo/Delete/",
            data: { id: id },
            success: function (data) {
                pushMessage("success", "The photo was deleted");
                $("#photosPage").html(data);
            },
            error: function (data) {
                pushMessage("error", "Something went wrong");
                $("#photosPage").html(data);
            }
        });
        load();
    }
}

function changePrimaryPhoto() {
    var elem = document.getElementById("primaryPhotoFile");
    if (elem && document.createEvent) {
        var evt = document.createEvent("MouseEvents");
        evt.initEvent("click", true, false);
        elem.dispatchEvent(evt);
    }
}