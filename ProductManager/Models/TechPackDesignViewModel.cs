﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductManager.Models
{
    public class TechPackDesignViewModel
    {
        public bool IncludeItemOnSpec { get; set; }
        public string TagAssembly { get; set; }
        public List<string> TagAssemblyOptions { get; set; } = new List<string>();
        public Dictionary<string, bool> Violators { get; set; } = new Dictionary<string, bool>();
        public Dictionary<string, bool> WarningLabels { get; set; } = new Dictionary<string, bool>();
    }
}