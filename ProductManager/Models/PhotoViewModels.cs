﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductManager.Models
{
    public class PhotosViewModel
    {
        public string BasePartNumber { get; set; }
        public IEnumerable<PhotoViewModel> Photos { get; set; }
    }

    public class PhotoViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class PhotoDetailViewModel
    {
        public virtual string AssociatedText { get; set; }
        public virtual Guid ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Type { get; set; }
    }

    public class CreatePhotoViewModel
    {
        public virtual string AssociatedText { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string Name
        {
            get
            {
                return File == null ? "" : File.FileName;
            }
        }
        public virtual string Type { get; set; }
        public virtual HttpPostedFileBase File { get; set; }
    }
}