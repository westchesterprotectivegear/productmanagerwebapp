﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductManager.Models
{
    public class ReportingIndexViewModel
    {
        public Dictionary<string, bool> Fields { get; set; }
        public Dictionary<string, bool> SKUs { get; set; }
        public bool CrossReferencs { get; set; }
        public bool Features { get; set; }
        public bool Pricing { get; set; }
        public bool Thumbnails { get; set; }
        public bool Specs { get; set; }

        public ReportingIndexViewModel()
        {
            SKUs = new Dictionary<string, bool>();
            Fields = new Dictionary<string, bool>();
        }
    }

    public class ZipDownloadViewModel
    {
        public string SKUs { get; set; }
        public bool Images { get; set; }
        public bool VendorSpecs { get; set; }
        public bool CustomerSpecs { get; set; }
    }
}