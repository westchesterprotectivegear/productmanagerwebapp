﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SAP.Entities;

namespace ProductManager.Models
{
    public class EditProductViewModel
    {
        public List<string> SKUs { get; set; }
        public List<string> Sizes { get; set; }
        public string BasePartNumber { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ImagePath { get; set; }
    }

    public class EditProductDetailsViewModel
    {
        public string Applications { get; set; }
        public string BasePartNumber { get; set; }
        public string Brand { get; set; }
        public List<string> BrandOptions { get; set; }
        public string CountryOfOrigin { get; set; }
        public string[] CountriesOfOriginSelected { get; set; }
        public List<string> CountryOfOriginOptions { get; set; }
        public List<CrossReferenceViewModel> CrossReferences { get; set; } = new List<CrossReferenceViewModel>();
        public string Description { get; set; }
        public string Division { get; set; }
        public List<string> DivisionOptions = new List<string>
        {
            "",
            "Retail",
            "Industrial",
            "Both",
            "Welding"
        };
        [Display(Name = "Discontinued Product")]
        public bool Discontinued { get; set; }
        public string DutyRate { get; set; }
        public List<FeatureViewModel> Features { get; set; } = new List<FeatureViewModel>();
        public string HTSNumber { get; set; }
        public string HTSNumberCanada { get; set; }
        public string Keywords { get; set; }
        public string MarketingCopy { get; set; }
        [Display(Name="Amazon Product Number")]
        public string AmazonLink { get; set; }
        public string MinimumOrderQuantity { get; set; }
        public string Name { get; set; }
        public bool NewProductDevelopment { get; set; }
        public bool PrivateLabel { get; set; }
        public string ProductLine { get; set; }
        public List<string> ProductLineOptions { get; set; }
        public bool Prop65LabelRequired { get; set; }
        public string Prop65Chemicals { get; set; }
        public string[] Prop65ChemicalsSelected { get; set; }
        public List<string> Prop65ChemicalOptions { get; set; }
        public string ProductCategory { get; set; }
        public List<string> ProductCategoryOptions = new List<string>
        {
            "",
            "Boot Covers",
            "Boufant/ booties/ etc.",
            "Chemical",
            "Chemical Resistant",
            "Coated",
            "Cut Resistant",
            "Disposable",
            "Disposable Clothing",
            "Ear Protection",
            "Eye Protection",
            "Garden Accessories",
            "General Purpose",
            "Greens/ Apparel",
            "Hard Hat",
            "Lawn & Garden",
            "Leather",
            "Mig",
            "Performance/ Hi Dex",
            "Rainwear",
            "Respiration",
            "Safety Apparel",
            "Safety Equipment",
            "Sleeves",
            "Stick",
            "Tig",
            "Umbrella",
            "Winter",
            "Winter Accessories"
        };
        public string ProductType { get; set; }
        public List<string> ProductTypeOptions = new List<string>
        {
            "",
            "Gloves",
            "Apparel",
            "Accessory",
            "PPE"
        };
        public string RNNumber { get; set; }
        public List<string> RNNumberOptions { get; set; }
        public string SAPGroupNumber { get; set; }
        public List<MaterialGroup> SAPGroupNumberOptions { get; set; }
        public string SAPHierarchy { get; set; }
        public string Testing { get; set; }
        [Display(Name="Discontinued Product")]
        public bool TestProduct { get; set; }
        public string UNSPSCNumber { get; set; }
        public Dictionary<string, string> UNSPSCNumberOptions = new Dictionary<string, string>
        {
            {"", ""},
            {"46181504", "Protective gloves"},
            {"46181538", "Thermal gloves"},
            {"46181541", "Chemical resistant gloves"},
            {"53102504", "Gloves or mittens"},
            {"42132203", "Medical exam or non surgical procedure gloves"},
            {"42132200", "Medical gloves and accessories"},
            {"46181537", "Insulated gloves"},
            {"46181540", "Welder gloves"},
            {"46181536", "Anti cut gloves"},
            {"46181539", "Anti vibratory gloves"},
            {"42132205", "Surgical gloves"},
            {"46181545", "Waterproof suit"},
            {"46181501", "Protective aprons"},
            {"46181600", "Safety footwear"},
            {"46181529", "Insulated clothing for cold environments"},
            {"46181503", "Protective coveralls"},
            {"46181508", "Fire retardant apparel"},
            {"46181543", "Waterproof jacket or raincoat"},
            {"46181506", "Protective ponchos"},
            {"46181611", "Waterproof boot"},
            {"46181533", "Protective coats"},
            {"46181516", "Safety sleeves"}
        };
    }

    public class ChangeDimensionProfileViewModel
    {
        public string BasePartNumber { get; set; }
        public string DimensionProfile { get; set; }
        public string ToDimensionProfile { get; set; }
        public List<string> DimensionProfileOptions { get; set; }
        public List<ChangeDimensionViewModel> Dimensions { get; set; } = new List<ChangeDimensionViewModel>();
    }

    public class ChangeDimensionViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public Guid ToID { get; set; }
    }

    public class FeatureViewModel
    {
        public string BasePartNumber { get; set; }
        public string Index { get; set; }
        public string Value { get; set; }
        public int Rank { get; set; }
    }

    public class CrossReferenceViewModel
    {
        public string BasePartNumber { get; set; }
        public string Index { get; set; }
        public string Company { get; set; }
        public string CrossReferenceNumber { get; set; }
    }

    public class PackSizePagerViewModel
    {
        public string BasePartNumber { get; set; }
        public List<string> SizeOptions { get; set; }
        public List<PackSizeEditorViewModel> Editors { get; set; } = new List<PackSizeEditorViewModel>();
    }

    public class PackSizeEditorViewModel
    {
        public EditPackSizeDetailsViewModel Details { get; set; }
        public EditPackSizePackagingViewModel Packaging { get; set; }
    }

    public class EditPackSizeDetailsViewModel
    {
        public string SKU { get; set; }
        [Display(Name = "Units Per CT1")]
        public int CT1Cases { get; set; }
        [Display(Name = "CT1 Gross Case Weight (lb)")]
        public double CT1GrossCaseWeight { get; set; }
        public string CT1GTIN { get; set; }
        [Display(Name = "CT1 Height (in)")]
        public double CT1Height { get; set; }
        [Display(Name = "CT1 Length (in)")]
        public double CT1Length { get; set; }
        [Display(Name = "CT1 Volume (in³)")]
        public double CT1Volume { get; set; }
        [Display(Name = "CT1 Width (in)")]
        public double CT1Width { get; set; }
        [Display(Name = "Case Height (in)")]
        public double CaseHeight { get; set; }
        [Display(Name = "Case Length (in)")]
        public double CaseLength { get; set; }
        [Display(Name = "Case Volume (in³)")]
        public string CaseVolume { get; set; }
        [Display(Name = "Case Width (in)")]
        public double CaseWidth { get; set; }
        public string DipType { get; set; }
        public string GoldenSampleLocation { get; set; }
        [Display(Name = "Gross Case Weight (lb)")]
        public double GrossCaseWeight { get; set; }
        public string HemColor { get; set; }
        [Display(Name = "Inner Height (in)")]
        public double InnerHeight { get; set; }
        [Display(Name = "Inner Height (in)")]
        public double InnerLength { get; set; }
        public bool InnerDimensionsOnSpec { get; set; }
        public string InnerPackGTIN { get; set; }
        [Display(Name = "Inner Volume (in³)")]
        public string InnerVolume { get; set; }
        [Display(Name = "Inner Weight (in)")]
        public double InnerWeight { get; set; }
        [Display(Name = "Inner Width (in)")]
        public double InnerWidth { get; set; }
        [Display(Name = "Item Height (in)")]
        public double ItemHeight { get; set; }
        [Display(Name = "Item Length (in)")]
        public double ItemLength { get; set; }
        [Display(Name = "Item Width (in)")]
        public double ItemWidth { get; set; }
        public string MasterCaseGTIN { get; set; }
        [Display(Name = "Material Weight (lb)")]
        public string MaterialWeight { get; set; }
        [Display(Name = "Net Case Weight (lb)")]
        public double NetCaseWeight { get; set; }
        public string PalletCaseLayer { get; set; }
        public string PalletGTIN { get; set; }
        public string PalletLayerNumber { get; set; }
        [Display(Name = "Item Weight (lb)")]
        public double PieceWeight { get; set; }
        public bool Sensormatic { get; set; }
        public string Size { get; set; }
        public string UPC { get; set; }
        public string UnitOfMeasure { get; set; }
        public string UnitsOnPallet { get; set; }
        public int UnitsPerCase { get; set; }
        public int UnitsPerPack { get; set; }

        public List<string> UnitOfMeasureOptions { get; set; }
        public List<string> SizeOptions { get; set; }
    }

    public class EditPackSizePackagingViewModel
    {
        public string SKU { get; set; }
        public string Display { get; set; }
        public string[] DisplayTypesSelected { get; set; }
        public List<string> DisplayTypeOptions { get; set; }
        public string TagAssembly { get; set; }
        public List<string> TagAssemblyOptions { get; set; }
        public string WarningLabel { get; set; }
        public string[] WarningLabelsSelected { get; set; }
        public List<string> WarningLabelOptions { get; set; }
        public string Violator { get; set; }
        public string[] ViolatorsSelected { get; set; }
        public List<string> ViolatorOptions { get; set; }
    }

    public class EditProductMeasurementsViewModel
    {
        public string DimensionProfile { get; set; }
        public string BasePartNumber { get; set; }
        public bool ShowHemColors { get; set; }
        public List<EditItemViewModel> Items { get; set; }
        public List<string> SizeOptions { get; set; }
        public List<List<EditItemDimensionViewModel>> Dimensions { get; set; } = new List<List<EditItemDimensionViewModel>>();
    }

    public class EditItemViewModel
    {
        public bool Active { get; set; }
        public string Size { get; set; }
        public string HemColor { get; set; }
    }

    public class EditItemDimensionViewModel
    {
        public string Code { get; set; }
        public Guid DimensionID { get; set; }
        public Guid ItemID { get; set; }
        public string DimensionName { get; set; }
        public Guid MeasurementID { get; set; }
        public string Tolerance { get; set; }
        public string Value { get; set; }
    }

    public class WebsitePresenceViewModel
    {
        public string BasePartNumber { get; set; }
        [Display(Name = "Website Active")]
        public bool ActiveIndustrial { get; set; }
        public bool ActiveRetail { get; set; }
        
        public bool ActiveIroncat { get; set; }
        public List<FacetGroupViewModel> IndustrialFacets { get; set; } = new List<FacetGroupViewModel>();
        public List<FacetGroupViewModel> RetailFacets { get; set; } = new List<FacetGroupViewModel>();
        public List<FacetGroupViewModel> IroncatFacets { get; set; } = new List<FacetGroupViewModel>();
    }

    public class FacetGroupViewModel
    {
        public string Name { get; set; }
        public List<WebFacetViewModel> Facets { get; set; } = new List<WebFacetViewModel>();
    }

    public class WebFacetViewModel
    {
        public Guid ID { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }
    }

    public class EditApparelConstructionViewModel
    {
        public string ANSIGarmentType { get; set; }
        public string ANSIPerformanceClass { get; set; }
        public string BasePartNumber { get; set; }
        public string BaseMaterial { get; set; }
        public string ButtonType { get; set; }
        public string Closure { get; set; }
        public string ColorPattern { get; set; }
        public string Components { get; set; }
        public string Configuration { get; set; }
        public string ElasticType { get; set; }
        public string GarmentLogo { get; set; }
        [Display(Name = "Thickness/Weight")]
        public string GramsMils { get; set; }
        public string MajorDefects { get; set; }
        public string MaterialContent { get; set; }
        public string MinorDefects { get; set; }
        public string NumberOfPockets { get; set; }
        public string QualityStatement { get; set; }
        public string ReflectiveTape { get; set; }
        public string SeamConstruction { get; set; }
        public string SewingInstructions { get; set; }
        public string SewnInLabel { get; set; }
        public string SpecialInstructions { get; set; }
        public string StitchesPerInch { get; set; }
        public string StormFlaps { get; set; }
        public string ThreadType { get; set; }
        public string ZipperGrade { get; set; }
        public string WashingInstructions { get; set; }

        public List<string> ANSIGarmentTypeOptions { get; set; }
        public List<string> ANSIPerformanceClassOptions { get; set; }
        public List<string> ReflectiveTapeOptions { get; set; }
    }

    public class EditGloveConstructionViewModel
    {
        public string BasePartNumber { get; set; }
        [Display(Name = "Back Of Hand Material/Construction")]
        public string BackOfHand { get; set; }
        public string BackOfHandConstruction { get; set; }
        public string Coating { get; set; }
        public string ColorDetail { get; set; }
        public string ColorPattern { get; set; }
        public string Components { get; set; }
        public string CuffConstruction { get; set; }
        public string CuffType { get; set; }
        public string DipType { get; set; }
        public string Grade { get; set; }
        [Display(Name = "Thickness/Weight")]
        public string GramsMils { get; set; }
        public string HemConstruction { get; set; }
        public string KnuckleConstruction { get; set; }
        public string LeatherLocation { get; set; }
        public string LeatherType { get; set; }
        public string Lining { get; set; }
        public string MajorDefects { get; set; }
        public string MaterialContent { get; set; }
        public string MilGauge { get; set; }
        public string MinorDefects { get; set; }
        public string PalmConstruction { get; set; }
        [Display(Name = "Palm Style Material/Construction")]
        public string PalmStyle { get; set; }
        public string Pattern { get; set; }
        public string QualityLevel { get; set; }
        public string QualityStatement { get; set; }
        public string ReinforcedFeatures { get; set; }
        public string SewingInstructions { get; set; }
        public string SewnInLabel { get; set; }
        public string Shell { get; set; }
        public string SpecialInstructions { get; set; }
        public string StitchesPerInch { get; set; }
        public string ThreadType { get; set; }
        public string ThumbType { get; set; }
        public string WashingInstructions { get; set; }

        public List<string> GlovePatternOptions { get; set; }
        public List<string> CuffTypeOptions { get; set; }
        public List<string> GradeOptions { get; set; }
        public List<string> QualityLevelOptions { get; set; }
        public List<string> ThumbTypeOptions { get; set; }
    }

    public class SAPHierarchyViewModel
    {
        public List<string> BreadCrumb { get; set; }
        public string Root { get; set; }
        public Dictionary<string, string> Options { get; set; } = new Dictionary<string, string>();
    }

    public class CreatePackSizeViewModel
    {
        [Required]
        public string Size { get; set; }
        [Required]
        public string SKU { get; set; }
    }
}