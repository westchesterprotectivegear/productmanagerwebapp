﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductManager.Models
{
	public class DimensionProfileViewModel
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public List<EditDimensionViewModel> Dimensions { get; set; } = new List<EditDimensionViewModel>();
    }

    public class EditDimensionViewModel
    {
        public string Code { get; set; }
        public Guid ID { get; set; }
        public string ProfileName { get; set; }
        public string Tolerance { get; set; }
        public string Value { get; set; }
        public string GoldenSpecValue { get; set; }
        public bool ActiveOnCustomerSpec { get; set; }
        public string Index { get; set; }
    }

    public class ProfileImageViewModel
    {
        public string Image { get; set; }
        public string Name { get; set; }
    }
}