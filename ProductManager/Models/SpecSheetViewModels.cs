﻿using System;

namespace ProductManager.Models
{
    public class SpecSheetViewModel
    {
        public string BasePartNumber { get; set; }
        public string Type { get; set; }
        public bool VendorSpecExists { get; set; }
        public bool CustomerSpecExists { get; set; }
    }
}