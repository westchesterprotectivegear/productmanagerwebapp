﻿using System.Web;
using OfficeOpenXml;
using ProductManager.Entities;
using ProductManager;
using Ninject;


namespace ProductManager.Models
{
    public class ProductListViewModel
    {
        public string SKU { get; set; }
        public string BasePartNumber { get; set; }
        public string Description { get; set; }
    }
    public class ProductMassCreateViewModel
    {
        public HttpPostedFileBase infile { get; set; }
        public HttpPostedFileBase outfile { get; set; }
        
    }
}