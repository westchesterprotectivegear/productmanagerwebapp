﻿using System;

namespace ProductManager.Models
{
    public class HistoryItemViewModel
    {
        public virtual Guid ID { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Message { get; set; }
    }

    public class HistoryDetailViewModel
    {
        public virtual Guid ID { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Message { get; set; }
        public virtual string OriginalValue { get; set; }
        public virtual string NewValue { get; set; }
        public virtual string Property { get; set; }
    }
}