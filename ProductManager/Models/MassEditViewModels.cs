﻿using System.Collections.Generic;

namespace ProductManager.Models
{
    public class TemplateBuilderViewModel
    {
        public Dictionary<string, bool> SKUs { get; set; } = new Dictionary<string, bool>();
        public Dictionary<string, bool> Fields { get; set; } = new Dictionary<string, bool>();
        public Dictionary<string, Dictionary<string, string>> changes = new Dictionary<string, Dictionary<string, string>>();
    }
}