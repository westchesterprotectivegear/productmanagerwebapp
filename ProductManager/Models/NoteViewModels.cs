﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductManager.Models
{
    public class NotesViewModel
    {
        public string BasePartNumber { get; set; }
        public IEnumerable<NoteViewModel> Notes { get; set; }
    }

    public class NoteViewModel
    {
        public DateTime Date { get; set; }
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string User { get; set; }
    }

    public class NoteDetailViewModel
    {
        public DateTime Date { get; set; }
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }

    public class CreateNoteViewModel
    {
        public string BasePartNumber { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public string Title { get; set; }
    }
}