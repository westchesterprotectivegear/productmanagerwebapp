﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductManager.Models
{
    public class EditCollectionViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public List<EditCollectionValueViewModel> Values { get; set; } = new List<EditCollectionValueViewModel>();
    }

    public class EditCollectionValueViewModel
    {
        public string Value { get; set; }
        public int CollectionIndex { get; set; }
        public string Index { get; set; }
    }
}