﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductManager.Models
{
    public class WizardResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Page { get; set; }
    }

    public class CopyProductModel
    {
        public CopyStep1ViewModel Step1 { get; set; }
        public CopyStep2ViewModel Step2 { get; set; }
        public CopyStep3ViewModel Step3 { get; set; }
    }

    public class CreateProductViewModel
    {
        public string BasePartNumber { get; set; }
        public string DimensionProfile { get; set; }
        public string ProductType { get; set; }
        public List<string> DimensionProfiles { get; set; }
        public Dictionary<string, bool> Sizes { get; set; } = new Dictionary<string, bool>();
    }

    public class CopyStep1ViewModel
    {
        [Required]
        public string CopyBasePartNumber { get; set; }
        [Required]
        public string NewBasePartNumber { get; set; }
        public List<string> BasePartNumbers { get; set; }
    }

    public class CopyStep2ViewModel
    {
        public Dictionary<string, bool> CopySizes { get; set; } = new Dictionary<string, bool>();
        public Dictionary<string, bool> NewSizes { get; set; } = new Dictionary<string, bool>();
    }

    public class CopyStep3ViewModel
    {
        public Dictionary<string, bool> Fields { get; set; } = new Dictionary<string, bool>();
        public bool CrossReferencs { get; set; } = true;
        public bool Features { get; set; } = true;
        public bool Measurements { get; set; } = true;
    }

    public class CopyStep4ViewModel
    {
        public string BasePartNumber { get; set; }
        public string CopyBasePartNumber { get; set; }
        public List<string> Sizes { get; set; } = new List<string>();
        public int FieldCount { get; set; }
    }

    public class CopyPackSizeModel
    {
        public bool Copy { get; set; }
        public string SKU { get; set; }
        public string Size { get; set; }
        public string NewSKU { get; set; }
    }

    public class NewOrCopyViewModel
    {
        [Display(Name = "Select a product to copy")]
        public string CopyFrom { get; set; }
        [Display(Name = "Dimension Profile")]
        public string DimensionProfile { get; set; }

        [Required(ErrorMessage = "Select an option")]
        public string NewOrCopy { get; set; }

        [Required(ErrorMessage = "You must supply a new part number")]
        [Display(Name = "New Part Number")]
        public string NewPartNumber { get; set; }
        [Display(Name = "Product Type")]
        public string ProductType { get; set; }
        public List<string> BaseParts { get; set; }
        public List<string> DimensionProfiles { get; set; }
    }

    public class NewItemsViewModel
    {
        public bool IsCopy { get; set; }
        public List<NewItemViewModel> NewItems { get; set; } = new List<NewItemViewModel>();
    }

    public class NewItemViewModel
    {
        public string CopyFrom { get; set; }
        public bool Copy { get; set; }
        public string SKU { get; set; }
        public string Size { get; set; }
        public string Index { get; set; }
        public Dictionary<string, string> CopyOptions { get; set; } = new Dictionary<string, string>();
        public List<string> SizeOptions { get; set; } = new List<string>();
    }

    public class ConfirmCreateViewModel
    {
        public string PartNumber { get; set; }
        public Dictionary<string, string> SKUs { get; set; }
    }
}