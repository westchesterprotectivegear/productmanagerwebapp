﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductManager.Models
{
    public class FilteredReportsViewModel
    {
        [Display(Name="SKU List (Comma or Newline Separated)")]
        public string SKUList { get; set; }
        public List<string> Divisions { get; set; }
        public List<string> DivisionOptions = new List<string>
        {
            "",
            "Retail",
            "Industrial",
            "Both",
            "Welding"
        };
        public List<string> ProductTypes { get; set; }
        public List<string> ProductTypeOptions = new List<string>
        {
            "",
            "Gloves",
            "Apparel",
            "Accessory",
            "PPE"
        };
        public List<string> ProductCategory { get; set; }
        public List<string> ProductCategoryOptions = new List<string>
        {
            "",
            "Boot Covers",
            "Boufant/ booties/ etc.",
            "Chemical",
            "Chemical Resistant",
            "Coated",
            "Cut Resistant",
            "Disposable",
            "Disposable Clothing",
            "Ear Protection",
            "Eye Protection",
            "Garden Accessories",
            "General Purpose",
            "Greens/ Apparel",
            "Hard Hat",
            "Lawn & Garden",
            "Leather",
            "Mig",
            "Performance/ Hi Dex",
            "Rainwear",
            "Respiration",
            "Safety Apparel",
            "Safety Equipment",
            "Sleeves",
            "Stick",
            "Tig",
            "Umbrella",
            "Winter",
            "Winter Accessories"
        };
        public List<string> Brand { get; set; }
        public List<string> BrandOptions;
        public List<string> CountryOfOrigin { get; set; }
        public List<string> CountryOfOriginOptions;
        public int Active { get; set; }
        public List<string> SelectedColumns { get; set; }
        public List<string> ColumnOptions = new List<string>
        {
            "Thumbnail",
            "Size",
            "Name",
            "Description",
            "Marketing Copy",
            "Division",
            "Product Type",
            "Product Category",
            "SAP Group Number",
            "Brand",
            "New Product Flag",
            "Private Label Flag",
            "Discontinued Flag",
            "Prop 65",
            "Country of Origin",
            "HTS Number",
            "HTS Number Canada",
            "Duty Rate",
            "UNSPSC Number",
            "RN Number",
            "SAP Hierarchy",
            "Features",
            "Testing",
            "Pattern",
            "Coating",
            "Mil Gauge",
            "Thickness/Weight",
            "Lining",
            "Shell",
            "Palm Style Material/Construction",
            "Back of Hand Material/Construction",
            "Cuff Type",
            "Leather Type",
            "Leather Location",
            "Grade",
            "Quality Level",
            "Thumb Type",
            "Components",
            "Material Content",
            "Cuff Construction",
            "Hem Construction",
            "Knuckle Construction",
            "Thread Type",
            "Color Pattern",
            "Base Material",
            "Number Of Pockets",
            "Elastic Type",
            "Seam Construction",
            "Button Type",
            "Zipper Grade",
            "Closure",
            "Storm Flaps",
            "Thread Type",
            "Stitches Per Inch",
            "Garment Logo",
            "Sewn In Label",
            "Material Content",
            "Washing Instructions",
            "Components",
            "Sewing Instructions",
            "Major Defects",
            "Minor Defects",
            "Special Instructions",
            "Quality Statement",
            "ANSI Garment Type",
            "ANSI Performance Class",
            "Reflective Tape",
            "Unit of Measure",
            "Item Length",
            "Item Width",
            "Item Height",
            "Item Weight",
            "Golden Sample Location",
            "Units Per Pack",
            "Units Per Case",
            "Units Per CT1",
            "Inner Length",
            "Inner Width",
            "Inner Height",
            "Inner Volume",
            "Inner Weight",
            "UPC",
            "Inner Pack GTIN",
            "Master Case GTIN",
            "CT1GTIN",
            "Pallet GTIN",
            "Case Length",
            "Case Width",
            "Case Height",
            "Case Volume",
            "Net Case Weight",
            "Gross Case Weight",
            "CT1 Length",
            "CT1 Width",
            "CT1 Height",
            "CT1 Volume",
            "CT1 Gross Case Weight"
        };
    }
}