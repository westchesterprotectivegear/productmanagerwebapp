﻿using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;
using NHibernate.Tool.hbm2ddl;

namespace ProductManager
{
    public interface DatabaseContext
    {
        ISessionFactory GetSessionFactory();
    }

    public class ProductManagerDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {
            return Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008.ConnectionString(c => c.FromConnectionStringWithKey("ProductManagerConnection")))
                    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("ProductManager.Entities")))
                    .CurrentSessionContext<WebSessionContext>()
                    .BuildSessionFactory();
        }
    }

    public class SAPDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {

            return Fluently.Configure()
                          .Database(MsSqlConfiguration.MsSql2000.ConnectionString(c => c.FromConnectionStringWithKey("SAPConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("SAP.Entities")))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }

    public class IroncatWebsiteDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {
            return Fluently.Configure()
                          .Database(MySQLConfiguration.Standard.ConnectionString(c => c.FromConnectionStringWithKey("IroncatConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("ProductManager.Website.Entities")))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }

    public class RetailWebsiteDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {
            return Fluently.Configure()
                          .Database(MySQLConfiguration.Standard.ConnectionString(c => c.FromConnectionStringWithKey("RetailConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("ProductManager.Website.Entities")))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }

    public class IndustrialWebsiteDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {
            return Fluently.Configure()
                          .Database(MySQLConfiguration.Standard.ConnectionString(c => c.FromConnectionStringWithKey("IndustrialConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("ProductManager.Website.Entities")))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }
}