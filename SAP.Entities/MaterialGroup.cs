﻿using FluentNHibernate.Mapping;

namespace SAP.Entities
{
    public class MaterialGroup
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
    }

    public sealed class MaterialGroupMapping : ClassMap<MaterialGroup>
    {
        public MaterialGroupMapping()
        {
            Table("prd.T023T");
            Where("MANDT = 100 and SPRAS = 'E'");
            Id(p => p.Code, "MATKL");
            Map(p => p.Name, "WGBEZ60");
        }
    }
}
