﻿using FluentNHibernate.Mapping;

namespace SAP.Entities
{
    public class Hierarchy
    {
        public virtual string ID { get; set; }
        public virtual string Text { get; set; }
        public virtual string ParentID
        {
            get
            {
                return ID.Substring(0, ID.Length - 3);
            }
        }
    }

    public sealed class HierarchyMapping : ClassMap<Hierarchy>
    {
        public HierarchyMapping()
        {
            Table("prd.T179T");
            Where("MANDT = 100 and SPRAS = 'E'");
            Id(p => p.ID, "PRODH");
            Map(p => p.Text, "VTEXT");
        }
    }
}
