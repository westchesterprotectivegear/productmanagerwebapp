﻿using FluentNHibernate.Mapping;

namespace ProductManager.Web.Entities
{
    public class FileUsage
    {
        public virtual int FileID { get; set; }
        public virtual string Module { get; set; } = "file";
        public virtual string Type { get; set; } = "node";
        public virtual int ID { get; set; }
        public virtual int Count { get; set; } = 1;

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class FileUsageMapping : ClassMap<FileUsage>
    {
        public FileUsageMapping()
        {
            LazyLoad();
            Table("drupal_file_usage");
            CompositeId()
                .KeyProperty(p => p.FileID, "fid")
                .KeyProperty(p => p.Module, "module")
                .KeyProperty(p => p.Type, "type")
                .KeyProperty(p => p.ID, "id");
            Map(p => p.Count, "count");
        }
    }
}
