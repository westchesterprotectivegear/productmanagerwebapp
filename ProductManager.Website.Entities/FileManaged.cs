﻿using System.Collections.Generic;
using FluentNHibernate.Mapping;

namespace ProductManager.Web.Entities
{
    public class FileManaged
    {
        public virtual int FileID { get; set; }
        public virtual int UserID { get; set; } = 42;
        public virtual string FileName { get; set; }
        public virtual string URI { get; set; }
        public virtual string FileMime { get; set; } = "file/png";
        public virtual int FileSize { get; set; }
        public virtual int Status { get; set; } = 1;
        public virtual int Timestamp { get; set; }
        public virtual IList<FileUsage> FileUsages { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class FileManagedMapping : ClassMap<FileManaged>
    {
        public FileManagedMapping()
        {
            Table("drupal_file_managed");
            Id(x => x.FileID, "fid").GeneratedBy.Increment();
            Map(p => p.UserID, "uid");
            Map(p => p.FileName, "filename");
            Map(p => p.URI, "uri");
            Map(p => p.FileMime, "filemime");
            Map(p => p.FileSize, "filesize");
            Map(p => p.Status, "status");
            Map(p => p.Timestamp, "timestamp");
            HasMany(x => x.FileUsages)
                .KeyColumn("fid")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.Delete();
        }
    }
}
