﻿using System.Collections.Generic;
using FluentNHibernate.Mapping;

namespace ProductManager.Web.Entities
{
    public class Field
    {
        public virtual string FieldDeclaration { get; set; }
        public virtual string EntityType { get; set; } = "node";
        public virtual string Bundle { get; set; } = "glove";
        public virtual int Deleted { get; set; } = 0;
        public virtual int EntityID { get; set; }
        public virtual int RevisionID { get; set; }
        public virtual string Language { get; set; } = "und";
        public virtual int Delta { get; set; } = 0;

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class ValueField : Field
    {
        public virtual string Value { get; set; }
    }
    public class ImageField : Field
    {
        public virtual int FileID { get; set; }
        public virtual string Alt { get; set; }
        public virtual string Title { get; set; }
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }
    }

    public class FieldMapping<T> : ClassMap<T> where T : Field
    {
        public FieldMapping()
        {
            CompositeId()
                .KeyProperty(p => p.EntityType, "entity_type")
                .KeyProperty(p => p.Deleted, "deleted")
                .KeyProperty(p => p.EntityID, "entity_id")
                .KeyProperty(p => p.Language, "language")
                .KeyProperty(p => p.Delta, "delta");
            Map(p => p.Bundle, "bundle");
            Map(p => p.RevisionID, "revision_id");
        }
    }

    public class Application : ValueField { }
    public class ApplicationMapping : FieldMapping<Application>
    {
        ApplicationMapping()
        {
            Table("drupal_field_data_field_application");
            Map(p => p.Value, "field_application_value");
        }
    }

    public class Audience : ValueField { }
    public class AudienceMapping : FieldMapping<Audience>
    {
        AudienceMapping()
        {
            Table("drupal_field_data_field_audience");
            Map(p => p.Value, "field_audience_value");
        }
    }

    public class Body : ValueField { }
    public class BodyMapping : FieldMapping<Body>
    {
        BodyMapping()
        {
            Table("drupal_field_data_body");
            Map(p => p.Value, "body_value");
        }
    }

    public class Brand : ValueField { }
    public class BrandMapping : FieldMapping<Brand>
    {
        BrandMapping()
        {
            Table("drupal_field_data_field_brand");
            Map(p => p.Value, "field_brand_value");
        }
    }

    public class CompetitorBrand : ValueField { }
    public class CompetitorBrandMapping : FieldMapping<CompetitorBrand>
    {
        CompetitorBrandMapping()
        {
            Table("drupal_field_data_field_competitor_brand");
            Map(p => p.Value, "field_competitor_brand_value");
        }
    }

    public class CompetitorPart : ValueField { }
    public class CompetitorPartMapping : FieldMapping<CompetitorPart>
    {
        CompetitorPartMapping()
        {
            Table("drupal_field_data_field_competitor_part");
            Map(p => p.Value, "field_competitor_part_value");
        }
    }

    public class Copy : ValueField { }
    public class CopyMapping : FieldMapping<Copy>
    {
        CopyMapping()
        {
            Table("drupal_field_data_field_copy");
            Map(p => p.Value, "field_copy_value");
        }
    }

    public class Feature : ValueField { }
    public class FeatureMapping : FieldMapping<Feature>
    {
        FeatureMapping()
        {
            Table("drupal_field_data_field_feature");
            Map(p => p.Value, "field_feature_value");
        }
    }

    public class ItemStyle : ValueField { }
    public class ItemStyleMapping : FieldMapping<ItemStyle>
    {
        ItemStyleMapping()
        {
            Table("drupal_field_data_field_item_style");
            Map(p => p.Value, "field_item_style_value");
        }
    }

    public class ItemType : ValueField { }
    public class ItemTypeMapping : FieldMapping<ItemType>
    {
        ItemTypeMapping()
        {
            Table("drupal_field_data_field_item_type");
            Map(p => p.Value, "field_item_type_value");
        }
    }

    public class Material : ValueField { }
    public class MaterialMapping : FieldMapping<Material>
    {
        MaterialMapping()
        {
            Table("drupal_field_data_field_material");
            Map(p => p.Value, "field_material_value");
        }
    }

    public class Name : ValueField { }
    public class NameMapping : FieldMapping<Name>
    {
        NameMapping()
        {
            Table("drupal_field_data_field_name");
            Map(p => p.Value, "field_name_value");
        }
    }

    public class PackSize : ValueField { }
    public class PackSizeMapping : FieldMapping<PackSize>
    {
        PackSizeMapping()
        {
            Table("drupal_field_data_field_pack_size");
            Map(p => p.Value, "field_pack_size_value");
        }
    }

    public class ProductType : ValueField { }
    public class ProductTypeMapping : FieldMapping<ProductType>
    {
        ProductTypeMapping()
        {
            Table("drupal_field_data_field_product_type");
            Map(p => p.Value, "field_product_type_value");
        }
    }

    public class ProtectionNeed : ValueField { }
    public class ProtectionNeedMapping : FieldMapping<ProtectionNeed>
    {
        ProtectionNeedMapping()
        {
            Table("drupal_field_data_field_protection_need");
            Map(p => p.Value, "field_protection_need_value");
        }
    }

    public class Quality : ValueField { }
    public class QualityMapping : FieldMapping<Quality>
    {
        QualityMapping()
        {
            Table("drupal_field_data_field_quality");
            Map(p => p.Value, "field_quality_value");
        }
    }

    public class Size : ValueField { }
    public class SizeMapping : FieldMapping<Size>
    {
        SizeMapping()
        {
            Table("drupal_field_data_field_size");
            Map(p => p.Value, "field_size_value");
        }
    }

    public class SKU : ValueField { }
    public class SKUMapping : FieldMapping<SKU>
    {
        SKUMapping()
        {
            Table("drupal_field_data_field_sku");
            Map(p => p.Value, "field_sku_value");
        }
    }

    public class SpecSheet : ValueField { }
    public class SpecSheetMapping : FieldMapping<SpecSheet>
    {
        SpecSheetMapping()
        {
            Table("drupal_field_data_field_spec_sheet");
            Map(p => p.Value, "field_spec_sheet_value");
        }
    }

    public class TypeOfJob : ValueField { }
    public class TypeOfJobMapping : FieldMapping<TypeOfJob>
    {
        TypeOfJobMapping()
        {
            Table("drupal_field_data_field_type_of_job");
            Map(p => p.Value, "field_type_of_job_value");
        }
    }

    public class Warning : ValueField { }
    public class WarningMapping : FieldMapping<Warning>
    {
        WarningMapping()
        {
            Table("drupal_field_data_field_warning");
            Map(p => p.Value, "field_warning_value");
        }
    }

    public class WestChesterPart : ValueField { }
    public class WestChesterPartMapping : FieldMapping<WestChesterPart>
    {
        WestChesterPartMapping()
        {
            Table("drupal_field_data_field_west_chester_part");
            Map(p => p.Value, "field_west_chester_part_value");
        }
    }

    public class CrossReferenceImage : ImageField { }
    public class CrossReferenceImageMapping : FieldMapping<CrossReferenceImage>
    {
        CrossReferenceImageMapping()
        {
            Table("drupal_field_data_field_comparative_tool_image");
            Map(p => p.FileID, "field_comparative_tool_image_fid");
            Map(p => p.Alt, "field_comparative_tool_image_alt");
            Map(p => p.Title, "field_comparative_tool_image_title");
            Map(p => p.Width, "field_comparative_tool_image_width");
            Map(p => p.Height, "field_comparative_tool_image_height");
        }
    }

    public class ProductImage : ImageField { }
    public class ProductImageMapping : FieldMapping<ProductImage>
    {
        ProductImageMapping()
        {
            Table("drupal_field_data_field_image");
            Map(p => p.FileID, "field_image_fid");
            Map(p => p.Alt, "field_image_alt");
            Map(p => p.Title, "field_image_title");
            Map(p => p.Width, "field_image_width");
            Map(p => p.Height, "field_image_height");
        }
    }
}
