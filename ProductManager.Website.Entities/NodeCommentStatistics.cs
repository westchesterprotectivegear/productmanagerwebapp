﻿using FluentNHibernate.Mapping;

namespace ProductManager.Web.Entities
{
    public class NodeCommentStatistics
    {
        public virtual int NodeID { get; set; }
        public virtual int CommentID { get; set; } = 0;
        public virtual int LastCommentDate { get; set; }
        public virtual string LastCommentName { get; set; } = null;
        public virtual int LastCommentUserID { get; set; } = 0;
        public virtual int CommentCount { get; set; } = 0;
    }

    public sealed class NodeCommentStatisticsMapping : ClassMap<NodeCommentStatistics>
    {
        public NodeCommentStatisticsMapping()
        {
            LazyLoad();
            Table("drupal_node_comment_statistics");
            Id(p => p.NodeID, "nid").GeneratedBy.Assigned();
            Map(p => p.CommentID, "cid");
            Map(p => p.LastCommentDate, "last_comment_timestamp");
            Map(p => p.LastCommentName, "last_comment_name");
            Map(p => p.LastCommentUserID, "last_comment_uid");
            Map(p => p.CommentCount, "comment_count");
        }
    }
}
