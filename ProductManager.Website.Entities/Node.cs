﻿using System.Collections.Generic;
using FluentNHibernate.Mapping;
using System.Linq;

namespace ProductManager.Web.Entities
{
    public class Node
    {
        public virtual int NodeID { get; set; }
        public virtual int VersionID { get; set; } = int.MaxValue;
        public virtual int UserID { get; set; } = 42;
        public virtual string Type { get; set; }
        public virtual string Language { get; set; } = "und";
        public virtual string Title { get; set; }
        public virtual int Status { get; set; } = 1;
        public virtual int Created { get; set; } = 0;
        public virtual int Changed { get; set; } = 0;
        public virtual int Comment { get; set; } = 2;
        public virtual int Promote { get; set; } = 0;
        public virtual int Sticky { get; set; } = 0;
        public virtual int TranslationID { get; set; } = 0;
        public virtual int Translate { get; set; } = 0;
    }

    public class NodeMapping<T> : ClassMap<T> where T : Node
    {
        public NodeMapping()
        {
            LazyLoad();
            Table("drupal_node");
            Id(p => p.NodeID, "nid").GeneratedBy.Increment();
            Map(p => p.VersionID, "vid");
            Map(p => p.UserID, "uid");
            Map(p => p.Type, "type");
            Map(p => p.Language, "language");
            Map(p => p.Title, "title");
            Map(p => p.Status, "status");
            Map(p => p.Created, "created");
            Map(p => p.Changed, "changed");
            Map(p => p.Comment, "comment");
            Map(p => p.Promote, "promote");
            Map(p => p.Sticky, "sticky");
            Map(p => p.TranslationID, "tnid");
            Map(p => p.Translate, "translate");
        }
    }

    public class NodeRevision
    {
        public virtual int NodeID { get; set; }
        public virtual int VersionID { get; set; } = int.MaxValue;
        public virtual int UserID { get; set; } = 42;
        public virtual string Title { get; set; }
        public virtual string Log { get; set; } = "";
        public virtual int TimeStamp { get; set; } = 0;
        public virtual int Status { get; set; } = 1;
        public virtual int Comment { get; set; } = 2;
        public virtual int Promote { get; set; } = 0;
        public virtual int Sticky { get; set; } = 0;
        public virtual string DSSwitch { get; set; } = "";
    }

    public sealed class NodeRevisionMapping : ClassMap<NodeRevision>
    {
        public NodeRevisionMapping()
        {
            LazyLoad();
            Table("drupal_node_revision");
            Id(p => p.VersionID, "vid").GeneratedBy.Increment();
            Map(p => p.NodeID, "nid");
            Map(p => p.UserID, "uid");
            Map(p => p.Title, "title");
            Map(p => p.Log, "log");
            Map(p => p.TimeStamp, "timestamp");
            Map(p => p.Status, "status");
            Map(p => p.Comment, "comment");
            Map(p => p.Promote, "promote");
            Map(p => p.Sticky, "sticky");
            Map(p => p.DSSwitch, "ds_switch");
        }
    }

    public class ProductNode : Node
    {
        public virtual IList<Application> Applications { get; set; } = new List<Application>();
        public virtual IList<Audience> Audiences { get; set; } = new List<Audience>();
        public virtual IList<Body> Bodies { get; set; } = new List<Body>();
        public virtual IList<Brand> Brands { get; set; } = new List<Brand>();
        public virtual IList<NodeCommentStatistics> CommentStatistics { get; set; } = new List<NodeCommentStatistics>();
        public virtual IList<Copy> Copies { get; set; } = new List<Copy>();
        public virtual IList<CrossReferenceNode> CrossReferences { get; set; } = new List<CrossReferenceNode>();
        public virtual IList<Feature> Features { get; set; } = new List<Feature>();
        public virtual IList<FileUsage> FileUsages { get; set; } = new List<FileUsage>();
        public virtual IList<ItemStyle> ItemStyles { get; set; } = new List<ItemStyle>();
        public virtual IList<ItemType> ItemTypes { get; set; } = new List<ItemType>();
        public virtual IList<Material> Materials { get; set; } = new List<Material>();
        public virtual IList<Name> Names { get; set; } = new List<Name>();
        public virtual IList<PackSize> PackSizes { get; set; } = new List<PackSize>();
        public virtual IList<ProductImage> ProductImages { get; set; } = new List<ProductImage>();
        public virtual IList<ProtectionNeed> ProtectionNeeds { get; set; } = new List<ProtectionNeed>();
        public virtual IList<Quality> Qualities { get; set; } = new List<Quality>();
        public virtual IList<NodeRevision> Revisions { get; set; } = new List<NodeRevision>();
        public virtual IList<Size> Sizes { get; set; } = new List<Size>();
        public virtual IList<SKU> SKUs { get; set; } = new List<SKU>();
        public virtual IList<SpecSheet> SpecSheets { get; set; } = new List<SpecSheet>();
        public virtual IList<TypeOfJob> TypeOfJobs { get; set; } = new List<TypeOfJob>();
        public virtual IList<Warning> Warnings { get; set; } = new List<Warning>();

        public virtual IList<Field> Fields
        {
            get
            {
                return Applications.Cast<Field>()
                    .Concat(Audiences.Cast<Field>())
                    .Concat(Brands.Cast<Field>())
                    .Concat(Copies.Cast<Field>())
                    .Concat(Features.Cast<Field>())
                    .Concat(ItemStyles.Cast<Field>())
                    .Concat(ItemTypes.Cast<Field>())
                    .Concat(Materials.Cast<Field>())
                    .Concat(Names.Cast<Field>())
                    .Concat(PackSizes.Cast<Field>())
                    .Concat(ProtectionNeeds.Cast<Field>())
                    .Concat(Qualities.Cast<Field>())
                    .Concat(Sizes.Cast<Field>())
                    .Concat(SKUs.Cast<Field>())
                    .Concat(SpecSheets.Cast<Field>())
                    .Concat(ProductImages.Cast<Field>())
                    .Concat(TypeOfJobs.Cast<Field>())
                    .Concat(Warnings.Cast<Field>()).ToList();
            }
        }

        public virtual void Clear()
        {
            Applications.Clear();
            Audiences.Clear();
            Brands.Clear();
            Bodies.Clear();
            CommentStatistics.Clear();
            Copies.Clear();
            CrossReferences.Clear();
            Features.Clear();
            ItemStyles.Clear();
            ItemTypes.Clear();
            Materials.Clear();
            Names.Clear();
            PackSizes.Clear();
            ProductImages.Clear();
            ProtectionNeeds.Clear();
            Qualities.Clear();
            Revisions.Clear();
            Sizes.Clear();
            SKUs.Clear();
            SpecSheets.Clear();
            TypeOfJobs.Clear();
            Warnings.Clear();
        }
    }

    public class ProductNodeMapping : NodeMapping<ProductNode>
    {
        public ProductNodeMapping()
        {
            HasMany(x => x.Applications)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Audiences)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Brands)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Bodies)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.CommentStatistics)
                .KeyColumn("nid")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Copies)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.CrossReferences)
                .KeyColumn("title")
                .PropertyRef("Title")
                .Where("type = 'product_comparison'")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Features)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.FileUsages)
                .KeyColumn("id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.ItemStyles)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.ItemTypes)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Materials)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Names)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.PackSizes)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.ProductImages)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.ProtectionNeeds)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Qualities)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Revisions)
                .KeyColumn("nid")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Sizes)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.SKUs)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.SpecSheets)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.TypeOfJobs)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
            HasMany(x => x.Warnings)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.DeleteOrphan();
        }
    }

    public class CrossReferenceNode : Node
    {
        public virtual IList<CrossReferenceImage> Images { get; set; } = new List<CrossReferenceImage>();
        public virtual IList<CompetitorBrand> CompetitorBrands { get; set; } = new List<CompetitorBrand>();
        public virtual IList<CompetitorPart> CompetitorParts { get; set; } = new List<CompetitorPart>();
        public virtual IList<NodeRevision> Revisions { get; set; } = new List<NodeRevision>();
        public virtual IList<WestChesterPart> WestChesterParts { get; set; } = new List<WestChesterPart>();
    }
    public class CrossReferenceNodeMapping : NodeMapping<CrossReferenceNode>
    {
        public CrossReferenceNodeMapping()
        {
            HasMany(x => x.Images)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.AllDeleteOrphan();
            HasMany(x => x.CompetitorBrands)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.AllDeleteOrphan();
            HasMany(x => x.CompetitorParts)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.AllDeleteOrphan();
            HasMany(x => x.Revisions)
                .KeyColumn("vid")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.AllDeleteOrphan();
            HasMany(x => x.WestChesterParts)
                .KeyColumn("entity_id")
                .Inverse()
                .NotFound.Ignore()
                .Cascade.AllDeleteOrphan();
        }
    }
}
