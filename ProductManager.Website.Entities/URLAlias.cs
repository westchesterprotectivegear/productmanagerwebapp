﻿using FluentNHibernate.Mapping;

namespace ProductManager.Web.Entities
{
    public class URLAlias
    {
        public virtual int PID { get; set; }
        public virtual string Source { get; set; } = "";
        public virtual string Alias { get; set; } = "";
        public virtual string Language { get; set; } = "und";
    }

    public sealed class URLAliasMapping : ClassMap<URLAlias>
    {
        public URLAliasMapping()
        {
            LazyLoad();
            Table("drupal_url_alias");
            Id(p => p.PID, "pid").GeneratedBy.Assigned();
            Map(p => p.Source, "source");
            Map(p => p.Alias, "alias");
            Map(p => p.Language, "language");
        }
    }
}
